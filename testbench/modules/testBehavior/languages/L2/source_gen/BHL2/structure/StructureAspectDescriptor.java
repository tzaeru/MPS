package BHL2.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import java.util.Map;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import jetbrains.mps.smodel.SNodePointer;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  private final Map<SConceptId, Integer> myIndexMap = new HashMap<SConceptId, Integer>(2);
  /*package*/ final ConceptDescriptor myConceptA = createDescriptorForA();
  /*package*/ final ConceptDescriptor myConceptB = createDescriptorForB();

  public StructureAspectDescriptor() {
    myIndexMap.put(myConceptA.getId(), 0);
    myIndexMap.put(myConceptB.getId(), 1);
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptA, myConceptB);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    Integer index = myIndexMap.get(id);
    if (index == null) {
      return null;
    }
    switch (((int) index)) {
      case 0:
        return myConceptA;
      case 1:
        return myConceptB;
      default:
        throw new IllegalStateException();
    }
  }

  private static ConceptDescriptor createDescriptorForA() {
    return new ConceptDescriptorBuilder("BHL2.structure.A", MetaIdFactory.conceptId(0xd9c7536e76b5498fL, 0x80640955dd8aebcbL, 0x6ab2e61d35e45c5dL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).sourceNode(new SNodePointer("r:e2b20dd3-debc-4be6-b7ff-17ade77d9a0f(BHL2.structure)", "7688460527007456349")).create();
  }
  private static ConceptDescriptor createDescriptorForB() {
    return new ConceptDescriptorBuilder("BHL2.structure.B", MetaIdFactory.conceptId(0xd9c7536e76b5498fL, 0x80640955dd8aebcbL, 0x6ab2e61d35e46065L)).super_("BHL2.structure.A").version(1).super_(MetaIdFactory.conceptId(0xd9c7536e76b5498fL, 0x80640955dd8aebcbL, 0x6ab2e61d35e45c5dL)).parents("BHL2.structure.A").parentIds(MetaIdFactory.conceptId(0xd9c7536e76b5498fL, 0x80640955dd8aebcbL, 0x6ab2e61d35e45c5dL)).sourceNode(new SNodePointer("r:e2b20dd3-debc-4be6-b7ff-17ade77d9a0f(BHL2.structure)", "7688460527007457381")).create();
  }
}

package ref;

/*Generated by MPS */

import jetbrains.mps.smodel.language.LanguageRuntime;
import jetbrains.mps.smodel.adapter.ids.SLanguageId;
import java.util.UUID;
import jetbrains.mps.smodel.runtime.ILanguageAspect;
import jetbrains.mps.openapi.editor.descriptor.EditorAspectDescriptor;
import ref.editor.EditorAspectDescriptorImpl;
import jetbrains.mps.lang.migration.runtime.base.MigrationAspectDescriptor;
import ref.migration.MigrationDescriptor;
import jetbrains.mps.smodel.runtime.StructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptPresentationAspect;
import ref.structure.ConceptPresentationAspectImpl;

public class Language extends LanguageRuntime {
  public static String MODULE_REF = "d3d2b6e3-a4b3-43d5-bb29-420d39fa86ab(ref)";
  public Language() {
  }
  @Override
  public String getNamespace() {
    return "ref";
  }

  @Override
  public int getVersion() {
    return 2;
  }

  public SLanguageId getId() {
    return new SLanguageId(UUID.fromString("d3d2b6e3-a4b3-43d5-bb29-420d39fa86ab"));
  }
  @Override
  protected String[] getExtendedLanguageIDs() {
    return new String[]{"decl"};
  }
  @Override
  protected <T extends ILanguageAspect> T createAspect(Class<T> aspectClass) {
    if (aspectClass.getName().equals("jetbrains.mps.openapi.editor.descriptor.EditorAspectDescriptor")) {
      if (aspectClass == EditorAspectDescriptor.class) {
        return (T) new EditorAspectDescriptorImpl();
      }
    }
    if (aspectClass.getName().equals("jetbrains.mps.lang.migration.runtime.base.MigrationAspectDescriptor")) {
      if (aspectClass == MigrationAspectDescriptor.class) {
        return (T) new MigrationDescriptor();
      }
    }
    if (aspectClass.getName().equals("jetbrains.mps.smodel.runtime.StructureAspectDescriptor")) {
      if (aspectClass == StructureAspectDescriptor.class) {
        return (T) new ref.structure.StructureAspectDescriptor();
      }
    }
    if (aspectClass.getName().equals("jetbrains.mps.smodel.runtime.ConceptPresentationAspect")) {
      if (aspectClass == ConceptPresentationAspect.class) {
        return (T) new ConceptPresentationAspectImpl();
      }
    }
    return super.createAspect(aspectClass);
  }
}

package jetbrains.mps.ide.refactoring;

/*Generated by MPS */

import jetbrains.mps.ide.platform.refactoring.RefactoringDialog;
import jetbrains.mps.workbench.goTo.ui.ChooseByNamePanel;
import jetbrains.mps.project.MPSProject;
import java.util.List;
import org.jetbrains.mps.openapi.module.SModuleReference;
import org.jetbrains.annotations.NotNull;
import jetbrains.mps.workbench.choose.ChooseByNameData;
import jetbrains.mps.workbench.choose.ModulesPresentation;
import jetbrains.mps.workbench.goTo.ui.MpsPopupFactory;
import com.intellij.ide.util.gotoByName.ChooseByNamePopupComponent;
import com.intellij.openapi.application.ModalityState;
import javax.swing.JComponent;
import org.jetbrains.annotations.Nullable;

public class ChooseModuleDialog extends RefactoringDialog {
  protected static String REFACTORING_NAME = "Move Model";
  private ChooseByNamePanel myChooser;
  private MPSProject myMPSProject;
  private List<SModuleReference> myModules;
  private SModuleReference selectedModule;
  public ChooseModuleDialog(@NotNull MPSProject mpsProject, List<SModuleReference> modules) {
    super(mpsProject.getProject(), true);
    myMPSProject = mpsProject;
    myModules = modules;
    setTitle(ChooseModuleDialog.REFACTORING_NAME);
    init();
  }
  @Override
  protected void init() {
    setModal(true);
    setHorizontalStretch(2.5f);
    setVerticalStretch(2);

    ChooseByNameData<SModuleReference> gotoData = new ChooseByNameData(new ModulesPresentation(myMPSProject.getRepository()));
    gotoData.derivePrompts("module").setCheckBoxName(null).setScope(myModules, null);
    myChooser = MpsPopupFactory.createPanelForPackage(getProject(), gotoData, false);
    myChooser.invoke(new ChooseByNamePopupComponent.Callback() {
      @Override
      public void elementChosen(Object p0) {
        selectedModule = (SModuleReference) p0;
        doRefactoringAction();
      }
    }, ModalityState.stateForComponent(getWindow()), false);
    super.init();
  }
  @Override
  public JComponent getPreferredFocusedComponent() {
    return myChooser.getPreferredFocusedComponent();
  }
  @Nullable
  @Override
  protected JComponent createCenterPanel() {
    return myChooser.getPanel();
  }
  public static SModuleReference getSelectedModule(@NotNull MPSProject project, List<SModuleReference> modules) {
    final ChooseModuleDialog dialog = new ChooseModuleDialog(project, modules);
    dialog.show();
    return dialog.selectedModule;
  }
}

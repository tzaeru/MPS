﻿Custom aspects

==============


This sample illustrates definition of custom aspects, as documented at
https://confluence.jetbrains.com/display/MPSD34/Custom+language+aspect+cookbook.

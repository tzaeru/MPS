package jetbrains.mps.checkers;

/*Generated by MPS */

import org.jetbrains.mps.openapi.model.SNodeReference;
import jetbrains.mps.smodel.runtime.CheckingNodeContext;
import org.jetbrains.mps.openapi.model.SNode;
import org.jetbrains.mps.openapi.module.SRepository;
import org.jetbrains.mps.openapi.language.SConcept;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.smodel.runtime.ConstraintsDescriptor;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.baseLanguage.closures.runtime._FunctionTypes;
import jetbrains.mps.smodel.constraints.ModelConstraints;
import org.jetbrains.mps.openapi.model.SModel;
import jetbrains.mps.baseLanguage.closures.runtime.Wrappers;
import jetbrains.mps.smodel.search.ConceptAndSuperConceptsScope;
import java.util.List;
import org.jetbrains.mps.util.Condition;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.smodel.PropertySupport;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import org.apache.log4j.Level;
import org.jetbrains.mps.openapi.model.SNodeAccessUtil;
import jetbrains.mps.smodel.adapter.MetaAdapterByDeclaration;
import jetbrains.mps.smodel.runtime.PropertyConstraintsDescriptor;
import jetbrains.mps.errors.messageTargets.PropertyMessageTarget;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

public class ConstraintsChecker extends AbstractConstraintsChecker {
  public ConstraintsChecker() {
  }

  /**
   * XXX does anyone understand what's going on here? 'breaking node' suggests it's the place
   * in user model (the one we check), while use of this pointer, as 'ruleNode' in LanguageErrorsComponent.addError
   * makes me think it shall point to 'meta' object, rule/intention/constraint which caused the error.
   */
  private SNodeReference getBreakingNodeAndClearContext(CheckingNodeContext checkingNodeContext) {
    SNodeReference breakingNodePointer = checkingNodeContext.getBreakingNode();
    checkingNodeContext.setBreakingNode(null);
    return breakingNodePointer;

  }
  @Override
  public void checkNode(final SNode node, LanguageErrorsComponent component, SRepository repository) {
    final SConcept nodeConcept = node.getConcept();
    final SNode nodeConceptNode = SNodeOperations.getConceptDeclaration(node);
    ConstraintsDescriptor newDescriptor = ConceptRegistry.getInstance().getConstraintsDescriptor(nodeConcept);
    final CheckingNodeContext checkingNodeContext = new jetbrains.mps.smodel.runtime.impl.CheckingNodeContext();

    if (SNodeOperations.getParent(node) != null) {
      component.addDependency(SNodeOperations.getParent(node));
    }
    if (SNodeOperations.getParent(node) != null && SNodeOperations.getParent(node).getConcept().isValid()) {
      final SNode link = SNodeOperations.getContainingLinkDeclaration(node);
      if (link == null) {
        component.addError(node, "Incorrect child role used: LinkDeclaration with role \"" + SNodeOperations.getContainingLinkRole(node) + "\" was not found in parent node's concept: " + SNodeOperations.getConcept(SNodeOperations.getParent(node)).getQualifiedName(), null);
        return;
      }
      final SNode parent = SNodeOperations.getParent(node);
      boolean canBeChild = component.runCheckingAction(new _FunctionTypes._return_P0_E0<Boolean>() {
        public Boolean invoke() {
          return ModelConstraints.canBeChild(nodeConcept, parent, link, node, checkingNodeContext);
        }
      });
      if (!(canBeChild)) {
        SNodeReference rule = getBreakingNodeAndClearContext(checkingNodeContext);
        component.addError(node, "Node " + node + " cannot be child of node " + SNodeOperations.getParent(node), rule);
      }
    }

    if (jetbrains.mps.util.SNodeOperations.isRoot(node)) {
      final SModel model = SNodeOperations.getModel(node);
      boolean canBeRoot = component.runCheckingAction(new _FunctionTypes._return_P0_E0<Boolean>() {
        public Boolean invoke() {
          return ModelConstraints.canBeRoot(nodeConcept, model);
        }
      });
      if (!(canBeRoot)) {
        SNodeReference rule = getBreakingNodeAndClearContext(checkingNodeContext);
        component.addError(node, "Not rootable concept added as root", rule);
      }
    }
    if (!(SNodeOperations.getConcept(node).isValid())) {
      component.addError(node, "Concept of a node was not found", null);
    }

    for (final SNode child : SNodeOperations.getChildren(node)) {
      final SNode childConcept = SNodeOperations.getConceptDeclaration(child);
      final SNode childLink = SNodeOperations.getContainingLinkDeclaration(child);
      if (childConcept == null || childLink == null) {
        continue;
      }
      boolean canBeParent = component.runCheckingAction(new _FunctionTypes._return_P0_E0<Boolean>() {
        public Boolean invoke() {
          return ModelConstraints.canBeParent(node, childConcept, childLink, child, checkingNodeContext);
        }
      });
      if (!(canBeParent)) {
        SNodeReference rule = getBreakingNodeAndClearContext(checkingNodeContext);
        component.addError(node, "Node " + node + " cannot be parent of node " + child, rule);
      }
    }

    if (nodeConceptNode != null) {
      for (final Wrappers._T<SNode> ancestor = new Wrappers._T<SNode>(SNodeOperations.getParent(node)); ancestor.value != null; ancestor.value = SNodeOperations.getParent(ancestor.value)) {
        boolean canBeAncestor = component.runCheckingAction(new _FunctionTypes._return_P0_E0<Boolean>() {
          public Boolean invoke() {
            return ModelConstraints.canBeAncestorDirect(ancestor.value, node, nodeConceptNode, checkingNodeContext);
          }
        });
        if (!(canBeAncestor)) {
          SNodeReference rule = getBreakingNodeAndClearContext(checkingNodeContext);
          component.addError(node, "Bad ancestor for node " + node, rule);
        }
      }
    }

    // Properties validation 
    component.addDependency(nodeConceptNode);
    ConceptAndSuperConceptsScope chs = new ConceptAndSuperConceptsScope(nodeConceptNode);
    for (SNode parentConcept : chs.getConcepts()) {
      component.addDependency(parentConcept);
    }
    List<SNode> props = ((List<SNode>) chs.getNodes(new Condition<SNode>() {
      public boolean met(SNode n) {
        return SNodeOperations.isInstanceOf(n, MetaAdapterFactory.getConcept(0xc72da2b97cce4447L, 0x8389f407dc1158b7L, 0xf979bd086bL, "jetbrains.mps.lang.structure.structure.PropertyDeclaration"));
      }
    }));
    for (SNode p : ListSequence.fromList(props)) {
      final PropertySupport ps = PropertySupport.getPropertySupport(p);
      final String propertyName = SPropertyOperations.getString(p, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"));
      if (propertyName == null) {
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Property declaration has a null name, declaration id: " + p.getNodeId() + ", model: " + SNodeOperations.getModel(p).getReference().getModelName());
        }
        continue;
      }
      final String value = ps.fromInternalValue(SNodeAccessUtil.getProperty(node, MetaAdapterByDeclaration.getProperty(p)));
      final PropertyConstraintsDescriptor propertyDescriptor = newDescriptor.getProperty(propertyName);
      boolean canSetValue = (propertyDescriptor == null ? false : component.runCheckingAction(new _FunctionTypes._return_P0_E0<Boolean>() {
        public Boolean invoke() {
          return ps.canSetValue(propertyDescriptor, node, propertyName, value);
        }
      }));
      if (!(canSetValue)) {
        // TODO this is a hack for anonymous classes 
        if ("name".equals(SPropertyOperations.getString(p, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"))) && ("AnonymousClass".equals(SPropertyOperations.getString(nodeConceptNode, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"))) || "InternalAnonymousClass".equals(SPropertyOperations.getString(nodeConceptNode, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"))))) {
          continue;
        }
        // todo find a rule 
        component.addError(node, "Property constraint violation for property \"" + SPropertyOperations.getString(p, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name")) + "\"", null, new PropertyMessageTarget(SPropertyOperations.getString(p, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"))));
      }
    }
  }
  protected static Logger LOG = LogManager.getLogger(ConstraintsChecker.class);
}

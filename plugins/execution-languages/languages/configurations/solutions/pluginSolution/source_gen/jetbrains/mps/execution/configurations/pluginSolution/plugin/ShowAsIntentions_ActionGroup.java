package jetbrains.mps.execution.configurations.pluginSolution.plugin;

/*Generated by MPS */

import jetbrains.mps.plugins.actions.GeneratedActionGroup;

public class ShowAsIntentions_ActionGroup extends GeneratedActionGroup {
  public static final String ID = "jetbrains.mps.execution.configurations.pluginSolution.plugin.ShowAsIntentions_ActionGroup";
  public ShowAsIntentions_ActionGroup() {
    super("ShowAsIntentions", ID);
    this.setIsInternal(false);
    this.setPopup(false);
    ShowAsIntentions_ActionGroup.this.addAction("jetbrains.mps.execution.configurations.pluginSolution.plugin.AddProducer_Action");
    ShowAsIntentions_ActionGroup.this.addAction("jetbrains.mps.execution.configurations.pluginSolution.plugin.AddExecutor_Action");
  }
}

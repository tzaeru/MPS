package jetbrains.mps.samples.highlevel.simpleEditor.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import java.util.Map;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import jetbrains.mps.smodel.SNodePointer;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  private final Map<SConceptId, Integer> myIndexMap = new HashMap<SConceptId, Integer>(3);
  /*package*/ final ConceptDescriptor myConceptEditorAspectDeclaration = createDescriptorForEditorAspectDeclaration();
  /*package*/ final ConceptDescriptor myConceptSimpleEditorDeclaration = createDescriptorForSimpleEditorDeclaration();
  /*package*/ final ConceptDescriptor myConceptSimplePropertyReference = createDescriptorForSimplePropertyReference();

  public StructureAspectDescriptor() {
    myIndexMap.put(myConceptEditorAspectDeclaration.getId(), 0);
    myIndexMap.put(myConceptSimpleEditorDeclaration.getId(), 1);
    myIndexMap.put(myConceptSimplePropertyReference.getId(), 2);
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptEditorAspectDeclaration, myConceptSimpleEditorDeclaration, myConceptSimplePropertyReference);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    Integer index = myIndexMap.get(id);
    if (index == null) {
      return null;
    }
    switch (((int) index)) {
      case 0:
        return myConceptEditorAspectDeclaration;
      case 1:
        return myConceptSimpleEditorDeclaration;
      case 2:
        return myConceptSimplePropertyReference;
      default:
        throw new IllegalStateException();
    }
  }

  private static ConceptDescriptor createDescriptorForEditorAspectDeclaration() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.highlevel.simpleEditor.structure.EditorAspectDeclaration", MetaIdFactory.conceptId(0xc457c5de60274104L, 0xab9ca31c5404ae8bL, 0x346ead2c08cc7fbbL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x346ead2c08ccf70cL, "editors", MetaIdFactory.conceptId(0xc457c5de60274104L, 0xab9ca31c5404ae8bL, 0x346ead2c08ccf6ffL), true, true, false, new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048241420"))).children(new String[]{"editors"}, new boolean[]{true}).rootable().sourceNode(new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048210875")).create();
  }
  private static ConceptDescriptor createDescriptorForSimpleEditorDeclaration() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.highlevel.simpleEditor.structure.SimpleEditorDeclaration", MetaIdFactory.conceptId(0xc457c5de60274104L, 0xab9ca31c5404ae8bL, 0x346ead2c08ccf6ffL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x346ead2c08ccfa54L, "caption", new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048242260"))).properties("caption").referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x346ead2c08ccf9f3L, "cncpt", MetaIdFactory.conceptId(0xbaa9238b36da4ccbL, 0xa76b8ad70e222183L, 0x460f3ddb176b0a3cL), false, new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048242163"))).references("cncpt").childDescriptors(new ConceptDescriptorBuilder.Link(0x346ead2c08ccfa0dL, "visibleProperties", MetaIdFactory.conceptId(0xc457c5de60274104L, 0xab9ca31c5404ae8bL, 0x346ead2c08ccfa10L), true, true, false, new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048242189"))).children(new String[]{"visibleProperties"}, new boolean[]{true}).sourceNode(new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048241407")).create();
  }
  private static ConceptDescriptor createDescriptorForSimplePropertyReference() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.highlevel.simpleEditor.structure.SimplePropertyReference", MetaIdFactory.conceptId(0xc457c5de60274104L, 0xab9ca31c5404ae8bL, 0x346ead2c08ccfa10L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x346ead2c08ccfa1fL, "prop", MetaIdFactory.conceptId(0xbaa9238b36da4ccbL, 0xa76b8ad70e222183L, 0x346ead2c08cc7faeL), false, new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048242207"))).references("prop").sourceNode(new SNodePointer("r:64c71df5-2adc-4e3d-8424-aa08c0bc782f(jetbrains.mps.samples.highlevel.simpleEditor.structure)", "3778147542048242192")).create();
  }
}

package jetbrains.mps.lang.editor.menus.contextAssistant.testExtendingLanguage.editor;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.IconResource;

public class IconContainer {
  protected static IconResource RESOURCE_a0a2e6 = new IconResource("refactoring.png", IconContainer.class);
  protected static IconResource RESOURCE_a0a2e7 = new IconResource("action.png", IconContainer.class);
  protected static IconResource RESOURCE_a0a8c7 = new IconResource("action.png", IconContainer.class);
}

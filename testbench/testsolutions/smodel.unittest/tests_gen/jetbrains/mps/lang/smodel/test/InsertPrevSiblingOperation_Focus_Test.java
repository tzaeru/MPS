package jetbrains.mps.lang.smodel.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class InsertPrevSiblingOperation_Focus_Test extends BaseTransformationTest {
  @Test
  public void test_InsertPrevSiblingOperation_Focus() throws Throwable {
    initTest("${mps_home}", "r:3deabf90-227b-4dd7-a1b3-e4735e4a0270(jetbrains.mps.lang.smodel.test)");
    runTest("jetbrains.mps.lang.smodel.test.InsertPrevSiblingOperation_Focus_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("1835794636205189194", "1835794636205189199");
      typeString("node.add next-sibling");
      typeString("new");
    }
  }
}

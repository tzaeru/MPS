package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class AddStatementToBlock_Test extends BaseTransformationTest {
  @Test
  public void test_AddStatementToBlock() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.AddStatementToBlock_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("8777381699079423281", "8777381699079423291");
      invokeAction("$Copy");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("$Paste");

    }
  }
}

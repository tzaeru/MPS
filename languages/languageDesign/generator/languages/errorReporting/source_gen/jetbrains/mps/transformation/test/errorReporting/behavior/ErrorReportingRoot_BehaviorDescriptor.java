package jetbrains.mps.transformation.test.errorReporting.behavior;

/*Generated by MPS */


/**
 * Will be removed after 3.4
 * Need to support compilation of the legacy behavior descriptors before the language is rebuilt
 * This class is not involved in the actual method invocation
 */
@Deprecated
public class ErrorReportingRoot_BehaviorDescriptor {
  public String getConceptFqName() {
    return null;
  }
}

package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class SwapTernaryBranches3_Test extends BaseTransformationTest {
  @Test
  public void test_SwapTernaryBranches3() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.SwapTernaryBranches3_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("6866160649815090465", "6866160649825663987");
      invokeIntention("jetbrains.mps.baseLanguage.intentions.SwapTernaryBranches_Intention", myStart.getNode());
    }
  }
}

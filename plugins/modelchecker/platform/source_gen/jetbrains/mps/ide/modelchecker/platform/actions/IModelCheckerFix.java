package jetbrains.mps.ide.modelchecker.platform.actions;

/*Generated by MPS */


public interface IModelCheckerFix {
  boolean doFix();
}

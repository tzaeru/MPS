package jetbrains.mps.console.base.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.openapi.editor.style.Style;
import jetbrains.mps.editor.runtime.style.StyleImpl;
import jetbrains.mps.nodeEditor.cells.EditorCell_Constant;
import jetbrains.mps.editor.runtime.style.StyleAttributes;
import jetbrains.mps.nodeEditor.MPSFonts;
import jetbrains.mps.lang.editor.cellProviders.SingleRoleCellProvider;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import jetbrains.mps.nodeEditor.cellMenu.OldNewCompositeSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.SChildSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.DefaultChildSubstituteInfo;
import jetbrains.mps.lang.editor.menus.substitute.DefaultEmptyCellSubstituteMenuLookup;

public class ConsoleRoot_Editor extends DefaultNodeEditor {
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createCollection_drvoix_a(editorContext, node);
  }
  private EditorCell createCollection_drvoix_a(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createVertical(editorContext, node);
    editorCell.setCellId("Collection_drvoix_a");
    editorCell.setBig(true);
    Style style = new StyleImpl();
    {
      Style styleToPut = new StyleImpl();
      ConsoleStyle_StyleSheet.apply_ReadOnly(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.addEditorCell(this.createConstant_drvoix_a0(editorContext, node));
    editorCell.addEditorCell(this.createConstant_drvoix_b0(editorContext, node));
    editorCell.addEditorCell(this.createConstant_drvoix_c0(editorContext, node));
    editorCell.addEditorCell(this.createConstant_drvoix_d0(editorContext, node));
    editorCell.addEditorCell(this.createRefNode_drvoix_e0(editorContext, node));
    editorCell.addEditorCell(this.createRefNode_drvoix_f0(editorContext, node));
    return editorCell;
  }
  private EditorCell createConstant_drvoix_a0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Type an expression or {statements} to execute.");
    editorCell.setCellId("Constant_drvoix_a0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.FONT_STYLE, 0, MPSFonts.PLAIN);
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_drvoix_b0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Type ? for a list of commands.");
    editorCell.setCellId("Constant_drvoix_b0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.FONT_STYLE, 0, MPSFonts.PLAIN);
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_drvoix_c0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Press Ctrl+Enter to execute command.");
    editorCell.setCellId("Constant_drvoix_c0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.FONT_STYLE, 0, MPSFonts.PLAIN);
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_drvoix_d0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Use Ctrl+M, Ctrl+R and Ctrl+L to add imports and languages.");
    editorCell.setCellId("Constant_drvoix_d0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.FONT_STYLE, 0, MPSFonts.PLAIN);
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createRefNode_drvoix_e0(EditorContext editorContext, SNode node) {
    SingleRoleCellProvider provider = new ConsoleRoot_Editor.historySingleRoleHandler_drvoix_e0(node, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bafL, "history"), editorContext);
    return provider.createCell();
  }
  private class historySingleRoleHandler_drvoix_e0 extends SingleRoleCellProvider {
    public historySingleRoleHandler_drvoix_e0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(ownerNode, containmentLink, context);
    }
    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = super.createChildCell(child);
      installCellInfo(child, editorCell);
      return editorCell;
    }
    private void installCellInfo(SNode child, EditorCell editorCell) {
      editorCell.setSubstituteInfo(new OldNewCompositeSubstituteInfo(myEditorContext, new SChildSubstituteInfo(editorCell, myOwnerNode, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bafL, "history"), child), new DefaultChildSubstituteInfo(myOwnerNode, myContainmentLink.getDeclarationNode(), myEditorContext)));
      if (editorCell.getRole() == null) {
        editorCell.setRole("history");
      }
    }
    @Override
    protected EditorCell createEmptyCell() {
      EditorCell editorCell = super.createEmptyCell();
      editorCell.setCellId("empty_history");
      installCellInfo(null, editorCell);
      editorCell.setTransformationMenuLookup(new DefaultEmptyCellSubstituteMenuLookup(MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bafL, "history")));

      return editorCell;
    }
    protected String getNoTargetText() {
      return "<no history>";
    }
  }
  private EditorCell createRefNode_drvoix_f0(EditorContext editorContext, SNode node) {
    SingleRoleCellProvider provider = new ConsoleRoot_Editor.commandHolderSingleRoleHandler_drvoix_f0(node, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bb1L, "commandHolder"), editorContext);
    return provider.createCell();
  }
  private class commandHolderSingleRoleHandler_drvoix_f0 extends SingleRoleCellProvider {
    public commandHolderSingleRoleHandler_drvoix_f0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(ownerNode, containmentLink, context);
    }
    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = super.createChildCell(child);
      installCellInfo(child, editorCell);
      return editorCell;
    }
    private void installCellInfo(SNode child, EditorCell editorCell) {
      editorCell.setSubstituteInfo(new OldNewCompositeSubstituteInfo(myEditorContext, new SChildSubstituteInfo(editorCell, myOwnerNode, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bb1L, "commandHolder"), child), new DefaultChildSubstituteInfo(myOwnerNode, myContainmentLink.getDeclarationNode(), myEditorContext)));
      if (editorCell.getRole() == null) {
        editorCell.setRole("commandHolder");
      }
      Style style = new StyleImpl();
      SNode node = myOwnerNode;
      EditorContext editorContext = myEditorContext;
      {
        Style styleToPut = new StyleImpl();
        ConsoleStyle_StyleSheet.apply_ReadOnly(styleToPut, editorCell);
        style.removeAll(styleToPut);
      }
      editorCell.getStyle().putAll(style);
    }
    @Override
    protected EditorCell createEmptyCell() {
      EditorCell editorCell = super.createEmptyCell();
      editorCell.setCellId("empty_commandHolder");
      installCellInfo(null, editorCell);
      editorCell.setTransformationMenuLookup(new DefaultEmptyCellSubstituteMenuLookup(MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x15fb34051f725a2cL, 0x15fb34051f725bb1L, "commandHolder")));

      return editorCell;
    }
    protected String getNoTargetText() {
      return "<no commandHolder>";
    }
  }
}

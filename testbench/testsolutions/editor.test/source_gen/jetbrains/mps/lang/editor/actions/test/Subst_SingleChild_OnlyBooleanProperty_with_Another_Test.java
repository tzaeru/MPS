package jetbrains.mps.lang.editor.actions.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class Subst_SingleChild_OnlyBooleanProperty_with_Another_Test extends BaseTransformationTest {
  @Test
  public void test_Subst_SingleChild_OnlyBooleanProperty_with_Another() throws Throwable {
    initTest("${mps_home}", "r:c44f4b8c-137c-4225-8bd9-38d232a9b736(jetbrains.mps.lang.editor.actions.test)");
    runTest("jetbrains.mps.lang.editor.actions.test.Subst_SingleChild_OnlyBooleanProperty_with_Another_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("2890539347285262654", "2890539347285262656");
      typeString("v1");
      invokeAction("jetbrains.mps.ide.editor.actions.Complete_Action");
    }
  }
}

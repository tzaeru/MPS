package jetbrains.mps.lang.core.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.internal.collections.runtime.Sequence;
import java.util.List;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import java.util.ArrayList;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public class NodeAttribute_comment_Editor extends DefaultNodeEditor {
  private Collection<String> myContextHints = Arrays.asList(new String[]{"jetbrains.mps.lang.core.editor.BaseEditorContextHints.comment"});
  @Override
  @NotNull
  public Collection<String> getContextHints() {
    return myContextHints;
  }
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createNextEditor_fqq7dl_a(editorContext, node);
  }
  public EditorCell createInspectedCell(EditorContext editorContext, SNode node) {
    return this.createNextEditor_fqq7dl_a_0(editorContext, node);
  }
  private EditorCell createNextEditor_fqq7dl_a(EditorContext editorContext, SNode node) {
    try {
      editorContext.getCellFactory().pushCellContext();
      editorContext.getCellFactory().removeCellContextHints(Sequence.fromIterable(getEditorHints_fqq7dl_a0(node, editorContext)).toGenericArray(String.class));
      {
        EditorCell editorCell = editorContext.getCellFactory().createEditorCell(node, false, NodeAttribute_comment_Editor.class);
        editorCell.setBig(true);
        return editorCell;
      }
    } finally {
      editorContext.getCellFactory().popCellContext();
    }
  }
  private Iterable<String> getEditorHints_fqq7dl_a0(SNode node, EditorContext editorContext) {
    List<String> result = ListSequence.fromList(new ArrayList<String>());
    if (check_fqq7dl_a1a5(node.getContainmentLink())) {
      ListSequence.fromList(result).addElement("jetbrains.mps.lang.core.editor.BaseEditorContextHints.comment");
    }
    return result;
  }
  private EditorCell createNextEditor_fqq7dl_a_0(EditorContext editorContext, SNode node) {
    try {
      editorContext.getCellFactory().pushCellContext();
      editorContext.getCellFactory().removeCellContextHints(Sequence.fromIterable(getEditorHints_fqq7dl_a0_0(node, editorContext)).toGenericArray(String.class));
      {
        EditorCell editorCell = editorContext.getCellFactory().createEditorCell(node, true, NodeAttribute_comment_Editor.class);
        editorCell.setBig(true);
        return editorCell;
      }
    } finally {
      editorContext.getCellFactory().popCellContext();
    }
  }
  private Iterable<String> getEditorHints_fqq7dl_a0_0(SNode node, EditorContext editorContext) {
    List<String> result = ListSequence.fromList(new ArrayList<String>());
    if (check_fqq7dl_a1a7(node.getContainmentLink())) {
      ListSequence.fromList(result).addElement("jetbrains.mps.lang.core.editor.BaseEditorContextHints.comment");
    }
    return result;
  }
  private static boolean check_fqq7dl_a1a5(SContainmentLink checkedDotOperand) {
    if (null != checkedDotOperand) {
      return checkedDotOperand.equals(MetaAdapterFactory.getContainmentLink(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, 0x47bf8397520e5942L, "smodelAttribute"));
    }
    return false;
  }
  private static boolean check_fqq7dl_a1a7(SContainmentLink checkedDotOperand) {
    if (null != checkedDotOperand) {
      return checkedDotOperand.equals(MetaAdapterFactory.getContainmentLink(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, 0x47bf8397520e5942L, "smodelAttribute"));
    }
    return false;
  }
}

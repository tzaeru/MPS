package jetbrains.mps.lang.structure.pluginSolution.plugin;

/*Generated by MPS */

import jetbrains.mps.plugins.actions.GeneratedActionGroup;

public class RefactoringAdditions_ActionGroup extends GeneratedActionGroup {
  public static final String ID = "jetbrains.mps.lang.structure.pluginSolution.plugin.RefactoringAdditions_ActionGroup";
  public RefactoringAdditions_ActionGroup() {
    super("RefactoringAdditions", ID);
    this.setIsInternal(false);
    this.setPopup(false);
    RefactoringAdditions_ActionGroup.this.addAction("jetbrains.mps.lang.structure.pluginSolution.plugin.RenameConcept_Action");
    RefactoringAdditions_ActionGroup.this.addAction("jetbrains.mps.lang.structure.pluginSolution.plugin.RenameLink_Action");
    RefactoringAdditions_ActionGroup.this.addAction("jetbrains.mps.lang.structure.pluginSolution.plugin.RenameProperty_Action");
  }
}

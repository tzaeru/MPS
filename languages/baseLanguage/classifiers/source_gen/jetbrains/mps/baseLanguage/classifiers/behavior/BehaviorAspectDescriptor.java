package jetbrains.mps.baseLanguage.classifiers.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBehaviorAspectDescriptor;
import jetbrains.mps.core.aspects.behaviour.api.BHDescriptor;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Map;
import java.util.HashMap;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public final class BehaviorAspectDescriptor extends BaseBehaviorAspectDescriptor {
  private final BHDescriptor myIClassifier__BehaviorDescriptor = new IClassifier__BehaviorDescriptor();
  private final BHDescriptor myIMember__BehaviorDescriptor = new IMember__BehaviorDescriptor();
  private final BHDescriptor myIClassifierPart__BehaviorDescriptor = new IClassifierPart__BehaviorDescriptor();
  private final BHDescriptor myThisClassifierExpression__BehaviorDescriptor = new ThisClassifierExpression__BehaviorDescriptor();
  private final BHDescriptor myBaseClassifierType__BehaviorDescriptor = new BaseClassifierType__BehaviorDescriptor();
  private final BHDescriptor myDefaultClassifierType__BehaviorDescriptor = new DefaultClassifierType__BehaviorDescriptor();
  private final BHDescriptor myIMemberOperation__BehaviorDescriptor = new IMemberOperation__BehaviorDescriptor();
  private final BHDescriptor myDefaultClassifierMethodDeclaration__BehaviorDescriptor = new DefaultClassifierMethodDeclaration__BehaviorDescriptor();
  private final BHDescriptor myDefaultClassifierFieldDeclaration__BehaviorDescriptor = new DefaultClassifierFieldDeclaration__BehaviorDescriptor();
  private final BHDescriptor myDefaultClassifierFieldAccessOperation__BehaviorDescriptor = new DefaultClassifierFieldAccessOperation__BehaviorDescriptor();
  private final BHDescriptor mySuperClassifierExpresson__BehaviorDescriptor = new SuperClassifierExpresson__BehaviorDescriptor();

  public BehaviorAspectDescriptor() {
  }

  @Nullable
  public BHDescriptor getDescriptor(@NotNull SAbstractConcept concept) {
    {
      SAbstractConcept cncpt = concept;
      Integer preIndex = indices_846f5o_a0o.get(cncpt);
      int switchIndex = (preIndex == null ? -1 : preIndex);
      switch (switchIndex) {
        case 0:
          if (true) {
            return myBaseClassifierType__BehaviorDescriptor;
          }
          break;
        case 1:
          if (true) {
            return myDefaultClassifierFieldAccessOperation__BehaviorDescriptor;
          }
          break;
        case 2:
          if (true) {
            return myDefaultClassifierFieldDeclaration__BehaviorDescriptor;
          }
          break;
        case 3:
          if (true) {
            return myDefaultClassifierMethodDeclaration__BehaviorDescriptor;
          }
          break;
        case 4:
          if (true) {
            return myDefaultClassifierType__BehaviorDescriptor;
          }
          break;
        case 5:
          if (true) {
            return myIClassifier__BehaviorDescriptor;
          }
          break;
        case 6:
          if (true) {
            return myIClassifierPart__BehaviorDescriptor;
          }
          break;
        case 7:
          if (true) {
            return myIMember__BehaviorDescriptor;
          }
          break;
        case 8:
          if (true) {
            return myIMemberOperation__BehaviorDescriptor;
          }
          break;
        case 9:
          if (true) {
            return mySuperClassifierExpresson__BehaviorDescriptor;
          }
          break;
        case 10:
          if (true) {
            return myThisClassifierExpression__BehaviorDescriptor;
          }
          break;
        default:
          // default 
      }
    }
    return null;
  }
  private static Map<SAbstractConcept, Integer> buildConceptIndices(SAbstractConcept... concepts) {
    HashMap<SAbstractConcept, Integer> res = new HashMap<SAbstractConcept, Integer>();
    int counter = 0;
    for (SAbstractConcept c : concepts) {
      res.put(c, counter++);
    }
    return res;
  }
  private static final Map<SAbstractConcept, Integer> indices_846f5o_a0o = buildConceptIndices(MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc77d845L, "jetbrains.mps.baseLanguage.classifiers.structure.BaseClassifierType"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x11aa7fc7570L, "jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierFieldAccessOperation"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x11aa7fc0293L, "jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierFieldDeclaration"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bd6ee3c3L, "jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierMethodDeclaration"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc7942feL, "jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierType"), MetaAdapterFactory.getInterfaceConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc6b2af5L, "jetbrains.mps.baseLanguage.classifiers.structure.IClassifier"), MetaAdapterFactory.getInterfaceConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc6e188eL, "jetbrains.mps.baseLanguage.classifiers.structure.IClassifierPart"), MetaAdapterFactory.getInterfaceConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc6becc0L, "jetbrains.mps.baseLanguage.classifiers.structure.IMember"), MetaAdapterFactory.getInterfaceConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bca97396L, "jetbrains.mps.baseLanguage.classifiers.structure.IMemberOperation"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x11b74b0357cL, "jetbrains.mps.baseLanguage.classifiers.structure.SuperClassifierExpresson"), MetaAdapterFactory.getConcept(0x443f4c36fcf54eb6L, 0x95008d06ed259e3eL, 0x118bc751a81L, "jetbrains.mps.baseLanguage.classifiers.structure.ThisClassifierExpression"));
}

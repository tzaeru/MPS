package jetbrains.mps.baseLanguage.tuples.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import java.util.Map;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.smodel.runtime.StaticScope;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  private final Map<SConceptId, Integer> myIndexMap = new HashMap<SConceptId, Integer>(9);
  /*package*/ final ConceptDescriptor myConceptIndexedTupleLiteral = createDescriptorForIndexedTupleLiteral();
  /*package*/ final ConceptDescriptor myConceptIndexedTupleMemberAccessExpression = createDescriptorForIndexedTupleMemberAccessExpression();
  /*package*/ final ConceptDescriptor myConceptIndexedTupleType = createDescriptorForIndexedTupleType();
  /*package*/ final ConceptDescriptor myConceptNamedTupleComponentAccessOperation = createDescriptorForNamedTupleComponentAccessOperation();
  /*package*/ final ConceptDescriptor myConceptNamedTupleComponentDeclaration = createDescriptorForNamedTupleComponentDeclaration();
  /*package*/ final ConceptDescriptor myConceptNamedTupleComponentReference = createDescriptorForNamedTupleComponentReference();
  /*package*/ final ConceptDescriptor myConceptNamedTupleDeclaration = createDescriptorForNamedTupleDeclaration();
  /*package*/ final ConceptDescriptor myConceptNamedTupleLiteral = createDescriptorForNamedTupleLiteral();
  /*package*/ final ConceptDescriptor myConceptNamedTupleType = createDescriptorForNamedTupleType();

  public StructureAspectDescriptor() {
    myIndexMap.put(myConceptIndexedTupleLiteral.getId(), 0);
    myIndexMap.put(myConceptIndexedTupleMemberAccessExpression.getId(), 1);
    myIndexMap.put(myConceptIndexedTupleType.getId(), 2);
    myIndexMap.put(myConceptNamedTupleComponentAccessOperation.getId(), 3);
    myIndexMap.put(myConceptNamedTupleComponentDeclaration.getId(), 4);
    myIndexMap.put(myConceptNamedTupleComponentReference.getId(), 5);
    myIndexMap.put(myConceptNamedTupleDeclaration.getId(), 6);
    myIndexMap.put(myConceptNamedTupleLiteral.getId(), 7);
    myIndexMap.put(myConceptNamedTupleType.getId(), 8);
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptIndexedTupleLiteral, myConceptIndexedTupleMemberAccessExpression, myConceptIndexedTupleType, myConceptNamedTupleComponentAccessOperation, myConceptNamedTupleComponentDeclaration, myConceptNamedTupleComponentReference, myConceptNamedTupleDeclaration, myConceptNamedTupleLiteral, myConceptNamedTupleType);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    Integer index = myIndexMap.get(id);
    if (index == null) {
      return null;
    }
    switch (((int) index)) {
      case 0:
        return myConceptIndexedTupleLiteral;
      case 1:
        return myConceptIndexedTupleMemberAccessExpression;
      case 2:
        return myConceptIndexedTupleType;
      case 3:
        return myConceptNamedTupleComponentAccessOperation;
      case 4:
        return myConceptNamedTupleComponentDeclaration;
      case 5:
        return myConceptNamedTupleComponentReference;
      case 6:
        return myConceptNamedTupleDeclaration;
      case 7:
        return myConceptNamedTupleLiteral;
      case 8:
        return myConceptNamedTupleType;
      default:
        throw new IllegalStateException();
    }
  }

  private static ConceptDescriptor createDescriptorForIndexedTupleLiteral() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleLiteral", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12071708c13L)).super_("jetbrains.mps.baseLanguage.structure.Expression").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL)).parents("jetbrains.mps.baseLanguage.structure.Expression").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x1207171832eL, "component", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL), true, true, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238853845806"))).children(new String[]{"component"}, new boolean[]{true}).alias("[", "indexed tuple").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238853782547")).create();
  }
  private static ConceptDescriptor createDescriptorForIndexedTupleMemberAccessExpression() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleMemberAccessExpression", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12071acfb50L)).super_("jetbrains.mps.baseLanguage.structure.Expression").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL)).parents("jetbrains.mps.baseLanguage.structure.Expression", "jetbrains.mps.lang.core.structure.IDontSubstituteByDefault").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x19796fa16a19888bL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x12071ad5056L, "tuple", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL), false, false, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238857764950")), new ConceptDescriptorBuilder.Link(0x12071ae5facL, "index", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL), false, false, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238857834412"))).children(new String[]{"tuple", "index"}, new boolean[]{false, false}).alias("[", "access tuple member by index").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238857743184")).create();
  }
  private static ConceptDescriptor createDescriptorForIndexedTupleType() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleType", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1207157a8dcL)).super_("jetbrains.mps.baseLanguage.structure.Type").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506dL)).parents("jetbrains.mps.baseLanguage.structure.Type", "jetbrains.mps.baseLanguage.structure.IGenericType").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506dL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x38ff5220e0ac710dL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x1207158795cL, "componentType", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506dL), true, true, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238852204892"))).children(new String[]{"componentType"}, new boolean[]{true}).alias("[", "indexed tuple type").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1238852151516")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleComponentAccessOperation() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleComponentAccessOperation", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1209c84a4eaL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.baseLanguage.structure.IOperation").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x116b46ac030L)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x1209c84fd08L, "component", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12095b3e54fL), false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239576542472"))).references("component").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239576519914")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleComponentDeclaration() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleComponentDeclaration", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12095b3e54fL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.baseLanguage.structure.IValidIdentifier", "jetbrains.mps.baseLanguage.structure.HasAnnotation", "jetbrains.mps.baseLanguage.structure.TypeDerivable", "jetbrains.mps.baseLanguage.structure.TypeAnnotable", "jetbrains.mps.lang.core.structure.IResolveInfo").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x11a3afa8c0dL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x114a6be947aL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x117ac2330f4L), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x11f4b6b2435L), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x116b17c6e46L)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x120cda6c3beL, "final", new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1240400839614"))).properties("final").childDescriptors(new ConceptDescriptorBuilder.Link(0x12095c0134fL, "type", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506dL), false, false, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239462974287"))).children(new String[]{"type"}, new boolean[]{false}).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239462176079")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleComponentReference() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleComponentReference", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1209b917141L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x1209b91a766L, "componentDeclaration", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12095b3e54fL), false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239560595302"))).references("componentDeclaration").childDescriptors(new ConceptDescriptorBuilder.Link(0x1209b955a61L, "value", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL), false, false, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239560837729"))).children(new String[]{"value"}, new boolean[]{false}).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239560581441")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleDeclaration() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleDeclaration", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1208fa48aa5L)).super_("jetbrains.mps.baseLanguage.structure.Classifier").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x101d9d3ca30L)).parents("jetbrains.mps.baseLanguage.structure.Classifier", "jetbrains.mps.baseLanguage.structure.IBLDeprecatable").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x101d9d3ca30L), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x11d2ea8a339L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x12099b7fca9L, "component", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12095b3e54fL), true, true, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239529553065")), new ConceptDescriptorBuilder.Link(0x479eb1f896fa444L, "extended", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12099dc1365L), true, false, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "322547369016009796")), new ConceptDescriptorBuilder.Link(0x21a3c1a0d8708ce4L, "implements", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x101de48bf9eL), true, true, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "2423993921025641700"))).children(new String[]{"component", "extended", "implements"}, new boolean[]{true, false, true}).rootable().alias("Tuple", "Named tuple declaration").sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239360506533")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleLiteral() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleLiteral", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1209b88731cL)).super_("jetbrains.mps.baseLanguage.structure.Expression").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL)).parents("jetbrains.mps.baseLanguage.structure.Expression").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c37f506fL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x1209b88b156L, "tupleDeclaration", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1208fa48aa5L), false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239560008022"))).references("tupleDeclaration").childDescriptors(new ConceptDescriptorBuilder.Link(0x1209b9676f1L, "componentRef", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x1209b917141L), true, true, false, new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239560910577"))).children(new String[]{"componentRef"}, new boolean[]{true}).alias("<{tupleDeclaration}> literal", "tuple literal").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239559992092")).create();
  }
  private static ConceptDescriptor createDescriptorForNamedTupleType() {
    return new ConceptDescriptorBuilder("jetbrains.mps.baseLanguage.tuples.structure.NamedTupleType", MetaIdFactory.conceptId(0xa247e09e243545baL, 0xb8d207e93feba96aL, 0x12099dc1365L)).super_("jetbrains.mps.baseLanguage.structure.ClassifierType").version(1).super_(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x101de48bf9eL)).parents("jetbrains.mps.baseLanguage.structure.ClassifierType", "jetbrains.mps.baseLanguage.structure.IGenericType", "jetbrains.mps.lang.core.structure.IDontSubstituteByDefault").parentIds(MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x101de48bf9eL), MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0x38ff5220e0ac710dL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x19796fa16a19888bL)).alias("<{tupleDeclaration}> type", "reference to named tuple declaration").staticScope(StaticScope.NONE).sourceNode(new SNodePointer("r:309aeee7-bee8-445c-b31d-35928d1da75f(jetbrains.mps.baseLanguage.tuples.structure)", "1239531918181")).create();
  }
}

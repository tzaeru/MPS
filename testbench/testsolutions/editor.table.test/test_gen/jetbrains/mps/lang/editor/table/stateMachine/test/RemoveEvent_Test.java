package jetbrains.mps.lang.editor.table.stateMachine.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class RemoveEvent_Test extends BaseTransformationTest {
  @Test
  public void test_RemoveEvent() throws Throwable {
    initTest("${mps_home}", "r:dc1400b5-0aa4-448e-8f15-11fb0ccb5c23(jetbrains.mps.lang.editor.table.stateMachine.test@tests)");
    runTest("jetbrains.mps.lang.editor.table.stateMachine.test.RemoveEvent_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("5877647854348551129", "5877647854348551155");
      invokeAction("jetbrains.mps.ide.editor.actions.Backspace_Action");
    }
  }
}

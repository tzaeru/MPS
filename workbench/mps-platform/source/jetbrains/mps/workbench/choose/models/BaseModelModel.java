/*
 * Copyright 2003-2016 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jetbrains.mps.workbench.choose.models;

import com.intellij.navigation.NavigationItem;
import jetbrains.mps.project.Project;
import jetbrains.mps.util.annotation.ToRemove;
import jetbrains.mps.workbench.choose.base.BaseMPSChooseModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.model.SModel;
import org.jetbrains.mps.openapi.model.SModelName;
import org.jetbrains.mps.openapi.model.SModelReference;
import org.jetbrains.mps.openapi.module.SearchScope;

import java.util.ArrayList;

/**
 * THERE ARE NO MORE USES IN MPS, PLEASE DO NOT ADD NEW!
 * @deprecated see {@link BaseMPSChooseModel} and {@link jetbrains.mps.workbench.choose.ChooseByNameData}
 */
@Deprecated
@ToRemove(version = 3.4)
public class BaseModelModel extends BaseMPSChooseModel<SModelReference> {
  public BaseModelModel(Project mpsProject) {
    super(mpsProject, "model");
  }

  public BaseModelModel(Project mpsProject, @NotNull SearchScope localScope, @Nullable  SearchScope globalScope) {
    this(mpsProject);
    setScope(localScope, globalScope);
  }

  @Override
  public boolean willOpenEditor() {
    return false;
  }

  @Override
  public String doGetFullName(NavigationItem element) {
    SModelReference ref = getModelObject(element);
    return ref.getModelName();
  }

  @Override
  public String doGetObjectName(SModelReference ref) {
    SModelName modelName = ref.getName();
    return modelName.hasStereotype() ? modelName.getSimpleName() + '@' + modelName.getStereotype() : modelName.getSimpleName();
  }

  @Override
  public NavigationItem doGetNavigationItem(SModelReference object) {
    return new BaseModelItem(object);
  }

  @Override
  public SModelReference[] find(SearchScope scope) {
    ArrayList<SModelReference> res = new ArrayList<>();
    for (SModel m : scope.getModels()) {
      res.add(m.getReference());
    }
    return res.toArray(new SModelReference[res.size()]);
  }

  /**
   * @see jetbrains.mps.workbench.choose.nodes.BaseNodePointerModel#getModelObject(Object)
   * @see jetbrains.mps.workbench.choose.modules.BaseModuleModel#getModelObject(Object)
   */
  @Override
  public SModelReference getModelObject(Object item) {
    if (item instanceof BaseModelItem) {
      return ((BaseModelItem) item).getModelReference();
    }
    return null;
  }
}

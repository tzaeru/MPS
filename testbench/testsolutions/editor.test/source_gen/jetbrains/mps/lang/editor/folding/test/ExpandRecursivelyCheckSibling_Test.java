package jetbrains.mps.lang.editor.folding.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class ExpandRecursivelyCheckSibling_Test extends BaseTransformationTest {
  @Test
  public void test_ExpandRecursivelyCheckSibling() throws Throwable {
    initTest("${mps_home}", "r:0204c664-b836-4137-bb87-42caecd8a4e3(jetbrains.mps.lang.editor.folding.test)");
    runTest("jetbrains.mps.lang.editor.folding.test.ExpandRecursivelyCheckSibling_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("8741431550096049956", "8741431550096049975");
      invokeAction("jetbrains.mps.ide.editor.actions.ExpandRecursively_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.End_Action");
    }
  }
}

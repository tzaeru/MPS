package jetbrains.mps.lang.typesystem.pluginSolution.plugin;

/*Generated by MPS */

import jetbrains.mps.plugins.actions.GeneratedActionGroup;

public class TypesystemActions_ActionGroup extends GeneratedActionGroup {
  public static final String ID = "jetbrains.mps.lang.typesystem.pluginSolution.plugin.TypesystemActions_ActionGroup";
  public TypesystemActions_ActionGroup() {
    super("Type System", ID);
    this.setIsInternal(false);
    this.setMnemonic("T".charAt(0));
    this.setPopup(false);
    TypesystemActions_ActionGroup.this.addAction("jetbrains.mps.lang.typesystem.pluginSolution.plugin.ShowNodeType_Action");
  }
}

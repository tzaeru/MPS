package jetbrains.mps.lang.editor.menus.substitute.testExtendingLanguage;

/*Generated by MPS */

import jetbrains.mps.smodel.language.LanguageRuntime;
import jetbrains.mps.smodel.adapter.ids.SLanguageId;
import java.util.UUID;
import jetbrains.mps.smodel.runtime.ILanguageAspect;
import jetbrains.mps.openapi.editor.descriptor.EditorAspectDescriptor;
import jetbrains.mps.lang.editor.menus.substitute.testExtendingLanguage.editor.EditorAspectDescriptorImpl;

public class Language extends LanguageRuntime {
  public static String MODULE_REF = "cf53f973-da8c-4f92-b001-a1311fb73959(jetbrains.mps.lang.editor.menus.substitute.testExtendingLanguage)";
  public Language() {
  }
  @Override
  public String getNamespace() {
    return "jetbrains.mps.lang.editor.menus.substitute.testExtendingLanguage";
  }

  @Override
  public int getVersion() {
    return 0;
  }

  public SLanguageId getId() {
    return new SLanguageId(UUID.fromString("cf53f973-da8c-4f92-b001-a1311fb73959"));
  }
  @Override
  protected String[] getExtendedLanguageIDs() {
    return new String[]{"jetbrains.mps.lang.editor.menus.substitute.testLanguage"};
  }
  @Override
  protected <T extends ILanguageAspect> T createAspect(Class<T> aspectClass) {
    if (aspectClass.getName().equals("jetbrains.mps.openapi.editor.descriptor.EditorAspectDescriptor")) {
      if (aspectClass == EditorAspectDescriptor.class) {
        return (T) new EditorAspectDescriptorImpl();
      }
    }
    return super.createAspect(aspectClass);
  }
}

package jetbrains.mps.kernel.language;

/*Generated by MPS */

import org.jetbrains.mps.openapi.model.SNode;
import org.jetbrains.mps.openapi.model.SModel;
import jetbrains.mps.smodel.behaviour.BHReflection;
import jetbrains.mps.core.aspects.behaviour.SMethodTrimmedId;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SModelOperations;
import jetbrains.mps.smodel.LanguageAspect;
import jetbrains.mps.smodel.Language;
import jetbrains.mps.kernel.model.SModelUtil;

public class ConceptAspectsHelper {
  public static SNode attachNewConceptAspect(SNode conceptNode, SNode aspectNode, SModel aspectModel) {
    BHReflection.invoke(aspectNode, SMethodTrimmedId.create("setBaseConcept", null, "5r_35Ihc58c"), conceptNode);
    if (SPropertyOperations.getString(conceptNode, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, 0x115eca8579fL, "virtualPackage")) != null) {
      SPropertyOperations.set(aspectNode, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, 0x115eca8579fL, "virtualPackage"), SPropertyOperations.getString(conceptNode, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, 0x115eca8579fL, "virtualPackage")));
    }
    SModelOperations.addRootNode(aspectModel, aspectNode);
    return aspectNode;
  }

  public static <T extends SNode> T attachNewConceptAspect(LanguageAspect aspect, SNode conceptNode, T aspectNode) {
    // [MM] this LanguageAspect usage is reviewed 
    Language language = SModelUtil.getDeclaringLanguage(conceptNode);
    assert language != null : "Language shouldn't be null for " + conceptNode;

    SModel md = aspect.get(language);
    if (md == null) {
      md = aspect.createNew(language);
    }
    attachNewConceptAspect(conceptNode, aspectNode, md);
    return aspectNode;
  }

}

package jetbrains.mps.build.stripping.tests.implementationStrippingTests;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseTestBody;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.test.behavior.INodesTestMethod__BehaviorDescriptor;

@MPSLaunch
public class RepetitionTest_Test extends BaseTransformationTest {
  @Test
  public void test_NodeErrorCheck5264300948581808886() throws Throwable {
    initTest("${mps_home}", "r:a7360bf3-0305-4b0f-a849-53283ec620bf(jetbrains.mps.build.stripping.tests.implementationStrippingTests@tests)", false);
    runTest("jetbrains.mps.build.stripping.tests.implementationStrippingTests.RepetitionTest_Test$TestBody", "test_NodeErrorCheck5264300948581808886", true);
  }
  @Test
  public void test_NodeErrorCheck5264300948581808891() throws Throwable {
    initTest("${mps_home}", "r:a7360bf3-0305-4b0f-a849-53283ec620bf(jetbrains.mps.build.stripping.tests.implementationStrippingTests@tests)", false);
    runTest("jetbrains.mps.build.stripping.tests.implementationStrippingTests.RepetitionTest_Test$TestBody", "test_NodeErrorCheck5264300948581808891", true);
  }
  @Test
  public void test_ErrorMessagesCheck5264300948581808827() throws Throwable {
    initTest("${mps_home}", "r:a7360bf3-0305-4b0f-a849-53283ec620bf(jetbrains.mps.build.stripping.tests.implementationStrippingTests@tests)", false);
    runTest("jetbrains.mps.build.stripping.tests.implementationStrippingTests.RepetitionTest_Test$TestBody", "test_ErrorMessagesCheck5264300948581808827", true);
  }

  @MPSLaunch
  public static class TestBody extends BaseTestBody {

    public void test_NodeErrorCheck5264300948581808886() throws Exception {
      SNode operation = SNodeOperations.cast(getRealNodeById("5264300948581808886"), MetaAdapterFactory.getInterfaceConcept(0x8585453e6bfb4d80L, 0x98deb16074f1d86cL, 0x1510445f8a2c272dL, "jetbrains.mps.lang.test.structure.INodesTestMethod"));
      INodesTestMethod__BehaviorDescriptor.perform_id1kgh5YabdhC.invoke(operation, getRealNodeById("5264300948581808855"));
    }
    public void test_NodeErrorCheck5264300948581808891() throws Exception {
      SNode operation = SNodeOperations.cast(getRealNodeById("5264300948581808891"), MetaAdapterFactory.getInterfaceConcept(0x8585453e6bfb4d80L, 0x98deb16074f1d86cL, 0x1510445f8a2c272dL, "jetbrains.mps.lang.test.structure.INodesTestMethod"));
      INodesTestMethod__BehaviorDescriptor.perform_id1kgh5YabdhC.invoke(operation, getRealNodeById("5264300948581808871"));
    }
    public void test_ErrorMessagesCheck5264300948581808827() throws Exception {
      SNode operation = SNodeOperations.cast(getRealNodeById("5264300948581808827"), MetaAdapterFactory.getInterfaceConcept(0x8585453e6bfb4d80L, 0x98deb16074f1d86cL, 0x1510445f8a2c272dL, "jetbrains.mps.lang.test.structure.INodesTestMethod"));
      INodesTestMethod__BehaviorDescriptor.perform_id1kgh5YabdhC.invoke(operation, getRealNodeById("5264300948581808819"));
    }

  }
}

package jetbrains.mps.editor.runtime.items;

/*Generated by MPS */

import jetbrains.mps.openapi.editor.menus.transformation.CompletionActionItem;

public interface SideTransformCompletionActionItem extends CompletionActionItem {
}

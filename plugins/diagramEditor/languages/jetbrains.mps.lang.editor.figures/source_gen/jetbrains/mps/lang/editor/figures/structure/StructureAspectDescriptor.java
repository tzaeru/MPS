package jetbrains.mps.lang.editor.figures.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import java.util.Map;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import jetbrains.mps.smodel.SNodePointer;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  private final Map<SConceptId, Integer> myIndexMap = new HashMap<SConceptId, Integer>(9);
  /*package*/ final ConceptDescriptor myConceptExternalViewFigure = createDescriptorForExternalViewFigure();
  /*package*/ final ConceptDescriptor myConceptExternalViewFigureParameter = createDescriptorForExternalViewFigureParameter();
  /*package*/ final ConceptDescriptor myConceptFigure = createDescriptorForFigure();
  /*package*/ final ConceptDescriptor myConceptFigureAttribute = createDescriptorForFigureAttribute();
  /*package*/ final ConceptDescriptor myConceptFigureParameter = createDescriptorForFigureParameter();
  /*package*/ final ConceptDescriptor myConceptFigureParameterAttribute = createDescriptorForFigureParameterAttribute();
  /*package*/ final ConceptDescriptor myConceptFigureParameterAttributeField = createDescriptorForFigureParameterAttributeField();
  /*package*/ final ConceptDescriptor myConceptFigureParameterAttributeMethod = createDescriptorForFigureParameterAttributeMethod();
  /*package*/ final ConceptDescriptor myConceptFigureParameterAttributeViewProperty = createDescriptorForFigureParameterAttributeViewProperty();

  public StructureAspectDescriptor() {
    myIndexMap.put(myConceptExternalViewFigure.getId(), 0);
    myIndexMap.put(myConceptExternalViewFigureParameter.getId(), 1);
    myIndexMap.put(myConceptFigure.getId(), 2);
    myIndexMap.put(myConceptFigureAttribute.getId(), 3);
    myIndexMap.put(myConceptFigureParameter.getId(), 4);
    myIndexMap.put(myConceptFigureParameterAttribute.getId(), 5);
    myIndexMap.put(myConceptFigureParameterAttributeField.getId(), 6);
    myIndexMap.put(myConceptFigureParameterAttributeMethod.getId(), 7);
    myIndexMap.put(myConceptFigureParameterAttributeViewProperty.getId(), 8);
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptExternalViewFigure, myConceptExternalViewFigureParameter, myConceptFigure, myConceptFigureAttribute, myConceptFigureParameter, myConceptFigureParameterAttribute, myConceptFigureParameterAttributeField, myConceptFigureParameterAttributeMethod, myConceptFigureParameterAttributeViewProperty);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    Integer index = myIndexMap.get(id);
    if (index == null) {
      return null;
    }
    switch (((int) index)) {
      case 0:
        return myConceptExternalViewFigure;
      case 1:
        return myConceptExternalViewFigureParameter;
      case 2:
        return myConceptFigure;
      case 3:
        return myConceptFigureAttribute;
      case 4:
        return myConceptFigureParameter;
      case 5:
        return myConceptFigureParameterAttribute;
      case 6:
        return myConceptFigureParameterAttributeField;
      case 7:
        return myConceptFigureParameterAttributeMethod;
      case 8:
        return myConceptFigureParameterAttributeViewProperty;
      default:
        throw new IllegalStateException();
    }
  }

  private static ConceptDescriptor createDescriptorForExternalViewFigure() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.ExternalViewFigure", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1e3b9cbb9f7493c2L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept", "jetbrains.mps.lang.core.structure.ScopeProvider").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x33d23ee961a0cbf3L)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x1e3b9cbb9f7493f7L, "classifier", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c108ca66L), false, new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2178507174411801591"))).references("classifier").childDescriptors(new ConceptDescriptorBuilder.Link(0x1e3b9cbb9f749440L, "fields", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1e3b9cbb9f749406L), true, true, false, new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2178507174411801664"))).children(new String[]{"fields"}, new boolean[]{true}).rootable().sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2178507174411801538")).create();
  }
  private static ConceptDescriptor createDescriptorForExternalViewFigureParameter() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.ExternalViewFigureParameter", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1e3b9cbb9f749406L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept", "jetbrains.mps.lang.editor.figures.structure.FigureParameter").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L), MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4bf6bbafe7e7155L)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x1e3b9cbb9f749431L, "fieldDeclaration", MetaIdFactory.conceptId(0xf3061a5392264cc5L, 0xa443f952ceaf5816L, 0xf8c108ca68L), false, new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2178507174411801649"))).references("fieldDeclaration").sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2178507174411801606")).create();
  }
  private static ConceptDescriptor createDescriptorForFigure() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.Figure", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0xae7ce997c3b4305L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).abstract_().sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "785823818609017605")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureAttribute() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureAttribute", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a095b5a4L)).super_("jetbrains.mps.lang.core.structure.NodeAttribute").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x2eb1ad060897da54L)).parents("jetbrains.mps.lang.core.structure.NodeAttribute").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x2eb1ad060897da54L)).alias("@Figure", "").sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "5422656561926747556")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureParameter() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureParameter", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4bf6bbafe7e7155L)).version(1).interface_().sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "342110547581235541")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureParameterAttribute() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).super_("jetbrains.mps.lang.core.structure.NodeAttribute").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x2eb1ad060897da54L)).parents("jetbrains.mps.lang.core.structure.NodeAttribute", "jetbrains.mps.lang.editor.figures.structure.FigureParameter").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x2eb1ad060897da54L), MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4bf6bbafe7e7155L)).abstract_().sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "5422656561929884641")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureParameterAttributeField() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeField", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1ceea85e3fd59976L)).super_("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").version(1).super_(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).parents("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").parentIds(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2084788800270473590")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureParameterAttributeMethod() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeMethod", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1ceea85e3fd59954L)).super_("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").version(1).super_(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).parents("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").parentIds(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "2084788800270473556")).create();
  }
  private static ConceptDescriptor createDescriptorForFigureParameterAttributeViewProperty() {
    return new ConceptDescriptorBuilder("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeViewProperty", MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x6595651980a1f8ecL)).super_("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").version(1).super_(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).parents("jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute").parentIds(MetaIdFactory.conceptId(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L)).sourceNode(new SNodePointer("r:64327a98-9d9a-43f9-aa56-fe3b1ee87c60(jetbrains.mps.lang.editor.figures.structure)", "7319867929567295724")).create();
  }
}

<?xml version="1.0" encoding="UTF-8"?>
<solution name="jetbrains.mps.samples.theSimplestLanguage.sandbox" uuid="710ae5d7-8901-4b20-9d5d-f48c728543b5" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="jetbrains" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="4" />
    <language slang="l:ed6d7656-532c-4bc2-81d1-af945aeb8280:jetbrains.mps.baseLanguage.blTypes" version="0" />
    <language slang="l:4caf0310-491e-41f5-8a9b-2006b3a94898:jetbrains.mps.execution.util" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
    <language slang="l:f8fecd49-3abe-4733-9741-0c637123d219:jetbrains.mps.samples.theSimplestLanguage" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="710ae5d7-8901-4b20-9d5d-f48c728543b5(jetbrains.mps.samples.theSimplestLanguage.sandbox)" version="0" />
  </dependencyVersions>
</solution>


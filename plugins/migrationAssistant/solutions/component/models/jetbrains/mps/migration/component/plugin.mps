<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d09733d9-6079-487b-b676-f99e7604f344(jetbrains.mps.migration.component.plugin)">
  <persistence version="9" />
  <languages>
    <use id="ed6d7656-532c-4bc2-81d1-af945aeb8280" name="jetbrains.mps.baseLanguage.blTypes" version="0" />
    <use id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers" version="0" />
    <use id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures" version="0" />
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="0" />
    <use id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging" version="0" />
    <use id="a247e09e-2435-45ba-b8d2-07e93feba96a" name="jetbrains.mps.baseLanguage.tuples" version="0" />
    <use id="fe9d76d7-5809-45c9-ae28-a40915b4d6ff" name="jetbrains.mps.lang.checkedName" version="0" />
    <use id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin" version="1" />
    <use id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" name="jetbrains.mps.lang.traceable" version="0" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="4" />
    <use id="1a8554c4-eb84-43ba-8c34-6f0d90c6e75a" name="jetbrains.mps.lang.smodel.query" version="2" />
    <use id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone" version="0" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="4" />
    <use id="de1ad86d-6e50-4a02-b306-d4d17f64c375" name="jetbrains.mps.console.base" version="0" />
    <use id="774bf8a0-62e5-41e1-af63-f4812e60e48b" name="jetbrains.mps.baseLanguage.checkedDots" version="0" />
    <engage id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone" />
  </languages>
  <imports>
    <import index="qq03" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.actions(MPS.Platform/)" />
    <import index="qkt" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.actionSystem(MPS.IDEA/)" />
    <import index="bim2" ref="a5b1c28d-abeb-49a6-a58c-559039616d64/r:a9597bdf-0806-4a79-8ace-88240c6b9878(jetbrains.mps.migration.component/jetbrains.mps.ide.migration)" />
    <import index="auc7" ref="r:22e3ec81-a192-41cd-83a2-488758bdeedc(jetbrains.mps.migration.component.util)" />
    <import index="1m72" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.components(MPS.IDEA/)" />
    <import index="jkm4" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.ui(MPS.IDEA/)" />
    <import index="lui2" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.module(MPS.OpenAPI/)" />
    <import index="bd8o" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.application(MPS.IDEA/)" />
    <import index="z1c3" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.project(MPS.Core/)" />
    <import index="tpce" ref="r:00000000-0000-4000-0000-011c89590292(jetbrains.mps.lang.structure.structure)" />
    <import index="pjrh" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter(MPS.Core/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="6f4m" ref="528ff3b9-5fc4-40dd-931f-c6ce3650640e/r:f69c3fa1-0e30-4980-84e2-190ae44e4c3d(jetbrains.mps.lang.migration.runtime/jetbrains.mps.lang.migration.runtime.base)" />
    <import index="18ew" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.util(MPS.Core/)" />
    <import index="bdll" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.migration.global(MPS.Core/)" />
    <import index="z1c4" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.project(MPS.Platform/)" />
    <import index="xnls" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.icons(MPS.Platform/)" />
    <import index="71xd" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.tools(MPS.Platform/)" />
    <import index="g4jo" ref="r:d98d04fb-4a60-4106-81cf-6cb40b67de4d(jetbrains.mps.ide.findusages.model)" />
    <import index="t99v" ref="r:5c426f30-a9c9-463b-90a5-2fae21a10696(jetbrains.mps.ide.migration.check)" />
    <import index="phxh" ref="r:5754bb7d-f802-4a0f-bd3d-0764f0d71413(jetbrains.mps.ide.modelchecker.platform.actions)" />
    <import index="o8ag" ref="a5b1c28d-abeb-49a6-a58c-559039616d64/r:49062720-8530-4489-916a-fdd3a02a7b82(jetbrains.mps.migration.component/jetbrains.mps.ide.migration.wizard)" />
    <import index="4nm9" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.project(MPS.IDEA/)" />
    <import index="rn77" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.module.impl.scopes(MPS.IDEA/)" />
    <import index="mte5" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.ide.findusages.model.scopes(MPS.Core/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="7bx7" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.workbench.action(MPS.Platform/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="vndm" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.language(MPS.Core/)" />
    <import index="xygl" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.progress(MPS.IDEA/)" />
    <import index="yif3" ref="r:13ec431d-483d-451c-a648-ffefde4fef51(jetbrains.mps.internal.make.runtime.backports)" />
    <import index="mk8z" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.progress(MPS.Platform/)" />
    <import index="9w4s" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.util(MPS.IDEA/)" />
    <import index="tp25" ref="r:00000000-0000-4000-0000-011c89590301(jetbrains.mps.lang.smodel.structure)" />
    <import index="tp3r" ref="r:00000000-0000-4000-0000-011c8959034b(jetbrains.mps.lang.quotation.structure)" />
    <import index="ngmm" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.findusages.view(MPS.Platform/)" />
    <import index="alof" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.project(MPS.Platform/)" />
    <import index="mk90" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.progress(MPS.Core/)" implicit="true" />
    <import index="tpcn" ref="r:00000000-0000-4000-0000-011c8959028b(jetbrains.mps.lang.structure.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="a247e09e-2435-45ba-b8d2-07e93feba96a" name="jetbrains.mps.baseLanguage.tuples">
      <concept id="1238853782547" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleLiteral" flags="nn" index="1Ls8ON">
        <child id="1238853845806" name="component" index="1Lso8e" />
      </concept>
    </language>
    <language id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin">
      <concept id="5023285075122009364" name="jetbrains.mps.lang.plugin.structure.IdeaInitializerDescriptor" flags="ng" index="9BnSX">
        <property id="5023285075122009369" name="version" index="9BnSK" />
        <property id="5023285075122009368" name="descripttion" index="9BnSL" />
        <property id="5023285075122009371" name="vendor" index="9BnSM" />
        <property id="5023285075122009373" name="ideaVersion" index="9BnSO" />
        <property id="5023285075122009372" name="vendorUrl" index="9BnSP" />
        <property id="5023285075122009366" name="id" index="9BnSZ" />
        <property id="4167053799973858143" name="vendorLogo" index="1qHB85" />
      </concept>
      <concept id="1207145163717" name="jetbrains.mps.lang.plugin.structure.ElementListContents" flags="ng" index="ftmFs">
        <child id="1207145201301" name="reference" index="ftvYc" />
      </concept>
      <concept id="1203071646776" name="jetbrains.mps.lang.plugin.structure.ActionDeclaration" flags="ng" index="sE7Ow">
        <property id="1211298967294" name="outsideCommandExecution" index="72QZ$" />
        <property id="1205250923097" name="caption" index="2uzpH1" />
        <property id="997079742910640235" name="fillActionContext" index="1teQrl" />
        <child id="1203083196627" name="updateBlock" index="tmbBb" />
        <child id="1203083461638" name="executeFunction" index="tncku" />
        <child id="1227008813498" name="constructionParameter" index="2JrayB" />
        <child id="1205851242421" name="methodDeclaration" index="32lrUH" />
        <child id="1217413222820" name="parameter" index="1NuT2Z" />
      </concept>
      <concept id="1203082695294" name="jetbrains.mps.lang.plugin.structure.DoUpdateBlock" flags="in" index="tkhdA" />
      <concept id="1203082903663" name="jetbrains.mps.lang.plugin.structure.ConceptFunctionParameter_AnActionEvent" flags="nn" index="tl45R" />
      <concept id="1203083511112" name="jetbrains.mps.lang.plugin.structure.ExecuteBlock" flags="in" index="tnohg" />
      <concept id="1203087890642" name="jetbrains.mps.lang.plugin.structure.ActionGroupDeclaration" flags="ng" index="tC5Ba">
        <property id="1204991940915" name="caption" index="2f7twF" />
        <property id="1213283637680" name="isPopup" index="1XlLyE" />
        <child id="1204991552650" name="modifier" index="2f5YQi" />
        <child id="1207145245948" name="contents" index="ftER_" />
      </concept>
      <concept id="1203088046679" name="jetbrains.mps.lang.plugin.structure.ActionInstance" flags="ng" index="tCFHf">
        <reference id="1203088061055" name="action" index="tCJdB" />
        <child id="1227011543811" name="actualParameter" index="2J__8u" />
      </concept>
      <concept id="1203092361741" name="jetbrains.mps.lang.plugin.structure.ModificationStatement" flags="lg" index="tT9cl">
        <reference id="1204992316090" name="point" index="2f8Tey" />
        <reference id="1203092736097" name="modifiedGroup" index="tU$_T" />
      </concept>
      <concept id="1227008846812" name="jetbrains.mps.lang.plugin.structure.ActionConstructionParameterDeclaration" flags="ig" index="2JriF1">
        <child id="1227019158144" name="toStringFunction" index="2K2Cet" />
      </concept>
      <concept id="1227013049127" name="jetbrains.mps.lang.plugin.structure.AddStatement" flags="nn" index="2JFkCU">
        <child id="1227013166210" name="item" index="2JFLmv" />
      </concept>
      <concept id="1512255007353869535" name="jetbrains.mps.lang.plugin.structure.ActionReference" flags="ln" index="2JRCWP">
        <reference id="1512255007353869536" name="actionDeclaration" index="2JRCWa" />
      </concept>
      <concept id="1512255007353869532" name="jetbrains.mps.lang.plugin.structure.NonDumbAwareActions" flags="ng" index="2JRCWQ">
        <child id="1512255007353869533" name="actions" index="2JRCWR" />
      </concept>
      <concept id="1227019068586" name="jetbrains.mps.lang.plugin.structure.ToStringConceptFunction" flags="in" index="2K2imR" />
      <concept id="1227019310584" name="jetbrains.mps.lang.plugin.structure.ToStringParameter" flags="nn" index="2K3dj_" />
      <concept id="1239975356883" name="jetbrains.mps.lang.plugin.structure.UpdateGroupBlock" flags="ng" index="2OiAzN">
        <child id="1239975488603" name="updateFunction" index="2Oj6PV" />
      </concept>
      <concept id="1239975436002" name="jetbrains.mps.lang.plugin.structure.UpdateGroupFunction" flags="in" index="2OiTZ2" />
      <concept id="1203680534665" name="jetbrains.mps.lang.plugin.structure.GroupAnchor" flags="ng" index="10WQ6h" />
      <concept id="5538333046911348654" name="jetbrains.mps.lang.plugin.structure.RequiredCondition" flags="ng" index="1oajcY" />
      <concept id="1217252042208" name="jetbrains.mps.lang.plugin.structure.ActionDataParameterDeclaration" flags="ng" index="1DS2jV">
        <reference id="1217252646389" name="key" index="1DUlNI" />
      </concept>
      <concept id="1217252428768" name="jetbrains.mps.lang.plugin.structure.ActionDataParameterReferenceOperation" flags="nn" index="1DTwFV" />
      <concept id="1204383956737" name="jetbrains.mps.lang.plugin.structure.InterfaceGroup" flags="ng" index="1ESbSp">
        <child id="1206193920040" name="groupID" index="3mKD$K" />
      </concept>
      <concept id="1217413147516" name="jetbrains.mps.lang.plugin.structure.ActionParameter" flags="ng" index="1NuADB">
        <child id="5538333046911298738" name="condition" index="1oa70y" />
      </concept>
    </language>
    <language id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone">
      <concept id="7520713872864775836" name="jetbrains.mps.lang.plugin.standalone.structure.StandalonePluginDescriptor" flags="ng" index="2DaZZR" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P">
        <reference id="1182955020723" name="classConcept" index="1HBi2w" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1073063089578" name="jetbrains.mps.baseLanguage.structure.SuperMethodCall" flags="nn" index="3nyPlj" />
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1081855346303" name="jetbrains.mps.baseLanguage.structure.BreakStatement" flags="nn" index="3zACq4" />
      <concept id="1184950988562" name="jetbrains.mps.baseLanguage.structure.ArrayCreator" flags="nn" index="3$_iS1">
        <child id="1184951007469" name="componentType" index="3$_nBY" />
        <child id="1184952969026" name="dimensionExpression" index="3$GQph" />
      </concept>
      <concept id="1184952934362" name="jetbrains.mps.baseLanguage.structure.DimensionExpression" flags="nn" index="3$GHV9">
        <child id="1184953288404" name="expression" index="3$I4v7" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1082113931046" name="jetbrains.mps.baseLanguage.structure.ContinueStatement" flags="nn" index="3N13vt" />
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
      </concept>
    </language>
    <language id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access">
      <concept id="8974276187400348173" name="jetbrains.mps.lang.access.structure.CommandClosureLiteral" flags="nn" index="1QHqEC" />
      <concept id="8974276187400348170" name="jetbrains.mps.lang.access.structure.BaseExecuteCommandStatement" flags="nn" index="1QHqEJ">
        <child id="1423104411234567454" name="repo" index="ukAjM" />
        <child id="8974276187400348171" name="commandClosureLiteral" index="1QHqEI" />
      </concept>
      <concept id="8974276187400348181" name="jetbrains.mps.lang.access.structure.ExecuteLightweightCommandStatement" flags="nn" index="1QHqEK" />
      <concept id="8974276187400348177" name="jetbrains.mps.lang.access.structure.ExecuteCommandStatement" flags="nn" index="1QHqEO" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1200830824066" name="jetbrains.mps.baseLanguage.closures.structure.YieldStatement" flags="nn" index="2n63Yl">
        <child id="1200830928149" name="expression" index="2n6tg2" />
      </concept>
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers">
      <concept id="1213999117680" name="jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierFieldAccessOperation" flags="nn" index="2BZ7hE" />
      <concept id="1205752633985" name="jetbrains.mps.baseLanguage.classifiers.structure.ThisClassifierExpression" flags="nn" index="2WthIp">
        <reference id="1218736638915" name="classifier" index="32nkFo" />
      </concept>
      <concept id="1205756064662" name="jetbrains.mps.baseLanguage.classifiers.structure.IMemberOperation" flags="ng" index="2WEnae">
        <reference id="1205756909548" name="member" index="2WH_rO" />
      </concept>
      <concept id="1205769003971" name="jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierMethodDeclaration" flags="ng" index="2XrIbr" />
      <concept id="1205769149993" name="jetbrains.mps.baseLanguage.classifiers.structure.DefaultClassifierMethodCallOperation" flags="nn" index="2XshWL">
        <child id="1205770614681" name="actualArgument" index="2XxRq1" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1219352745532" name="jetbrains.mps.lang.smodel.structure.NodeRefExpression" flags="nn" index="3B5_sB">
        <reference id="1219352800908" name="referentNode" index="3B5MYn" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="1a8554c4-eb84-43ba-8c34-6f0d90c6e75a" name="jetbrains.mps.lang.smodel.query">
      <concept id="7738379549910147341" name="jetbrains.mps.lang.smodel.query.structure.InstancesExpression" flags="ng" index="qVDSY">
        <child id="7738379549910147342" name="conceptArg" index="qVDSX" />
      </concept>
      <concept id="4234138103881610891" name="jetbrains.mps.lang.smodel.query.structure.WithStatement" flags="ng" index="L3pyB">
        <child id="4234138103881610935" name="scope" index="L3pyr" />
        <child id="4234138103881610892" name="stmts" index="L3pyw" />
      </concept>
      <concept id="4307205004131544317" name="jetbrains.mps.lang.smodel.query.structure.QueryExpression" flags="ng" index="1dNuzs">
        <child id="4307205004132279624" name="parameter" index="1dOa5D" />
      </concept>
      <concept id="4307205004132277753" name="jetbrains.mps.lang.smodel.query.structure.QueryParameterList" flags="ng" index="1dO9Bo" />
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1226511727824" name="jetbrains.mps.baseLanguage.collections.structure.SetType" flags="in" index="2hMVRd">
        <child id="1226511765987" name="elementType" index="2hN53Y" />
      </concept>
      <concept id="1226516258405" name="jetbrains.mps.baseLanguage.collections.structure.HashSetCreator" flags="nn" index="2i4dXS" />
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1151702311717" name="jetbrains.mps.baseLanguage.collections.structure.ToListOperation" flags="nn" index="ANE8D" />
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
        <child id="1237731803878" name="copyFrom" index="I$8f6" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1205679737078" name="jetbrains.mps.baseLanguage.collections.structure.SortOperation" flags="nn" index="2S7cBI">
        <child id="1205679832066" name="ascending" index="2S7zOq" />
      </concept>
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
      <concept id="1173946412755" name="jetbrains.mps.baseLanguage.collections.structure.RemoveAllElementsOperation" flags="nn" index="1kEaZ2" />
      <concept id="1178286324487" name="jetbrains.mps.baseLanguage.collections.structure.SortDirection" flags="nn" index="1nlBCl" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
      <concept id="1178894719932" name="jetbrains.mps.baseLanguage.collections.structure.DistinctOperation" flags="nn" index="1VAtEI" />
    </language>
  </registry>
  <node concept="sE7Ow" id="5$zfhXzhLy1">
    <property role="TrG5h" value="ExecuteRerunnableMigrations" />
    <property role="2uzpH1" value="Execute Re-Runnable Migrations" />
    <property role="3GE5qa" value="migrationList" />
    <property role="72QZ$" value="true" />
    <node concept="tnohg" id="5$zfhXzhLy2" role="tncku">
      <node concept="3clFbS" id="5$zfhXzhLy3" role="2VODD2">
        <node concept="3cpWs8" id="5$zfhXzq_ju" role="3cqZAp">
          <node concept="3cpWsn" id="5$zfhXzq_jv" role="3cpWs9">
            <property role="TrG5h" value="mc" />
            <node concept="2OqwBi" id="5$zfhXzq_M4" role="33vP2m">
              <node concept="2OqwBi" id="5$zfhXzq_M5" role="2Oq$k0">
                <node concept="2WthIp" id="5$zfhXzq_M6" role="2Oq$k0" />
                <node concept="1DTwFV" id="5$zfhXzq_M7" role="2OqNvi">
                  <ref role="2WH_rO" node="5$zfhXzqyPE" resolve="project" />
                </node>
              </node>
              <node concept="liA8E" id="5$zfhXzq_M8" role="2OqNvi">
                <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                <node concept="3VsKOn" id="5$zfhXzq_M9" role="37wK5m">
                  <ref role="3VsUkX" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
                </node>
              </node>
            </node>
            <node concept="3uibUv" id="1v09psn9kb6" role="1tU5fm">
              <ref role="3uigEE" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1wrYbVZdAo" role="3cqZAp" />
        <node concept="3cpWs8" id="1wrYbVZej8" role="3cqZAp">
          <node concept="3cpWsn" id="1wrYbVZej9" role="3cpWs9">
            <property role="TrG5h" value="modules" />
            <property role="3TUv4t" value="true" />
            <node concept="10Q1$e" id="1wrYbVZeja" role="1tU5fm">
              <node concept="3uibUv" id="1wrYbVZejb" role="10Q1$1">
                <ref role="3uigEE" to="33ny:~List" resolve="List" />
                <node concept="3uibUv" id="1wrYbVZejc" role="11_B2D">
                  <ref role="3uigEE" to="lui2:~SModule" resolve="SModule" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="1wrYbVZejd" role="33vP2m">
              <node concept="3$_iS1" id="1wrYbVZeje" role="2ShVmc">
                <node concept="3$GHV9" id="1wrYbVZejf" role="3$GQph">
                  <node concept="3cmrfG" id="1wrYbVZejg" role="3$I4v7">
                    <property role="3cmrfH" value="1" />
                  </node>
                </node>
                <node concept="3uibUv" id="1wrYbVZejh" role="3$_nBY">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1QHqEK" id="1wrYbVZeji" role="3cqZAp">
          <node concept="1QHqEC" id="1wrYbVZejj" role="1QHqEI">
            <node concept="3clFbS" id="1wrYbVZejk" role="1bW5cS">
              <node concept="3clFbF" id="1wrYbVZejl" role="3cqZAp">
                <node concept="37vLTI" id="1wrYbVZejm" role="3clFbG">
                  <node concept="AH0OO" id="1wrYbVZejq" role="37vLTJ">
                    <node concept="3cmrfG" id="1wrYbVZejr" role="AHEQo">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="37vLTw" id="1wrYbVZejs" role="AHHXb">
                      <ref role="3cqZAo" node="1wrYbVZej9" resolve="modules" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1wrYbVZLvz" role="37vLTx">
                    <node concept="2YIFZM" id="1wrYbVZLv$" role="2Oq$k0">
                      <ref role="1Pybhc" to="auc7:3UfGsecu96G" resolve="MigrationsUtil" />
                      <ref role="37wK5l" to="auc7:3UfGsecu96H" resolve="getMigrateableModulesFromProject" />
                      <node concept="2OqwBi" id="1wrYbVZMqK" role="37wK5m">
                        <node concept="2WthIp" id="1wrYbVZMqN" role="2Oq$k0" />
                        <node concept="1DTwFV" id="1wrYbVZMqP" role="2OqNvi">
                          <ref role="2WH_rO" node="5$zfhXzjQau" resolve="mpsProject" />
                        </node>
                      </node>
                    </node>
                    <node concept="ANE8D" id="1wrYbVZLvA" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1wrYbVZejt" role="ukAjM">
            <node concept="2OqwBi" id="1wrYbVZpZm" role="2Oq$k0">
              <node concept="2WthIp" id="1wrYbVZpZp" role="2Oq$k0" />
              <node concept="1DTwFV" id="1wrYbVZq4S" role="2OqNvi">
                <ref role="2WH_rO" node="5$zfhXzjQau" resolve="mpsProject" />
              </node>
            </node>
            <node concept="liA8E" id="1wrYbVZejv" role="2OqNvi">
              <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1wrYbVZejw" role="3cqZAp">
          <node concept="2OqwBi" id="1wrYbVZejx" role="3clFbG">
            <node concept="2YIFZM" id="1wrYbVZejy" role="2Oq$k0">
              <ref role="1Pybhc" to="xygl:~ProgressManager" resolve="ProgressManager" />
              <ref role="37wK5l" to="xygl:~ProgressManager.getInstance():com.intellij.openapi.progress.ProgressManager" resolve="getInstance" />
            </node>
            <node concept="liA8E" id="1wrYbVZejz" role="2OqNvi">
              <ref role="37wK5l" to="xygl:~ProgressManager.run(com.intellij.openapi.progress.Task):void" resolve="run" />
              <node concept="2ShNRf" id="1wrYbVZej$" role="37wK5m">
                <node concept="YeOm9" id="1wrYbVZej_" role="2ShVmc">
                  <node concept="1Y3b0j" id="1wrYbVZejA" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="xygl:~Task$Modal" resolve="Task.Modal" />
                    <ref role="37wK5l" to="xygl:~Task$Modal.&lt;init&gt;(com.intellij.openapi.project.Project,java.lang.String,boolean)" resolve="Task.Modal" />
                    <node concept="3Tm1VV" id="1wrYbVZejB" role="1B3o_S" />
                    <node concept="3clFb_" id="1wrYbVZejC" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="run" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="1wrYbVZejD" role="1B3o_S" />
                      <node concept="3cqZAl" id="1wrYbVZejE" role="3clF45" />
                      <node concept="37vLTG" id="1wrYbVZejF" role="3clF46">
                        <property role="TrG5h" value="progressIndicator" />
                        <node concept="3uibUv" id="1wrYbVZejG" role="1tU5fm">
                          <ref role="3uigEE" to="xygl:~ProgressIndicator" resolve="ProgressIndicator" />
                        </node>
                        <node concept="2AHcQZ" id="1wrYbVZejH" role="2AJF6D">
                          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="1wrYbVZejI" role="3clF47">
                        <node concept="3cpWs8" id="1wrYbVZejJ" role="3cqZAp">
                          <node concept="3cpWsn" id="1wrYbVZejK" role="3cpWs9">
                            <property role="TrG5h" value="progressMonitor" />
                            <node concept="3uibUv" id="1wrYbVZejL" role="1tU5fm">
                              <ref role="3uigEE" to="mk8z:~ProgressMonitorAdapter" resolve="ProgressMonitorAdapter" />
                            </node>
                            <node concept="2ShNRf" id="1wrYbVZejM" role="33vP2m">
                              <node concept="1pGfFk" id="1wrYbVZejN" role="2ShVmc">
                                <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.&lt;init&gt;(com.intellij.openapi.progress.ProgressIndicator)" resolve="ProgressMonitorAdapter" />
                                <node concept="37vLTw" id="1wrYbVZejO" role="37wK5m">
                                  <ref role="3cqZAo" node="1wrYbVZejF" resolve="progressIndicator" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cpWs8" id="1wrYbVZejP" role="3cqZAp">
                          <node concept="3cpWsn" id="1wrYbVZejQ" role="3cpWs9">
                            <property role="TrG5h" value="steps" />
                            <node concept="2OqwBi" id="1wrYbVZejR" role="33vP2m">
                              <node concept="AH0OO" id="1wrYbVZejS" role="2Oq$k0">
                                <node concept="3cmrfG" id="1wrYbVZejT" role="AHEQo">
                                  <property role="3cmrfH" value="0" />
                                </node>
                                <node concept="37vLTw" id="1wrYbVZejU" role="AHHXb">
                                  <ref role="3cqZAo" node="1wrYbVZej9" resolve="modules" />
                                </node>
                              </node>
                              <node concept="liA8E" id="1wrYbVZejV" role="2OqNvi">
                                <ref role="37wK5l" to="33ny:~List.size():int" resolve="size" />
                              </node>
                            </node>
                            <node concept="10Oyi0" id="1wrYbVZejW" role="1tU5fm" />
                          </node>
                        </node>
                        <node concept="3clFbF" id="1wrYbVZejX" role="3cqZAp">
                          <node concept="2OqwBi" id="1wrYbVZejY" role="3clFbG">
                            <node concept="37vLTw" id="1wrYbVZejZ" role="2Oq$k0">
                              <ref role="3cqZAo" node="1wrYbVZejK" resolve="progressMonitor" />
                            </node>
                            <node concept="liA8E" id="1wrYbVZek0" role="2OqNvi">
                              <ref role="37wK5l" to="mk90:~ProgressMonitorBase.start(java.lang.String,int):void" resolve="start" />
                              <node concept="Xl_RD" id="1wrYbVZtjw" role="37wK5m">
                                <property role="Xl_RC" value="Running..." />
                              </node>
                              <node concept="37vLTw" id="1wrYbVZek4" role="37wK5m">
                                <ref role="3cqZAo" node="1wrYbVZejQ" resolve="steps" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2Gpval" id="1wrYbVZek5" role="3cqZAp">
                          <node concept="2GrKxI" id="1wrYbVZek6" role="2Gsz3X">
                            <property role="TrG5h" value="module" />
                          </node>
                          <node concept="3clFbS" id="1wrYbVZek7" role="2LFqv$">
                            <node concept="3clFbF" id="1wrYbVZek8" role="3cqZAp">
                              <node concept="2OqwBi" id="1wrYbVZek9" role="3clFbG">
                                <node concept="37vLTw" id="1wrYbVZeka" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1wrYbVZejK" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="1wrYbVZekb" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.step(java.lang.String):void" resolve="step" />
                                  <node concept="2OqwBi" id="1wrYbVZekc" role="37wK5m">
                                    <node concept="2GrUjf" id="1wrYbVZekd" role="2Oq$k0">
                                      <ref role="2Gs0qQ" node="1wrYbVZek6" resolve="module" />
                                    </node>
                                    <node concept="liA8E" id="1wrYbVZeke" role="2OqNvi">
                                      <ref role="37wK5l" to="lui2:~SModule.getModuleName():java.lang.String" resolve="getModuleName" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="1wrYbVZekf" role="3cqZAp">
                              <node concept="2YIFZM" id="1wrYbVZekg" role="3clFbG">
                                <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
                                <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
                                <node concept="1bVj0M" id="1wrYbVZekh" role="37wK5m">
                                  <node concept="3clFbS" id="1wrYbVZeki" role="1bW5cS">
                                    <node concept="1QHqEO" id="1wrYbVZekj" role="3cqZAp">
                                      <node concept="1QHqEC" id="1wrYbVZekk" role="1QHqEI">
                                        <node concept="3clFbS" id="1wrYbVZekl" role="1bW5cS">
                                          <node concept="3cpWs8" id="5$zfhXzqIML" role="3cqZAp">
                                            <node concept="3cpWsn" id="5$zfhXzqIMM" role="3cpWs9">
                                              <property role="TrG5h" value="languages" />
                                              <node concept="2hMVRd" id="5$zfhXzqIYa" role="1tU5fm">
                                                <node concept="3uibUv" id="5$zfhXzqIYc" role="2hN53Y">
                                                  <ref role="3uigEE" to="c17a:~SLanguage" resolve="SLanguage" />
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="5$zfhXzqIMN" role="33vP2m">
                                                <node concept="2ShNRf" id="5$zfhXzqIMO" role="2Oq$k0">
                                                  <node concept="1pGfFk" id="5$zfhXzqIMP" role="2ShVmc">
                                                    <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.&lt;init&gt;(jetbrains.mps.smodel.language.LanguageRegistry,java.util.Collection)" resolve="SLanguageHierarchy" />
                                                    <node concept="2YIFZM" id="1v09psn9qep" role="37wK5m">
                                                      <ref role="1Pybhc" to="vndm:~LanguageRegistry" resolve="LanguageRegistry" />
                                                      <ref role="37wK5l" to="vndm:~LanguageRegistry.getInstance(org.jetbrains.mps.openapi.module.SRepository):jetbrains.mps.smodel.language.LanguageRegistry" resolve="getInstance" />
                                                      <node concept="2OqwBi" id="1v09psn9rqe" role="37wK5m">
                                                        <node concept="2OqwBi" id="1v09psn9qQc" role="2Oq$k0">
                                                          <node concept="2WthIp" id="1v09psn9qQf" role="2Oq$k0">
                                                            <ref role="32nkFo" node="5$zfhXzhLy1" resolve="ExecuteRerunnableMigrations" />
                                                          </node>
                                                          <node concept="1DTwFV" id="1v09psn9qQh" role="2OqNvi">
                                                            <ref role="2WH_rO" node="5$zfhXzjQau" resolve="mpsProject" />
                                                          </node>
                                                        </node>
                                                        <node concept="liA8E" id="1v09psn9rLx" role="2OqNvi">
                                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                                        </node>
                                                      </node>
                                                    </node>
                                                    <node concept="2OqwBi" id="5$zfhXzqIMQ" role="37wK5m">
                                                      <node concept="2GrUjf" id="5$zfhXzqIMR" role="2Oq$k0">
                                                        <ref role="2Gs0qQ" node="1wrYbVZek6" resolve="module" />
                                                      </node>
                                                      <node concept="liA8E" id="5$zfhXzqIMS" role="2OqNvi">
                                                        <ref role="37wK5l" to="lui2:~SModule.getUsedLanguages():java.util.Set" resolve="getUsedLanguages" />
                                                      </node>
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="5$zfhXzqIMT" role="2OqNvi">
                                                  <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.getExtended():java.util.Set" resolve="getExtended" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3cpWs8" id="5$zfhXzqQnw" role="3cqZAp">
                                            <node concept="3cpWsn" id="5$zfhXzqQnx" role="3cpWs9">
                                              <property role="TrG5h" value="scripts" />
                                              <node concept="A3Dl8" id="5$zfhXzqQnr" role="1tU5fm">
                                                <node concept="3uibUv" id="5$zfhXzqQnu" role="A3Ik2">
                                                  <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="5$zfhXzqQny" role="33vP2m">
                                                <node concept="37vLTw" id="5$zfhXzqQnz" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="5$zfhXzqIMM" resolve="languages" />
                                                </node>
                                                <node concept="3goQfb" id="5$zfhXzqQn$" role="2OqNvi">
                                                  <node concept="1bVj0M" id="5$zfhXzqQn_" role="23t8la">
                                                    <node concept="3clFbS" id="5$zfhXzqQnA" role="1bW5cS">
                                                      <node concept="1Dw8fO" id="5$zfhXzqQnB" role="3cqZAp">
                                                        <node concept="3cpWsn" id="5$zfhXzqQnC" role="1Duv9x">
                                                          <property role="TrG5h" value="ver" />
                                                          <node concept="10Oyi0" id="5$zfhXzqQnD" role="1tU5fm" />
                                                          <node concept="3cmrfG" id="5$zfhXzqQnE" role="33vP2m">
                                                            <property role="3cmrfH" value="0" />
                                                          </node>
                                                        </node>
                                                        <node concept="3clFbS" id="5$zfhXzqQnF" role="2LFqv$">
                                                          <node concept="3cpWs8" id="5$zfhXzqQnG" role="3cqZAp">
                                                            <node concept="3cpWsn" id="5$zfhXzqQnH" role="3cpWs9">
                                                              <property role="TrG5h" value="script" />
                                                              <node concept="3uibUv" id="5$zfhXzqQnI" role="1tU5fm">
                                                                <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
                                                              </node>
                                                              <node concept="2OqwBi" id="5$zfhXzqQnJ" role="33vP2m">
                                                                <node concept="37vLTw" id="5$zfhXzqQnK" role="2Oq$k0">
                                                                  <ref role="3cqZAo" node="5$zfhXzq_jv" resolve="mc" />
                                                                </node>
                                                                <node concept="liA8E" id="5$zfhXzqQnL" role="2OqNvi">
                                                                  <ref role="37wK5l" to="bim2:5TtkZMYUq8y" resolve="fetchMigrationScript" />
                                                                  <node concept="2ShNRf" id="5$zfhXzqQnM" role="37wK5m">
                                                                    <node concept="1pGfFk" id="5$zfhXzqQnN" role="2ShVmc">
                                                                      <ref role="37wK5l" to="6f4m:1HyHl70Zxpa" resolve="MigrationScriptReference" />
                                                                      <node concept="37vLTw" id="5$zfhXzqQnO" role="37wK5m">
                                                                        <ref role="3cqZAo" node="5$zfhXzqQo4" resolve="it" />
                                                                      </node>
                                                                      <node concept="37vLTw" id="5$zfhXzqQnP" role="37wK5m">
                                                                        <ref role="3cqZAo" node="5$zfhXzqQnC" resolve="ver" />
                                                                      </node>
                                                                    </node>
                                                                  </node>
                                                                  <node concept="3clFbT" id="5$zfhXzrKx8" role="37wK5m">
                                                                    <property role="3clFbU" value="true" />
                                                                  </node>
                                                                </node>
                                                              </node>
                                                            </node>
                                                          </node>
                                                          <node concept="3clFbJ" id="5$zfhXzqQnQ" role="3cqZAp">
                                                            <node concept="3clFbS" id="5$zfhXzqQnR" role="3clFbx">
                                                              <node concept="2n63Yl" id="5$zfhXzqQnS" role="3cqZAp">
                                                                <node concept="37vLTw" id="5$zfhXzqQnT" role="2n6tg2">
                                                                  <ref role="3cqZAo" node="5$zfhXzqQnH" resolve="script" />
                                                                </node>
                                                              </node>
                                                            </node>
                                                            <node concept="1Wc70l" id="5$zfhXzqRV$" role="3clFbw">
                                                              <node concept="2OqwBi" id="5$zfhXzqSbI" role="3uHU7w">
                                                                <node concept="37vLTw" id="5$zfhXzqS3$" role="2Oq$k0">
                                                                  <ref role="3cqZAo" node="5$zfhXzqQnH" resolve="script" />
                                                                </node>
                                                                <node concept="liA8E" id="5$zfhXzrupz" role="2OqNvi">
                                                                  <ref role="37wK5l" to="6f4m:5$zfhXzrsbn" resolve="isRerunnable" />
                                                                </node>
                                                              </node>
                                                              <node concept="1Wc70l" id="5$zfhXzqUPf" role="3uHU7B">
                                                                <node concept="2OqwBi" id="5$zfhXzqVyt" role="3uHU7w">
                                                                  <node concept="2OqwBi" id="5$zfhXzqV5T" role="2Oq$k0">
                                                                    <node concept="37vLTw" id="5$zfhXzqUXv" role="2Oq$k0">
                                                                      <ref role="3cqZAo" node="5$zfhXzqQnH" resolve="script" />
                                                                    </node>
                                                                    <node concept="liA8E" id="5$zfhXzqVeS" role="2OqNvi">
                                                                      <ref role="37wK5l" to="6f4m:2RG318eVG2m" resolve="requiresData" />
                                                                    </node>
                                                                  </node>
                                                                  <node concept="1v1jN8" id="5$zfhXzqWbk" role="2OqNvi" />
                                                                </node>
                                                                <node concept="3y3z36" id="5$zfhXzqQnU" role="3uHU7B">
                                                                  <node concept="37vLTw" id="5$zfhXzqQnW" role="3uHU7B">
                                                                    <ref role="3cqZAo" node="5$zfhXzqQnH" resolve="script" />
                                                                  </node>
                                                                  <node concept="10Nm6u" id="5$zfhXzqQnV" role="3uHU7w" />
                                                                </node>
                                                              </node>
                                                            </node>
                                                          </node>
                                                        </node>
                                                        <node concept="3eOVzh" id="5$zfhXzqQnX" role="1Dwp0S">
                                                          <node concept="2OqwBi" id="5$zfhXzqQnY" role="3uHU7w">
                                                            <node concept="37vLTw" id="5$zfhXzqQnZ" role="2Oq$k0">
                                                              <ref role="3cqZAo" node="5$zfhXzqQo4" resolve="it" />
                                                            </node>
                                                            <node concept="liA8E" id="5$zfhXzqQo0" role="2OqNvi">
                                                              <ref role="37wK5l" to="c17a:~SLanguage.getLanguageVersion():int" resolve="getLanguageVersion" />
                                                            </node>
                                                          </node>
                                                          <node concept="37vLTw" id="5$zfhXzqQo1" role="3uHU7B">
                                                            <ref role="3cqZAo" node="5$zfhXzqQnC" resolve="ver" />
                                                          </node>
                                                        </node>
                                                        <node concept="3uNrnE" id="5$zfhXzqQo2" role="1Dwrff">
                                                          <node concept="37vLTw" id="5$zfhXzqQo3" role="2$L3a6">
                                                            <ref role="3cqZAo" node="5$zfhXzqQnC" resolve="ver" />
                                                          </node>
                                                        </node>
                                                      </node>
                                                    </node>
                                                    <node concept="Rh6nW" id="5$zfhXzqQo4" role="1bW2Oz">
                                                      <property role="TrG5h" value="it" />
                                                      <node concept="2jxLKc" id="5$zfhXzqQo5" role="1tU5fm" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3clFbF" id="5$zfhXzqU48" role="3cqZAp">
                                            <node concept="2OqwBi" id="5$zfhXzqUmI" role="3clFbG">
                                              <node concept="37vLTw" id="5$zfhXzqU46" role="2Oq$k0">
                                                <ref role="3cqZAo" node="5$zfhXzqQnx" resolve="scripts" />
                                              </node>
                                              <node concept="2es0OD" id="5$zfhXzqUBu" role="2OqNvi">
                                                <node concept="1bVj0M" id="5$zfhXzqUBw" role="23t8la">
                                                  <node concept="3clFbS" id="5$zfhXzqUBx" role="1bW5cS">
                                                    <node concept="3clFbF" id="5$zfhXzqUGb" role="3cqZAp">
                                                      <node concept="2OqwBi" id="5$zfhXzqUIc" role="3clFbG">
                                                        <node concept="37vLTw" id="5$zfhXzqUGa" role="2Oq$k0">
                                                          <ref role="3cqZAo" node="5$zfhXzqUBy" resolve="it" />
                                                        </node>
                                                        <node concept="liA8E" id="5$zfhXzqUKz" role="2OqNvi">
                                                          <ref role="37wK5l" to="6f4m:5UnT0284V4S" resolve="execute" />
                                                          <node concept="2GrUjf" id="5$zfhXzqUMM" role="37wK5m">
                                                            <ref role="2Gs0qQ" node="1wrYbVZek6" resolve="module" />
                                                          </node>
                                                        </node>
                                                      </node>
                                                    </node>
                                                  </node>
                                                  <node concept="Rh6nW" id="5$zfhXzqUBy" role="1bW2Oz">
                                                    <property role="TrG5h" value="it" />
                                                    <node concept="2jxLKc" id="5$zfhXzqUBz" role="1tU5fm" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="1wrYbVZekr" role="ukAjM">
                                        <node concept="2OqwBi" id="1wrYbVZx2r" role="2Oq$k0">
                                          <node concept="2WthIp" id="1wrYbVZx2u" role="2Oq$k0">
                                            <ref role="32nkFo" node="5$zfhXzhLy1" resolve="ExecuteRerunnableMigrations" />
                                          </node>
                                          <node concept="1DTwFV" id="1wrYbVZx2w" role="2OqNvi">
                                            <ref role="2WH_rO" node="5$zfhXzjQau" resolve="mpsProject" />
                                          </node>
                                        </node>
                                        <node concept="liA8E" id="1wrYbVZekv" role="2OqNvi">
                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="1wrYbVZekw" role="3cqZAp">
                              <node concept="2OqwBi" id="1wrYbVZekx" role="3clFbG">
                                <node concept="37vLTw" id="1wrYbVZeky" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1wrYbVZejK" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="1wrYbVZekz" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.advance(int):void" resolve="advance" />
                                  <node concept="3cmrfG" id="1wrYbVZek$" role="37wK5m">
                                    <property role="3cmrfH" value="1" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbJ" id="1wrYbVZek_" role="3cqZAp">
                              <node concept="3clFbS" id="1wrYbVZekA" role="3clFbx">
                                <node concept="3zACq4" id="1wrYbVZekB" role="3cqZAp" />
                              </node>
                              <node concept="2OqwBi" id="1wrYbVZekC" role="3clFbw">
                                <node concept="37vLTw" id="1wrYbVZekD" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1wrYbVZejK" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="1wrYbVZekE" role="2OqNvi">
                                  <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.isCanceled():boolean" resolve="isCanceled" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="AH0OO" id="1wrYbVZekF" role="2GsD0m">
                            <node concept="3cmrfG" id="1wrYbVZekG" role="AHEQo">
                              <property role="3cmrfH" value="0" />
                            </node>
                            <node concept="37vLTw" id="1wrYbVZekH" role="AHHXb">
                              <ref role="3cqZAo" node="1wrYbVZej9" resolve="modules" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="1wrYbVZs6B" role="37wK5m">
                      <node concept="2WthIp" id="1wrYbVZs6E" role="2Oq$k0">
                        <ref role="32nkFo" node="5$zfhXzhLy1" resolve="ExecuteRerunnableMigrations" />
                      </node>
                      <node concept="1DTwFV" id="1wrYbVZs6G" role="2OqNvi">
                        <ref role="2WH_rO" node="5$zfhXzqyPE" resolve="project" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="1wrYbVZekL" role="37wK5m">
                      <property role="Xl_RC" value="Run Migrations" />
                    </node>
                    <node concept="3clFbT" id="1wrYbVZekM" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1DS2jV" id="5$zfhXzjQau" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="5$zfhXzjQav" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="5$zfhXzqyPE" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="5$zfhXzqyPF" role="1oa70y" />
    </node>
  </node>
  <node concept="tC5Ba" id="5$zfhXzqWk4">
    <property role="TrG5h" value="Migrations" />
    <property role="3GE5qa" value="migrationList" />
    <property role="1XlLyE" value="true" />
    <node concept="tT9cl" id="5$zfhXzr3Gr" role="2f5YQi">
      <ref role="tU$_T" node="5$zfhXzhGmb" resolve="MigrationsMenu" />
      <ref role="2f8Tey" node="4H96pIbL850" resolve="migrations" />
    </node>
    <node concept="ftmFs" id="7R6nBIbwq8o" role="ftER_">
      <node concept="tCFHf" id="7R6nBIbwq8t" role="ftvYc">
        <ref role="tCJdB" node="5$zfhXzhLy1" resolve="ExecuteRerunnableMigrations" />
      </node>
      <node concept="10WQ6h" id="7R6nBIbwq8B" role="ftvYc">
        <property role="TrG5h" value="projectMigrations" />
      </node>
      <node concept="10WQ6h" id="7R6nBIbwq8J" role="ftvYc">
        <property role="TrG5h" value="languageMigrations" />
      </node>
    </node>
  </node>
  <node concept="sE7Ow" id="5$zfhXzrxUf">
    <property role="TrG5h" value="RunProjectMigration" />
    <property role="2uzpH1" value="Run" />
    <property role="3GE5qa" value="migrationList" />
    <property role="72QZ$" value="true" />
    <node concept="tnohg" id="5$zfhXzrxUg" role="tncku">
      <node concept="3clFbS" id="5$zfhXzrxUh" role="2VODD2">
        <node concept="3clFbF" id="68FOACLu3hW" role="3cqZAp">
          <node concept="2OqwBi" id="68FOACLu3hX" role="3clFbG">
            <node concept="2YIFZM" id="68FOACLu3hY" role="2Oq$k0">
              <ref role="37wK5l" to="xygl:~ProgressManager.getInstance():com.intellij.openapi.progress.ProgressManager" resolve="getInstance" />
              <ref role="1Pybhc" to="xygl:~ProgressManager" resolve="ProgressManager" />
            </node>
            <node concept="liA8E" id="68FOACLu3hZ" role="2OqNvi">
              <ref role="37wK5l" to="xygl:~ProgressManager.run(com.intellij.openapi.progress.Task):void" resolve="run" />
              <node concept="2ShNRf" id="68FOACLu3i0" role="37wK5m">
                <node concept="YeOm9" id="68FOACLu3i1" role="2ShVmc">
                  <node concept="1Y3b0j" id="68FOACLu3i2" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="xygl:~Task$Modal" resolve="Task.Modal" />
                    <ref role="37wK5l" to="xygl:~Task$Modal.&lt;init&gt;(com.intellij.openapi.project.Project,java.lang.String,boolean)" resolve="Task.Modal" />
                    <node concept="3Tm1VV" id="68FOACLu3i3" role="1B3o_S" />
                    <node concept="3clFb_" id="68FOACLu3i4" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="run" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="68FOACLu3i5" role="1B3o_S" />
                      <node concept="3cqZAl" id="68FOACLu3i6" role="3clF45" />
                      <node concept="37vLTG" id="68FOACLu3i7" role="3clF46">
                        <property role="TrG5h" value="progressIndicator" />
                        <node concept="3uibUv" id="68FOACLu3i8" role="1tU5fm">
                          <ref role="3uigEE" to="xygl:~ProgressIndicator" resolve="ProgressIndicator" />
                        </node>
                        <node concept="2AHcQZ" id="68FOACLu3i9" role="2AJF6D">
                          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="68FOACLu3ia" role="3clF47">
                        <node concept="3clFbF" id="68FOACLubRK" role="3cqZAp">
                          <node concept="2OqwBi" id="68FOACLuc5C" role="3clFbG">
                            <node concept="37vLTw" id="68FOACLubRI" role="2Oq$k0">
                              <ref role="3cqZAo" node="68FOACLu3i7" resolve="progressIndicator" />
                            </node>
                            <node concept="liA8E" id="68FOACLucaW" role="2OqNvi">
                              <ref role="37wK5l" to="xygl:~ProgressIndicator.setText(java.lang.String):void" resolve="setText" />
                              <node concept="2OqwBi" id="68FOACLucbk" role="37wK5m">
                                <node concept="2OqwBi" id="68FOACLucbl" role="2Oq$k0">
                                  <node concept="2WthIp" id="68FOACLucbm" role="2Oq$k0" />
                                  <node concept="2BZ7hE" id="68FOACLucbn" role="2OqNvi">
                                    <ref role="2WH_rO" node="5$zfhXzrxUO" resolve="script" />
                                  </node>
                                </node>
                                <node concept="liA8E" id="68FOACLucbo" role="2OqNvi">
                                  <ref role="37wK5l" to="bdll:~ProjectMigration.getDescription():java.lang.String" resolve="getDescription" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="68FOACLu8_S" role="3cqZAp">
                          <node concept="2OqwBi" id="68FOACLu8R5" role="3clFbG">
                            <node concept="37vLTw" id="68FOACLu8_Q" role="2Oq$k0">
                              <ref role="3cqZAo" node="68FOACLu3i7" resolve="progressIndicator" />
                            </node>
                            <node concept="liA8E" id="68FOACLu8Z5" role="2OqNvi">
                              <ref role="37wK5l" to="xygl:~ProgressIndicator.setIndeterminate(boolean):void" resolve="setIndeterminate" />
                              <node concept="3clFbT" id="68FOACLu9a9" role="37wK5m">
                                <property role="3clFbU" value="true" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="68FOACLu7DC" role="3cqZAp">
                          <node concept="2YIFZM" id="68FOACLu7DD" role="3clFbG">
                            <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
                            <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
                            <node concept="1bVj0M" id="68FOACLu7DE" role="37wK5m">
                              <node concept="3clFbS" id="68FOACLu7DF" role="1bW5cS">
                                <node concept="1QHqEO" id="68FOACLu7DG" role="3cqZAp">
                                  <node concept="1QHqEC" id="68FOACLu7DH" role="1QHqEI">
                                    <node concept="3clFbS" id="68FOACLu7DI" role="1bW5cS">
                                      <node concept="3clFbF" id="5$zfhXzr$tv" role="3cqZAp">
                                        <node concept="2OqwBi" id="5$zfhXzr$E3" role="3clFbG">
                                          <node concept="2OqwBi" id="5$zfhXzr$tp" role="2Oq$k0">
                                            <node concept="2WthIp" id="5$zfhXzr$ts" role="2Oq$k0" />
                                            <node concept="2BZ7hE" id="5$zfhXzr$tu" role="2OqNvi">
                                              <ref role="2WH_rO" node="5$zfhXzrxUO" resolve="script" />
                                            </node>
                                          </node>
                                          <node concept="liA8E" id="5$zfhXzr$FX" role="2OqNvi">
                                            <ref role="37wK5l" to="bdll:~ProjectMigration.execute(jetbrains.mps.project.Project):void" resolve="execute" />
                                            <node concept="2OqwBi" id="5$zfhXzr$GE" role="37wK5m">
                                              <node concept="2WthIp" id="5$zfhXzr$GH" role="2Oq$k0" />
                                              <node concept="1DTwFV" id="5$zfhXzr$GJ" role="2OqNvi">
                                                <ref role="2WH_rO" node="5$zfhXzrxV9" resolve="mpsProject" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="68FOACLu7DO" role="ukAjM">
                                    <node concept="2OqwBi" id="68FOACLucom" role="2Oq$k0">
                                      <node concept="2WthIp" id="68FOACLucop" role="2Oq$k0">
                                        <ref role="32nkFo" node="5$zfhXzrxUf" resolve="RunProjectMigration" />
                                      </node>
                                      <node concept="1DTwFV" id="68FOACLucor" role="2OqNvi">
                                        <ref role="2WH_rO" node="5$zfhXzrxV9" resolve="mpsProject" />
                                      </node>
                                    </node>
                                    <node concept="liA8E" id="68FOACLu7DS" role="2OqNvi">
                                      <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="68FOACLu6PY" role="37wK5m">
                      <node concept="2WthIp" id="68FOACLu6Q1" role="2Oq$k0">
                        <ref role="32nkFo" node="5$zfhXzrxUf" resolve="RunProjectMigration" />
                      </node>
                      <node concept="1DTwFV" id="68FOACLu6Q3" role="2OqNvi">
                        <ref role="2WH_rO" node="68FOACLu4fp" resolve="project" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="68FOACLu3kj" role="37wK5m">
                      <property role="Xl_RC" value="Run Migration" />
                    </node>
                    <node concept="3clFbT" id="68FOACLu3kk" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="68FOACLu3h5" role="3cqZAp" />
      </node>
    </node>
    <node concept="tkhdA" id="5$zfhXzrxUB" role="tmbBb">
      <node concept="3clFbS" id="5$zfhXzrxUC" role="2VODD2">
        <node concept="3clFbF" id="5$zfhXzrxUD" role="3cqZAp">
          <node concept="2OqwBi" id="5$zfhXzrxUE" role="3clFbG">
            <node concept="2OqwBi" id="5$zfhXzrxUF" role="2Oq$k0">
              <node concept="tl45R" id="5$zfhXzrxUG" role="2Oq$k0" />
              <node concept="liA8E" id="5$zfhXzrxUH" role="2OqNvi">
                <ref role="37wK5l" to="qkt:~AnActionEvent.getPresentation():com.intellij.openapi.actionSystem.Presentation" resolve="getPresentation" />
              </node>
            </node>
            <node concept="liA8E" id="5$zfhXzrxUI" role="2OqNvi">
              <ref role="37wK5l" to="qkt:~Presentation.setText(java.lang.String):void" resolve="setText" />
              <node concept="2OqwBi" id="5$zfhXzrxUJ" role="37wK5m">
                <node concept="2OqwBi" id="5$zfhXzrxUK" role="2Oq$k0">
                  <node concept="2WthIp" id="5$zfhXzrxUL" role="2Oq$k0" />
                  <node concept="2BZ7hE" id="5$zfhXzrxUM" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzrxUO" resolve="script" />
                  </node>
                </node>
                <node concept="liA8E" id="5$zfhXzrxUN" role="2OqNvi">
                  <ref role="37wK5l" to="bdll:~ProjectMigration.getDescription():java.lang.String" resolve="getDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2JriF1" id="5$zfhXzrxUO" role="2JrayB">
      <property role="TrG5h" value="script" />
      <node concept="3Tm6S6" id="5$zfhXzrxUP" role="1B3o_S" />
      <node concept="3uibUv" id="5$zfhXzryKf" role="1tU5fm">
        <ref role="3uigEE" to="bdll:~ProjectMigration" resolve="ProjectMigration" />
      </node>
      <node concept="2K2imR" id="5$zfhXzrxUR" role="2K2Cet">
        <node concept="3clFbS" id="5$zfhXzrxUS" role="2VODD2">
          <node concept="3clFbF" id="5$zfhXzrxUT" role="3cqZAp">
            <node concept="2OqwBi" id="5$zfhXzrxUY" role="3clFbG">
              <node concept="2OqwBi" id="5$zfhXzrxUZ" role="2Oq$k0">
                <node concept="2K3dj_" id="5$zfhXzrxV0" role="2Oq$k0" />
                <node concept="liA8E" id="5$zfhXzrxV1" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="liA8E" id="5$zfhXzrxV2" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Class.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1DS2jV" id="5$zfhXzrxV9" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="5$zfhXzrxVa" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="68FOACLu4fp" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="68FOACLu4fq" role="1oa70y" />
    </node>
  </node>
  <node concept="2DaZZR" id="aOQ7vgq8Xg" />
  <node concept="9BnSX" id="vzRt1VjcfK">
    <property role="9BnSZ" value="foo" />
    <property role="TrG5h" value="foo" />
    <property role="9BnSL" value="foo" />
    <property role="9BnSK" value="foo" />
    <property role="9BnSM" value="foo" />
    <property role="9BnSP" value="foo" />
    <property role="1qHB85" value="foo" />
    <property role="9BnSO" value="foo" />
  </node>
  <node concept="sE7Ow" id="5$zfhXzhIiu">
    <property role="TrG5h" value="RunPreUpdateCheck" />
    <property role="2uzpH1" value="Run Pre-Update Check" />
    <property role="72QZ$" value="true" />
    <node concept="1DS2jV" id="5$zfhXzr51q" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="5$zfhXzr51r" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="5$zfhXzr51s" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="5$zfhXzr51t" role="1oa70y" />
    </node>
    <node concept="tnohg" id="5$zfhXzhIiv" role="tncku">
      <node concept="3clFbS" id="5$zfhXzhIiw" role="2VODD2">
        <node concept="3cpWs8" id="5$zfhXzreRo" role="3cqZAp">
          <node concept="3cpWsn" id="5$zfhXzreRp" role="3cpWs9">
            <property role="TrG5h" value="problems" />
            <node concept="_YKpA" id="5$zfhXzvO4t" role="1tU5fm">
              <node concept="3uibUv" id="5$zfhXzvO4v" role="_ZDj9">
                <ref role="3uigEE" to="6f4m:3n7MNzO_IjP" resolve="Problem" />
              </node>
            </node>
            <node concept="2ShNRf" id="5$zfhXzvGnN" role="33vP2m">
              <node concept="Tc6Ow" id="5$zfhXzvGTD" role="2ShVmc">
                <node concept="3uibUv" id="5$zfhXzvHv7" role="HW$YZ">
                  <ref role="3uigEE" to="6f4m:3n7MNzO_IjP" resolve="Problem" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5$zfhXzr60j" role="3cqZAp">
          <node concept="3cpWsn" id="5$zfhXzr60k" role="3cpWs9">
            <property role="TrG5h" value="mc" />
            <node concept="2OqwBi" id="5$zfhXzr60o" role="33vP2m">
              <node concept="2OqwBi" id="5$zfhXzr60p" role="2Oq$k0">
                <node concept="2WthIp" id="5$zfhXzr60q" role="2Oq$k0" />
                <node concept="1DTwFV" id="5$zfhXzr60r" role="2OqNvi">
                  <ref role="2WH_rO" node="5$zfhXzr51s" resolve="project" />
                </node>
              </node>
              <node concept="liA8E" id="5$zfhXzr60s" role="2OqNvi">
                <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                <node concept="3VsKOn" id="5$zfhXzr60t" role="37wK5m">
                  <ref role="3VsUkX" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
                </node>
              </node>
            </node>
            <node concept="3uibUv" id="7ew$qV_fp76" role="1tU5fm">
              <ref role="3uigEE" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7ew$qV_f9Ii" role="3cqZAp">
          <node concept="3cpWsn" id="7ew$qV_f9Ij" role="3cpWs9">
            <property role="TrG5h" value="modules" />
            <property role="3TUv4t" value="true" />
            <node concept="10Q1$e" id="7ew$qV_f9Ik" role="1tU5fm">
              <node concept="3uibUv" id="7ew$qV_f9Il" role="10Q1$1">
                <ref role="3uigEE" to="33ny:~List" resolve="List" />
                <node concept="3uibUv" id="7ew$qV_f9Im" role="11_B2D">
                  <ref role="3uigEE" to="lui2:~SModule" resolve="SModule" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="7ew$qV_f9In" role="33vP2m">
              <node concept="3$_iS1" id="7ew$qV_f9Io" role="2ShVmc">
                <node concept="3$GHV9" id="7ew$qV_f9Ip" role="3$GQph">
                  <node concept="3cmrfG" id="7ew$qV_f9Iq" role="3$I4v7">
                    <property role="3cmrfH" value="1" />
                  </node>
                </node>
                <node concept="3uibUv" id="7ew$qV_f9Ir" role="3$_nBY">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1QHqEK" id="7ew$qV_f9Is" role="3cqZAp">
          <node concept="1QHqEC" id="7ew$qV_f9It" role="1QHqEI">
            <node concept="3clFbS" id="7ew$qV_f9Iu" role="1bW5cS">
              <node concept="3clFbF" id="7ew$qV_f9Iv" role="3cqZAp">
                <node concept="37vLTI" id="7ew$qV_f9Iw" role="3clFbG">
                  <node concept="AH0OO" id="7ew$qV_f9Ix" role="37vLTJ">
                    <node concept="3cmrfG" id="7ew$qV_f9Iy" role="AHEQo">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="37vLTw" id="7ew$qV_f9Iz" role="AHHXb">
                      <ref role="3cqZAo" node="7ew$qV_f9Ij" resolve="modules" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7ew$qV_f9I$" role="37vLTx">
                    <node concept="2YIFZM" id="7ew$qV_f9I_" role="2Oq$k0">
                      <ref role="37wK5l" to="auc7:3UfGsecu96H" resolve="getMigrateableModulesFromProject" />
                      <ref role="1Pybhc" to="auc7:3UfGsecu96G" resolve="MigrationsUtil" />
                      <node concept="2OqwBi" id="7ew$qV_f9IA" role="37wK5m">
                        <node concept="2WthIp" id="7ew$qV_f9IB" role="2Oq$k0" />
                        <node concept="1DTwFV" id="7ew$qV_f9IC" role="2OqNvi">
                          <ref role="2WH_rO" node="5$zfhXzr51q" resolve="mpsProject" />
                        </node>
                      </node>
                    </node>
                    <node concept="ANE8D" id="7ew$qV_f9ID" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7ew$qV_f9IE" role="ukAjM">
            <node concept="2OqwBi" id="7ew$qV_f9IF" role="2Oq$k0">
              <node concept="2WthIp" id="7ew$qV_f9IG" role="2Oq$k0" />
              <node concept="1DTwFV" id="7ew$qV_f9IH" role="2OqNvi">
                <ref role="2WH_rO" node="5$zfhXzr51q" resolve="mpsProject" />
              </node>
            </node>
            <node concept="liA8E" id="7ew$qV_f9II" role="2OqNvi">
              <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7ew$qV_fvso" role="3cqZAp" />
        <node concept="3clFbF" id="7ew$qV_f9IJ" role="3cqZAp">
          <node concept="2OqwBi" id="7ew$qV_f9IK" role="3clFbG">
            <node concept="2YIFZM" id="7ew$qV_f9IL" role="2Oq$k0">
              <ref role="37wK5l" to="xygl:~ProgressManager.getInstance():com.intellij.openapi.progress.ProgressManager" resolve="getInstance" />
              <ref role="1Pybhc" to="xygl:~ProgressManager" resolve="ProgressManager" />
            </node>
            <node concept="liA8E" id="7ew$qV_f9IM" role="2OqNvi">
              <ref role="37wK5l" to="xygl:~ProgressManager.run(com.intellij.openapi.progress.Task):void" resolve="run" />
              <node concept="2ShNRf" id="7ew$qV_f9IN" role="37wK5m">
                <node concept="YeOm9" id="7ew$qV_f9IO" role="2ShVmc">
                  <node concept="1Y3b0j" id="7ew$qV_f9IP" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="xygl:~Task$Modal" resolve="Task.Modal" />
                    <ref role="37wK5l" to="xygl:~Task$Modal.&lt;init&gt;(com.intellij.openapi.project.Project,java.lang.String,boolean)" resolve="Task.Modal" />
                    <node concept="3Tm1VV" id="7ew$qV_f9IQ" role="1B3o_S" />
                    <node concept="3clFb_" id="7ew$qV_f9IR" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="run" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="7ew$qV_f9IS" role="1B3o_S" />
                      <node concept="3cqZAl" id="7ew$qV_f9IT" role="3clF45" />
                      <node concept="37vLTG" id="7ew$qV_f9IU" role="3clF46">
                        <property role="TrG5h" value="progressIndicator" />
                        <node concept="3uibUv" id="7ew$qV_f9IV" role="1tU5fm">
                          <ref role="3uigEE" to="xygl:~ProgressIndicator" resolve="ProgressIndicator" />
                        </node>
                        <node concept="2AHcQZ" id="7ew$qV_f9IW" role="2AJF6D">
                          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="7ew$qV_f9IX" role="3clF47">
                        <node concept="3cpWs8" id="7ew$qV_f9IY" role="3cqZAp">
                          <node concept="3cpWsn" id="7ew$qV_f9IZ" role="3cpWs9">
                            <property role="TrG5h" value="progressMonitor" />
                            <node concept="3uibUv" id="7ew$qV_f9J0" role="1tU5fm">
                              <ref role="3uigEE" to="mk8z:~ProgressMonitorAdapter" resolve="ProgressMonitorAdapter" />
                            </node>
                            <node concept="2ShNRf" id="7ew$qV_f9J1" role="33vP2m">
                              <node concept="1pGfFk" id="7ew$qV_f9J2" role="2ShVmc">
                                <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.&lt;init&gt;(com.intellij.openapi.progress.ProgressIndicator)" resolve="ProgressMonitorAdapter" />
                                <node concept="37vLTw" id="7ew$qV_f9J3" role="37wK5m">
                                  <ref role="3cqZAo" node="7ew$qV_f9IU" resolve="progressIndicator" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cpWs8" id="7ew$qV_f9J4" role="3cqZAp">
                          <node concept="3cpWsn" id="7ew$qV_f9J5" role="3cpWs9">
                            <property role="TrG5h" value="steps" />
                            <node concept="2OqwBi" id="7ew$qV_f9J6" role="33vP2m">
                              <node concept="AH0OO" id="7ew$qV_f9J7" role="2Oq$k0">
                                <node concept="3cmrfG" id="7ew$qV_f9J8" role="AHEQo">
                                  <property role="3cmrfH" value="0" />
                                </node>
                                <node concept="37vLTw" id="7ew$qV_f9J9" role="AHHXb">
                                  <ref role="3cqZAo" node="7ew$qV_f9Ij" resolve="modules" />
                                </node>
                              </node>
                              <node concept="liA8E" id="7ew$qV_f9Ja" role="2OqNvi">
                                <ref role="37wK5l" to="33ny:~List.size():int" resolve="size" />
                              </node>
                            </node>
                            <node concept="10Oyi0" id="7ew$qV_f9Jb" role="1tU5fm" />
                          </node>
                        </node>
                        <node concept="3clFbF" id="7ew$qV_f9Jc" role="3cqZAp">
                          <node concept="2OqwBi" id="7ew$qV_f9Jd" role="3clFbG">
                            <node concept="37vLTw" id="7ew$qV_f9Je" role="2Oq$k0">
                              <ref role="3cqZAo" node="7ew$qV_f9IZ" resolve="progressMonitor" />
                            </node>
                            <node concept="liA8E" id="7ew$qV_f9Jf" role="2OqNvi">
                              <ref role="37wK5l" to="mk90:~ProgressMonitorBase.start(java.lang.String,int):void" resolve="start" />
                              <node concept="Xl_RD" id="7ew$qV_f9Jg" role="37wK5m">
                                <property role="Xl_RC" value="Running..." />
                              </node>
                              <node concept="37vLTw" id="7ew$qV_f9Jh" role="37wK5m">
                                <ref role="3cqZAo" node="7ew$qV_f9J5" resolve="steps" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2Gpval" id="7ew$qV_f9Ji" role="3cqZAp">
                          <node concept="2GrKxI" id="7ew$qV_f9Jj" role="2Gsz3X">
                            <property role="TrG5h" value="module" />
                          </node>
                          <node concept="3clFbS" id="7ew$qV_f9Jk" role="2LFqv$">
                            <node concept="3clFbF" id="7ew$qV_f9Jl" role="3cqZAp">
                              <node concept="2OqwBi" id="7ew$qV_f9Jm" role="3clFbG">
                                <node concept="37vLTw" id="7ew$qV_f9Jn" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7ew$qV_f9IZ" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="7ew$qV_f9Jo" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.step(java.lang.String):void" resolve="step" />
                                  <node concept="2OqwBi" id="7ew$qV_f9Jp" role="37wK5m">
                                    <node concept="2GrUjf" id="7ew$qV_f9Jq" role="2Oq$k0">
                                      <ref role="2Gs0qQ" node="7ew$qV_f9Jj" resolve="module" />
                                    </node>
                                    <node concept="liA8E" id="7ew$qV_f9Jr" role="2OqNvi">
                                      <ref role="37wK5l" to="lui2:~SModule.getModuleName():java.lang.String" resolve="getModuleName" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7ew$qV_f9Js" role="3cqZAp">
                              <node concept="2YIFZM" id="7ew$qV_f9Jt" role="3clFbG">
                                <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
                                <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
                                <node concept="1bVj0M" id="7ew$qV_f9Ju" role="37wK5m">
                                  <node concept="3clFbS" id="7ew$qV_f9Jv" role="1bW5cS">
                                    <node concept="1QHqEO" id="7ew$qV_f9Jw" role="3cqZAp">
                                      <node concept="1QHqEC" id="7ew$qV_f9Jx" role="1QHqEI">
                                        <node concept="3clFbS" id="7ew$qV_f9Jy" role="1bW5cS">
                                          <node concept="3cpWs8" id="5$zfhXzr60B" role="3cqZAp">
                                            <node concept="3cpWsn" id="5$zfhXzr60C" role="3cpWs9">
                                              <property role="TrG5h" value="languages" />
                                              <node concept="2hMVRd" id="5$zfhXzr60D" role="1tU5fm">
                                                <node concept="3uibUv" id="5$zfhXzr60E" role="2hN53Y">
                                                  <ref role="3uigEE" to="c17a:~SLanguage" resolve="SLanguage" />
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="5$zfhXzr60F" role="33vP2m">
                                                <node concept="2ShNRf" id="5$zfhXzr60G" role="2Oq$k0">
                                                  <node concept="1pGfFk" id="5$zfhXzr60H" role="2ShVmc">
                                                    <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.&lt;init&gt;(jetbrains.mps.smodel.language.LanguageRegistry,java.util.Collection)" resolve="SLanguageHierarchy" />
                                                    <node concept="2YIFZM" id="7ew$qV_ggu_" role="37wK5m">
                                                      <ref role="37wK5l" to="vndm:~LanguageRegistry.getInstance(org.jetbrains.mps.openapi.module.SRepository):jetbrains.mps.smodel.language.LanguageRegistry" resolve="getInstance" />
                                                      <ref role="1Pybhc" to="vndm:~LanguageRegistry" resolve="LanguageRegistry" />
                                                      <node concept="2OqwBi" id="7ew$qV_gguA" role="37wK5m">
                                                        <node concept="2OqwBi" id="7ew$qV_gguB" role="2Oq$k0">
                                                          <node concept="2WthIp" id="7ew$qV_gguC" role="2Oq$k0">
                                                            <ref role="32nkFo" node="5$zfhXzhIiu" resolve="RunPreUpdateCheck" />
                                                          </node>
                                                          <node concept="1DTwFV" id="7ew$qV_gh9R" role="2OqNvi">
                                                            <ref role="2WH_rO" node="5$zfhXzr51q" resolve="mpsProject" />
                                                          </node>
                                                        </node>
                                                        <node concept="liA8E" id="7ew$qV_gguE" role="2OqNvi">
                                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                                        </node>
                                                      </node>
                                                    </node>
                                                    <node concept="2OqwBi" id="5$zfhXzr60I" role="37wK5m">
                                                      <node concept="2GrUjf" id="7ew$qV_fz8E" role="2Oq$k0">
                                                        <ref role="2Gs0qQ" node="7ew$qV_f9Jj" resolve="module" />
                                                      </node>
                                                      <node concept="liA8E" id="5$zfhXzr60K" role="2OqNvi">
                                                        <ref role="37wK5l" to="lui2:~SModule.getUsedLanguages():java.util.Set" resolve="getUsedLanguages" />
                                                      </node>
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="5$zfhXzr60L" role="2OqNvi">
                                                  <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.getExtended():java.util.Set" resolve="getExtended" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3cpWs8" id="5$zfhXzr60M" role="3cqZAp">
                                            <node concept="3cpWsn" id="5$zfhXzr60N" role="3cpWs9">
                                              <property role="TrG5h" value="scripts" />
                                              <node concept="A3Dl8" id="5$zfhXzr60O" role="1tU5fm">
                                                <node concept="3uibUv" id="5$zfhXzr60P" role="A3Ik2">
                                                  <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="5$zfhXzr60Q" role="33vP2m">
                                                <node concept="37vLTw" id="5$zfhXzr60R" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="5$zfhXzr60C" resolve="languages" />
                                                </node>
                                                <node concept="3goQfb" id="5$zfhXzr60S" role="2OqNvi">
                                                  <node concept="1bVj0M" id="5$zfhXzr60T" role="23t8la">
                                                    <node concept="3clFbS" id="5$zfhXzr60U" role="1bW5cS">
                                                      <node concept="1Dw8fO" id="5$zfhXzr60V" role="3cqZAp">
                                                        <node concept="3cpWsn" id="5$zfhXzr60W" role="1Duv9x">
                                                          <property role="TrG5h" value="ver" />
                                                          <node concept="10Oyi0" id="5$zfhXzr60X" role="1tU5fm" />
                                                          <node concept="3cmrfG" id="5$zfhXzr60Y" role="33vP2m">
                                                            <property role="3cmrfH" value="0" />
                                                          </node>
                                                        </node>
                                                        <node concept="3clFbS" id="5$zfhXzr60Z" role="2LFqv$">
                                                          <node concept="3cpWs8" id="5$zfhXzr610" role="3cqZAp">
                                                            <node concept="3cpWsn" id="5$zfhXzr611" role="3cpWs9">
                                                              <property role="TrG5h" value="script" />
                                                              <node concept="3uibUv" id="5$zfhXzr612" role="1tU5fm">
                                                                <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
                                                              </node>
                                                              <node concept="2OqwBi" id="5$zfhXzr613" role="33vP2m">
                                                                <node concept="37vLTw" id="5$zfhXzr614" role="2Oq$k0">
                                                                  <ref role="3cqZAo" node="5$zfhXzr60k" resolve="mc" />
                                                                </node>
                                                                <node concept="liA8E" id="5$zfhXzr615" role="2OqNvi">
                                                                  <ref role="37wK5l" to="bim2:5TtkZMYUq8y" resolve="fetchMigrationScript" />
                                                                  <node concept="2ShNRf" id="5$zfhXzr616" role="37wK5m">
                                                                    <node concept="1pGfFk" id="5$zfhXzr617" role="2ShVmc">
                                                                      <ref role="37wK5l" to="6f4m:1HyHl70Zxpa" resolve="MigrationScriptReference" />
                                                                      <node concept="37vLTw" id="5$zfhXzr618" role="37wK5m">
                                                                        <ref role="3cqZAo" node="5$zfhXzr61$" resolve="it" />
                                                                      </node>
                                                                      <node concept="37vLTw" id="5$zfhXzr619" role="37wK5m">
                                                                        <ref role="3cqZAo" node="5$zfhXzr60W" resolve="ver" />
                                                                      </node>
                                                                    </node>
                                                                  </node>
                                                                  <node concept="3clFbT" id="5$zfhXzrLvm" role="37wK5m">
                                                                    <property role="3clFbU" value="true" />
                                                                  </node>
                                                                </node>
                                                              </node>
                                                            </node>
                                                          </node>
                                                          <node concept="3clFbJ" id="5$zfhXzr61c" role="3cqZAp">
                                                            <node concept="3clFbS" id="5$zfhXzr61d" role="3clFbx">
                                                              <node concept="2n63Yl" id="5$zfhXzr61e" role="3cqZAp">
                                                                <node concept="37vLTw" id="5$zfhXzr61f" role="2n6tg2">
                                                                  <ref role="3cqZAo" node="5$zfhXzr611" resolve="script" />
                                                                </node>
                                                              </node>
                                                            </node>
                                                            <node concept="3y3z36" id="5$zfhXzr61q" role="3clFbw">
                                                              <node concept="37vLTw" id="5$zfhXzr61r" role="3uHU7B">
                                                                <ref role="3cqZAo" node="5$zfhXzr611" resolve="script" />
                                                              </node>
                                                              <node concept="10Nm6u" id="5$zfhXzr61s" role="3uHU7w" />
                                                            </node>
                                                          </node>
                                                        </node>
                                                        <node concept="3eOVzh" id="5$zfhXzr61t" role="1Dwp0S">
                                                          <node concept="2OqwBi" id="5$zfhXzr61u" role="3uHU7w">
                                                            <node concept="37vLTw" id="5$zfhXzr61v" role="2Oq$k0">
                                                              <ref role="3cqZAo" node="5$zfhXzr61$" resolve="it" />
                                                            </node>
                                                            <node concept="liA8E" id="5$zfhXzr61w" role="2OqNvi">
                                                              <ref role="37wK5l" to="c17a:~SLanguage.getLanguageVersion():int" resolve="getLanguageVersion" />
                                                            </node>
                                                          </node>
                                                          <node concept="37vLTw" id="5$zfhXzr61x" role="3uHU7B">
                                                            <ref role="3cqZAo" node="5$zfhXzr60W" resolve="ver" />
                                                          </node>
                                                        </node>
                                                        <node concept="3uNrnE" id="5$zfhXzr61y" role="1Dwrff">
                                                          <node concept="37vLTw" id="5$zfhXzr61z" role="2$L3a6">
                                                            <ref role="3cqZAo" node="5$zfhXzr60W" resolve="ver" />
                                                          </node>
                                                        </node>
                                                      </node>
                                                    </node>
                                                    <node concept="Rh6nW" id="5$zfhXzr61$" role="1bW2Oz">
                                                      <property role="TrG5h" value="it" />
                                                      <node concept="2jxLKc" id="5$zfhXzr61_" role="1tU5fm" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3clFbF" id="5$zfhXzvHSm" role="3cqZAp">
                                            <node concept="2OqwBi" id="5$zfhXzvIf9" role="3clFbG">
                                              <node concept="37vLTw" id="5$zfhXzvNs0" role="2Oq$k0">
                                                <ref role="3cqZAo" node="5$zfhXzreRp" resolve="problems" />
                                              </node>
                                              <node concept="X8dFx" id="5$zfhXzvIKj" role="2OqNvi">
                                                <node concept="2OqwBi" id="5$zfhXzreRq" role="25WWJ7">
                                                  <node concept="37vLTw" id="5$zfhXzreRr" role="2Oq$k0">
                                                    <ref role="3cqZAo" node="5$zfhXzr60N" resolve="scripts" />
                                                  </node>
                                                  <node concept="3goQfb" id="5$zfhXzreRs" role="2OqNvi">
                                                    <node concept="1bVj0M" id="5$zfhXzreRt" role="23t8la">
                                                      <node concept="3clFbS" id="5$zfhXzreRu" role="1bW5cS">
                                                        <node concept="3clFbF" id="5$zfhXzreRv" role="3cqZAp">
                                                          <node concept="2OqwBi" id="5$zfhXzreRw" role="3clFbG">
                                                            <node concept="37vLTw" id="5$zfhXzreRx" role="2Oq$k0">
                                                              <ref role="3cqZAo" node="5$zfhXzreR$" resolve="it" />
                                                            </node>
                                                            <node concept="liA8E" id="5$zfhXzreRy" role="2OqNvi">
                                                              <ref role="37wK5l" to="6f4m:5$zfhXzr7lw" resolve="check" />
                                                              <node concept="2GrUjf" id="7ew$qV_fz_P" role="37wK5m">
                                                                <ref role="2Gs0qQ" node="7ew$qV_f9Jj" resolve="module" />
                                                              </node>
                                                            </node>
                                                          </node>
                                                        </node>
                                                      </node>
                                                      <node concept="Rh6nW" id="5$zfhXzreR$" role="1bW2Oz">
                                                        <property role="TrG5h" value="it" />
                                                        <node concept="2jxLKc" id="5$zfhXzreR_" role="1tU5fm" />
                                                      </node>
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="7ew$qV_f9KO" role="ukAjM">
                                        <node concept="2OqwBi" id="7ew$qV_f9KP" role="2Oq$k0">
                                          <node concept="2WthIp" id="7ew$qV_f9KQ" role="2Oq$k0">
                                            <ref role="32nkFo" node="5$zfhXzhIiu" resolve="RunPreUpdateCheck" />
                                          </node>
                                          <node concept="1DTwFV" id="7ew$qV_fsRT" role="2OqNvi">
                                            <ref role="2WH_rO" node="5$zfhXzr51q" resolve="mpsProject" />
                                          </node>
                                        </node>
                                        <node concept="liA8E" id="7ew$qV_f9KS" role="2OqNvi">
                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7ew$qV_f9KT" role="3cqZAp">
                              <node concept="2OqwBi" id="7ew$qV_f9KU" role="3clFbG">
                                <node concept="37vLTw" id="7ew$qV_f9KV" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7ew$qV_f9IZ" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="7ew$qV_f9KW" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.advance(int):void" resolve="advance" />
                                  <node concept="3cmrfG" id="7ew$qV_f9KX" role="37wK5m">
                                    <property role="3cmrfH" value="1" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbJ" id="7ew$qV_f9KY" role="3cqZAp">
                              <node concept="3clFbS" id="7ew$qV_f9KZ" role="3clFbx">
                                <node concept="3zACq4" id="7ew$qV_f9L0" role="3cqZAp" />
                              </node>
                              <node concept="2OqwBi" id="7ew$qV_f9L1" role="3clFbw">
                                <node concept="37vLTw" id="7ew$qV_f9L2" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7ew$qV_f9IZ" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="7ew$qV_f9L3" role="2OqNvi">
                                  <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.isCanceled():boolean" resolve="isCanceled" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="AH0OO" id="7ew$qV_f9L4" role="2GsD0m">
                            <node concept="3cmrfG" id="7ew$qV_f9L5" role="AHEQo">
                              <property role="3cmrfH" value="0" />
                            </node>
                            <node concept="37vLTw" id="7ew$qV_f9L6" role="AHHXb">
                              <ref role="3cqZAo" node="7ew$qV_f9Ij" resolve="modules" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="7ew$qV_f9L7" role="37wK5m">
                      <node concept="2WthIp" id="7ew$qV_f9L8" role="2Oq$k0">
                        <ref role="32nkFo" node="5$zfhXzhIiu" resolve="RunPreUpdateCheck" />
                      </node>
                      <node concept="1DTwFV" id="7ew$qV_ft$K" role="2OqNvi">
                        <ref role="2WH_rO" node="5$zfhXzr51s" resolve="project" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="7ew$qV_f9La" role="37wK5m">
                      <property role="Xl_RC" value="Pre-Update Check" />
                    </node>
                    <node concept="3clFbT" id="7ew$qV_f9Lb" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1v09psn9tTg" role="3cqZAp" />
        <node concept="3clFbJ" id="5$zfhXzrekL" role="3cqZAp">
          <node concept="3clFbS" id="5$zfhXzrekN" role="3clFbx">
            <node concept="3clFbF" id="5$zfhXzrgSQ" role="3cqZAp">
              <node concept="2YIFZM" id="5$zfhXzrgT9" role="3clFbG">
                <ref role="1Pybhc" to="jkm4:~Messages" resolve="Messages" />
                <ref role="37wK5l" to="jkm4:~Messages.showMessageDialog(com.intellij.openapi.project.Project,java.lang.String,java.lang.String,javax.swing.Icon):void" resolve="showMessageDialog" />
                <node concept="2OqwBi" id="5$zfhXzrgYc" role="37wK5m">
                  <node concept="2WthIp" id="5$zfhXzrgYf" role="2Oq$k0" />
                  <node concept="1DTwFV" id="5$zfhXzrgYh" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzr51s" resolve="project" />
                  </node>
                </node>
                <node concept="Xl_RD" id="5$zfhXzrlKw" role="37wK5m">
                  <property role="Xl_RC" value="No problems found.\nProject can be migrated" />
                </node>
                <node concept="Xl_RD" id="5$zfhXzrh08" role="37wK5m">
                  <property role="Xl_RC" value="Migration" />
                </node>
                <node concept="10Nm6u" id="5$zfhXzrmKK" role="37wK5m" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5$zfhXzrfLy" role="3clFbw">
            <node concept="37vLTw" id="5$zfhXzrfwG" role="2Oq$k0">
              <ref role="3cqZAo" node="5$zfhXzreRp" resolve="problems" />
            </node>
            <node concept="1v1jN8" id="5$zfhXzrguE" role="2OqNvi" />
          </node>
          <node concept="9aQIb" id="7ew$qV_fA$S" role="9aQIa">
            <node concept="3clFbS" id="7ew$qV_fA$T" role="9aQI4">
              <node concept="3clFbF" id="5$zfhXztK4n" role="3cqZAp">
                <node concept="2YIFZM" id="5$zfhXztKqy" role="3clFbG">
                  <ref role="1Pybhc" to="t99v:5$zfhXzsScF" resolve="MigrationOutputUtil" />
                  <ref role="37wK5l" to="t99v:5$zfhXzsSdf" resolve="showProblems" />
                  <node concept="2OqwBi" id="5$zfhXztNfy" role="37wK5m">
                    <node concept="2WthIp" id="5$zfhXztNf_" role="2Oq$k0" />
                    <node concept="1DTwFV" id="5$zfhXztNfB" role="2OqNvi">
                      <ref role="2WH_rO" node="5$zfhXzr51s" resolve="project" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="5$zfhXztKGx" role="37wK5m">
                    <ref role="3cqZAo" node="5$zfhXzreRp" resolve="problems" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="tC5Ba" id="5$zfhXzhGmb">
    <property role="TrG5h" value="MigrationsMenu" />
    <node concept="tT9cl" id="5$zfhXzhGmd" role="2f5YQi">
      <ref role="tU$_T" node="5$zfhXzhGmz" resolve="IDEAMigration" />
    </node>
    <node concept="ftmFs" id="5$zfhXzhIip" role="ftER_">
      <node concept="tCFHf" id="5$zfhXzqWjN" role="ftvYc">
        <ref role="tCJdB" node="6MUcJMtBJJe" resolve="ExecuteMigrationAssistant" />
      </node>
      <node concept="tCFHf" id="51adnVzU34O" role="ftvYc">
        <ref role="tCJdB" node="51adnVzU34y" resolve="ShowDeprecatedStuff" />
      </node>
      <node concept="tCFHf" id="5$zfhXzhIis" role="ftvYc">
        <ref role="tCJdB" node="5$zfhXzhGml" resolve="ShowDeprecatedUsages" />
      </node>
      <node concept="tCFHf" id="5$zfhXzhLyp" role="ftvYc">
        <ref role="tCJdB" node="5$zfhXzhIiu" resolve="RunPreUpdateCheck" />
      </node>
      <node concept="10WQ6h" id="4H96pIbL850" role="ftvYc">
        <property role="TrG5h" value="migrations" />
      </node>
      <node concept="10WQ6h" id="4H96pIbL85e" role="ftvYc">
        <property role="TrG5h" value="utils" />
      </node>
    </node>
  </node>
  <node concept="1ESbSp" id="5$zfhXzhGmz">
    <property role="TrG5h" value="IDEAMigration" />
    <node concept="ftmFs" id="5$zfhXzhIig" role="ftER_" />
    <node concept="Xl_RD" id="5$zfhXzhIhK" role="3mKD$K">
      <property role="Xl_RC" value="Migration" />
    </node>
  </node>
  <node concept="sE7Ow" id="5$zfhXzhGml">
    <property role="TrG5h" value="ShowDeprecatedUsages" />
    <property role="2uzpH1" value="Show Usages of Deprecated" />
    <node concept="tnohg" id="5$zfhXzhGmm" role="tncku">
      <node concept="3clFbS" id="5$zfhXzhGmn" role="2VODD2">
        <node concept="3cpWs8" id="5$zfhXziDHi" role="3cqZAp">
          <node concept="3cpWsn" id="5$zfhXziDHj" role="3cpWs9">
            <property role="TrG5h" value="theirModules" />
            <node concept="2hMVRd" id="5$zfhXziEs9" role="1tU5fm">
              <node concept="3uibUv" id="5$zfhXziEsb" role="2hN53Y">
                <ref role="3uigEE" to="lui2:~SModule" resolve="SModule" />
              </node>
            </node>
            <node concept="2ShNRf" id="5$zfhXziFlM" role="33vP2m">
              <node concept="2i4dXS" id="5$zfhXziFl_" role="2ShVmc">
                <node concept="3uibUv" id="5$zfhXziFlA" role="HW$YZ">
                  <ref role="3uigEE" to="lui2:~SModule" resolve="SModule" />
                </node>
                <node concept="2OqwBi" id="5$zfhXziDHk" role="I$8f6">
                  <node concept="2OqwBi" id="5$zfhXziDHl" role="2Oq$k0">
                    <node concept="2OqwBi" id="5$zfhXziDHm" role="2Oq$k0">
                      <node concept="2WthIp" id="5$zfhXziDHn" role="2Oq$k0" />
                      <node concept="1DTwFV" id="5$zfhXziDHo" role="2OqNvi">
                        <ref role="2WH_rO" node="5$zfhXzi2u9" resolve="mpsProject" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5$zfhXziDHp" role="2OqNvi">
                      <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                    </node>
                  </node>
                  <node concept="liA8E" id="5$zfhXziDHq" role="2OqNvi">
                    <ref role="37wK5l" to="lui2:~SRepository.getModules():java.lang.Iterable" resolve="getModules" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5$zfhXziGSV" role="3cqZAp">
          <node concept="2OqwBi" id="5$zfhXziHwc" role="3clFbG">
            <node concept="37vLTw" id="5$zfhXziGST" role="2Oq$k0">
              <ref role="3cqZAo" node="5$zfhXziDHj" resolve="theirModules" />
            </node>
            <node concept="1kEaZ2" id="5$zfhXziIpa" role="2OqNvi">
              <node concept="2OqwBi" id="5$zfhXziJof" role="25WWJ7">
                <node concept="2OqwBi" id="5$zfhXziIH7" role="2Oq$k0">
                  <node concept="2WthIp" id="5$zfhXziIHa" role="2Oq$k0" />
                  <node concept="1DTwFV" id="5$zfhXziIHc" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzi2u9" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="5$zfhXziKdg" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getModulesWithGenerators():java.lang.Iterable" resolve="getModulesWithGenerators" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="51adnVzXqji" role="3cqZAp">
          <node concept="3cpWsn" id="51adnVzXqjj" role="3cpWs9">
            <property role="TrG5h" value="depLibs" />
            <node concept="2hMVRd" id="51adnVzXqiE" role="1tU5fm">
              <node concept="3Tqbb2" id="51adnVzXqiH" role="2hN53Y" />
            </node>
            <node concept="2YIFZM" id="51adnVzXqjk" role="33vP2m">
              <ref role="37wK5l" to="auc7:51adnVzUmYa" resolve="usagesOfDeprecated" />
              <ref role="1Pybhc" to="auc7:51adnVzUm$d" resolve="DeprecatedUtil" />
              <node concept="2ShNRf" id="51adnVzXqjl" role="37wK5m">
                <node concept="1pGfFk" id="51adnVzXqjm" role="2ShVmc">
                  <ref role="37wK5l" to="mte5:~ModulesScope.&lt;init&gt;(java.lang.Iterable)" resolve="ModulesScope" />
                  <node concept="37vLTw" id="51adnVzXqjn" role="37wK5m">
                    <ref role="3cqZAo" node="5$zfhXziDHj" resolve="theirModules" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="51adnVzXqjo" role="37wK5m">
                <node concept="2OqwBi" id="51adnVzXqjp" role="2Oq$k0">
                  <node concept="2WthIp" id="51adnVzXqjq" role="2Oq$k0" />
                  <node concept="1DTwFV" id="51adnVzXqjr" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzi2u9" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="51adnVzXqjs" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getScope():jetbrains.mps.project.Project$ProjectScope" resolve="getScope" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="51adnVzXu6C" role="3cqZAp">
          <node concept="3cpWsn" id="51adnVzXu6D" role="3cpWs9">
            <property role="TrG5h" value="depProj" />
            <node concept="2hMVRd" id="51adnVzXu6E" role="1tU5fm">
              <node concept="3Tqbb2" id="51adnVzXu6F" role="2hN53Y" />
            </node>
            <node concept="2YIFZM" id="51adnVzXu6G" role="33vP2m">
              <ref role="37wK5l" to="auc7:51adnVzUmYa" resolve="usagesOfDeprecated" />
              <ref role="1Pybhc" to="auc7:51adnVzUm$d" resolve="DeprecatedUtil" />
              <node concept="2OqwBi" id="51adnVzXvQL" role="37wK5m">
                <node concept="2OqwBi" id="51adnVzXvQM" role="2Oq$k0">
                  <node concept="2WthIp" id="51adnVzXvQN" role="2Oq$k0" />
                  <node concept="1DTwFV" id="51adnVzXvQO" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzi2u9" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="51adnVzXvQP" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getScope():jetbrains.mps.project.Project$ProjectScope" resolve="getScope" />
                </node>
              </node>
              <node concept="2OqwBi" id="51adnVzXu6K" role="37wK5m">
                <node concept="2OqwBi" id="51adnVzXu6L" role="2Oq$k0">
                  <node concept="2WthIp" id="51adnVzXu6M" role="2Oq$k0" />
                  <node concept="1DTwFV" id="51adnVzXu6N" role="2OqNvi">
                    <ref role="2WH_rO" node="5$zfhXzi2u9" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="51adnVzXu6O" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getScope():jetbrains.mps.project.Project$ProjectScope" resolve="getScope" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="51adnVzX49b" role="3cqZAp" />
        <node concept="3clFbF" id="5EyPfg3xGbc" role="3cqZAp">
          <node concept="2OqwBi" id="5$zfhXzt7xy" role="3clFbG">
            <node concept="2YIFZM" id="5$zfhXzt7xz" role="2Oq$k0">
              <ref role="37wK5l" to="bd8o:~ApplicationManager.getApplication():com.intellij.openapi.application.Application" resolve="getApplication" />
              <ref role="1Pybhc" to="bd8o:~ApplicationManager" resolve="ApplicationManager" />
            </node>
            <node concept="liA8E" id="5$zfhXzt7x$" role="2OqNvi">
              <ref role="37wK5l" to="bd8o:~Application.invokeLater(java.lang.Runnable,com.intellij.openapi.application.ModalityState):void" resolve="invokeLater" />
              <node concept="1bVj0M" id="5$zfhXzt7x_" role="37wK5m">
                <node concept="3clFbS" id="5$zfhXzt7xA" role="1bW5cS">
                  <node concept="1QHqEK" id="5$zfhXzt7xB" role="3cqZAp">
                    <node concept="1QHqEC" id="5$zfhXzt7xC" role="1QHqEI">
                      <node concept="3clFbS" id="5$zfhXzt7xD" role="1bW5cS">
                        <node concept="3clFbF" id="5$zfhXzsYvc" role="3cqZAp">
                          <node concept="2YIFZM" id="5$zfhXztCDx" role="3clFbG">
                            <ref role="37wK5l" to="t99v:5$zfhXztqeB" resolve="showNodes" />
                            <ref role="1Pybhc" to="t99v:5$zfhXzsScF" resolve="MigrationOutputUtil" />
                            <node concept="2OqwBi" id="5$zfhXztCDy" role="37wK5m">
                              <node concept="2WthIp" id="5$zfhXztCDz" role="2Oq$k0" />
                              <node concept="1DTwFV" id="5$zfhXztCD$" role="2OqNvi">
                                <ref role="2WH_rO" node="5$zfhXzt8TX" resolve="project" />
                              </node>
                            </node>
                            <node concept="1Ls8ON" id="51adnVzXre_" role="37wK5m">
                              <node concept="Xl_RD" id="5$zfhXztFwa" role="1Lso8e">
                                <property role="Xl_RC" value="Deprecated library stuff" />
                              </node>
                              <node concept="37vLTw" id="51adnVzXsF9" role="1Lso8e">
                                <ref role="3cqZAo" node="51adnVzXqjj" resolve="depLibs" />
                              </node>
                            </node>
                            <node concept="1Ls8ON" id="51adnVzXwJE" role="37wK5m">
                              <node concept="Xl_RD" id="51adnVzXxms" role="1Lso8e">
                                <property role="Xl_RC" value="Deprecated project stuff" />
                              </node>
                              <node concept="37vLTw" id="51adnVzXxId" role="1Lso8e">
                                <ref role="3cqZAo" node="51adnVzXu6D" resolve="depProj" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="10M0yZ" id="5$zfhXzt7xE" role="37wK5m">
                <ref role="3cqZAo" to="bd8o:~ModalityState.NON_MODAL" resolve="NON_MODAL" />
                <ref role="1PxDUh" to="bd8o:~ModalityState" resolve="ModalityState" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1DS2jV" id="5$zfhXzi2u9" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="5$zfhXzi2ua" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="5$zfhXzt8TX" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="5$zfhXzt8TY" role="1oa70y" />
    </node>
  </node>
  <node concept="sE7Ow" id="6MUcJMtBJJe">
    <property role="TrG5h" value="ExecuteMigrationAssistant" />
    <property role="2uzpH1" value="Run Migration Assistant" />
    <property role="72QZ$" value="true" />
    <property role="1teQrl" value="true" />
    <node concept="1DS2jV" id="2UTM4$6j1gg" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="7HZe2EwZDoU" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="7FLry2XR1vG" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="7FLry2XR1vH" role="1oa70y" />
    </node>
    <node concept="tnohg" id="6MUcJMtBJJf" role="tncku">
      <node concept="3clFbS" id="6MUcJMtBJJg" role="2VODD2">
        <node concept="3cpWs8" id="7lByCvU$QgK" role="3cqZAp">
          <node concept="3cpWsn" id="7lByCvU$QgL" role="3cpWs9">
            <property role="TrG5h" value="mt" />
            <node concept="3uibUv" id="7lByCvU$QgB" role="1tU5fm">
              <ref role="3uigEE" to="bim2:5SsFeroaajZ" resolve="MigrationTrigger" />
            </node>
            <node concept="1eOMI4" id="7lByCvU$QgM" role="33vP2m">
              <node concept="10QFUN" id="7lByCvU$QgN" role="1eOMHV">
                <node concept="2OqwBi" id="7lByCvU$QgO" role="10QFUP">
                  <node concept="2OqwBi" id="7lByCvU$QgP" role="2Oq$k0">
                    <node concept="2WthIp" id="7lByCvU$QgQ" role="2Oq$k0" />
                    <node concept="1DTwFV" id="7lByCvU$QgR" role="2OqNvi">
                      <ref role="2WH_rO" node="2UTM4$6j1gg" resolve="project" />
                    </node>
                  </node>
                  <node concept="liA8E" id="7lByCvU$QgS" role="2OqNvi">
                    <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                    <node concept="3VsKOn" id="7lByCvU$QgT" role="37wK5m">
                      <ref role="3VsUkX" to="bim2:4D3Y1hNxTJF" resolve="IStartupMigrationExecutor" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="7lByCvU$QgU" role="10QFUM">
                  <ref role="3uigEE" to="bim2:5SsFeroaajZ" resolve="MigrationTrigger" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7FLry2XR1h8" role="3cqZAp" />
        <node concept="3cpWs8" id="6KPfPAmPB01" role="3cqZAp">
          <node concept="3cpWsn" id="6KPfPAmPB02" role="3cpWs9">
            <property role="TrG5h" value="migrationRequired" />
            <node concept="10P_77" id="6KPfPAmPAZY" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbH" id="7FLry2XR4gs" role="3cqZAp" />
        <node concept="3clFbF" id="2fg9H9N2fAp" role="3cqZAp">
          <node concept="2OqwBi" id="2fg9H9N2gDv" role="3clFbG">
            <node concept="2OqwBi" id="2fg9H9N2gp5" role="2Oq$k0">
              <node concept="2OqwBi" id="2fg9H9N2fTu" role="2Oq$k0">
                <node concept="2OqwBi" id="7FLry2XR2Gy" role="2Oq$k0">
                  <node concept="2WthIp" id="7FLry2XR2G_" role="2Oq$k0" />
                  <node concept="1DTwFV" id="7FLry2XR2GB" role="2OqNvi">
                    <ref role="2WH_rO" node="7FLry2XR1vG" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="2fg9H9N2go2" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                </node>
              </node>
              <node concept="liA8E" id="2fg9H9N2gCu" role="2OqNvi">
                <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
              </node>
            </node>
            <node concept="liA8E" id="2fg9H9N2gWB" role="2OqNvi">
              <ref role="37wK5l" to="lui2:~ModelAccess.runWriteAction(java.lang.Runnable):void" resolve="runWriteAction" />
              <node concept="1bVj0M" id="2fg9H9N2gZs" role="37wK5m">
                <node concept="3clFbS" id="2fg9H9N2gZt" role="1bW5cS">
                  <node concept="3clFbF" id="1Ql1_c6N7pv" role="3cqZAp">
                    <node concept="2YIFZM" id="1Ql1_c6N7rw" role="3clFbG">
                      <ref role="1Pybhc" to="bim2:5SsFeroaajZ" resolve="MigrationTrigger" />
                      <ref role="37wK5l" to="bim2:1Ql1_c6N3AN" resolve="updateUsedLanguagesVersions" />
                      <node concept="2YIFZM" id="7FLry2XR4KL" role="37wK5m">
                        <ref role="1Pybhc" to="auc7:3UfGsecu96G" resolve="MigrationsUtil" />
                        <ref role="37wK5l" to="auc7:3UfGsecu96H" resolve="getMigrateableModulesFromProject" />
                        <node concept="2OqwBi" id="7FLry2XR4KM" role="37wK5m">
                          <node concept="2WthIp" id="7FLry2XR4KN" role="2Oq$k0" />
                          <node concept="1DTwFV" id="7FLry2XR4KO" role="2OqNvi">
                            <ref role="2WH_rO" node="7FLry2XR1vG" resolve="mpsProject" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="6KPfPAmPB9S" role="3cqZAp">
                    <node concept="37vLTI" id="6KPfPAmPB9U" role="3clFbG">
                      <node concept="2OqwBi" id="6KPfPAmPB03" role="37vLTx">
                        <node concept="2OqwBi" id="7FLry2XReY1" role="2Oq$k0">
                          <node concept="2OqwBi" id="7FLry2XR9uq" role="2Oq$k0">
                            <node concept="2WthIp" id="7FLry2XR9ut" role="2Oq$k0" />
                            <node concept="1DTwFV" id="7FLry2XR9uv" role="2OqNvi">
                              <ref role="2WH_rO" node="2UTM4$6j1gg" resolve="project" />
                            </node>
                          </node>
                          <node concept="liA8E" id="7FLry2XRfhO" role="2OqNvi">
                            <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                            <node concept="3VsKOn" id="7FLry2XRfBM" role="37wK5m">
                              <ref role="3VsUkX" to="bim2:5SsFeroaabl" resolve="MigrationManager" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="6KPfPAmPB05" role="2OqNvi">
                          <ref role="37wK5l" to="bim2:5SsFeroaac6" resolve="isMigrationRequired" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="6KPfPAmPB9Y" role="37vLTJ">
                        <ref role="3cqZAo" node="6KPfPAmPB02" resolve="migrationRequired" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2fg9H9N2hhz" role="3cqZAp">
          <node concept="3clFbS" id="2fg9H9N2hh_" role="3clFbx">
            <node concept="3clFbF" id="6CdT9mpBjNd" role="3cqZAp">
              <node concept="2YIFZM" id="6CdT9mpBkGy" role="3clFbG">
                <ref role="1Pybhc" to="bim2:6CdT9mpAIg7" resolve="MigrationDialogUtil" />
                <ref role="37wK5l" to="bim2:6CdT9mpBjP3" resolve="showNoMigrationMessage" />
                <node concept="2OqwBi" id="7FLry2XR3jA" role="37wK5m">
                  <node concept="2WthIp" id="7FLry2XR3jD" role="2Oq$k0" />
                  <node concept="1DTwFV" id="7FLry2XR3jF" role="2OqNvi">
                    <ref role="2WH_rO" node="2UTM4$6j1gg" resolve="project" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7FLry2XRg86" role="3cqZAp">
              <node concept="2OqwBi" id="7FLry2XS5Ir" role="3clFbG">
                <node concept="37vLTw" id="7FLry2XS5Ek" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lByCvU$QgL" resolve="mt" />
                </node>
                <node concept="liA8E" id="7FLry2XS6g2" role="2OqNvi">
                  <ref role="37wK5l" to="bim2:7FLry2XRgwU" resolve="resetMigrationQueuedFlag" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3fqX7Q" id="2fg9H9N2hlv" role="3clFbw">
            <node concept="37vLTw" id="6KPfPAmPB06" role="3fr31v">
              <ref role="3cqZAo" node="6KPfPAmPB02" resolve="migrationRequired" />
            </node>
          </node>
          <node concept="9aQIb" id="7FLry2XR35a" role="9aQIa">
            <node concept="3clFbS" id="7FLry2XR35b" role="9aQI4">
              <node concept="3clFbF" id="7lByCvU$Xrq" role="3cqZAp">
                <node concept="2OqwBi" id="7lByCvU$XCD" role="3clFbG">
                  <node concept="37vLTw" id="7lByCvU$Xro" role="2Oq$k0">
                    <ref role="3cqZAo" node="7lByCvU$QgL" resolve="mt" />
                  </node>
                  <node concept="liA8E" id="7lByCvU_3f2" role="2OqNvi">
                    <ref role="37wK5l" to="bim2:1AzqgyAh5MG" resolve="postponeMigration" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="sE7Ow" id="51adnVzU34y">
    <property role="TrG5h" value="ShowDeprecatedStuff" />
    <property role="2uzpH1" value="Show Deprecated Stuff in Project" />
    <node concept="1DS2jV" id="51adnVzXyUQ" role="1NuT2Z">
      <property role="TrG5h" value="mpsProject" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="51adnVzXyUR" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="51adnVzXyUS" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="51adnVzXyUT" role="1oa70y" />
    </node>
    <node concept="tnohg" id="51adnVzU34z" role="tncku">
      <node concept="3clFbS" id="51adnVzU34$" role="2VODD2">
        <node concept="3cpWs8" id="51adnVzXy8J" role="3cqZAp">
          <node concept="3cpWsn" id="51adnVzXy8K" role="3cpWs9">
            <property role="TrG5h" value="dep" />
            <node concept="2hMVRd" id="51adnVzXy8L" role="1tU5fm">
              <node concept="3Tqbb2" id="51adnVzXy8M" role="2hN53Y" />
            </node>
            <node concept="2YIFZM" id="51adnVzX_35" role="33vP2m">
              <ref role="37wK5l" to="auc7:51adnVzVq2Z" resolve="deprecated" />
              <ref role="1Pybhc" to="auc7:51adnVzUm$d" resolve="DeprecatedUtil" />
              <node concept="2OqwBi" id="51adnVzX_39" role="37wK5m">
                <node concept="2OqwBi" id="51adnVzX_3a" role="2Oq$k0">
                  <node concept="2WthIp" id="51adnVzX_3b" role="2Oq$k0" />
                  <node concept="1DTwFV" id="51adnVzX_3c" role="2OqNvi">
                    <ref role="2WH_rO" node="51adnVzXyUQ" resolve="mpsProject" />
                  </node>
                </node>
                <node concept="liA8E" id="51adnVzX_3d" role="2OqNvi">
                  <ref role="37wK5l" to="z1c3:~Project.getScope():jetbrains.mps.project.Project$ProjectScope" resolve="getScope" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="51adnVzXy9c" role="3cqZAp">
          <node concept="2OqwBi" id="51adnVzXy9d" role="3clFbG">
            <node concept="2YIFZM" id="51adnVzXy9e" role="2Oq$k0">
              <ref role="37wK5l" to="bd8o:~ApplicationManager.getApplication():com.intellij.openapi.application.Application" resolve="getApplication" />
              <ref role="1Pybhc" to="bd8o:~ApplicationManager" resolve="ApplicationManager" />
            </node>
            <node concept="liA8E" id="51adnVzXy9f" role="2OqNvi">
              <ref role="37wK5l" to="bd8o:~Application.invokeLater(java.lang.Runnable,com.intellij.openapi.application.ModalityState):void" resolve="invokeLater" />
              <node concept="1bVj0M" id="51adnVzXy9g" role="37wK5m">
                <node concept="3clFbS" id="51adnVzXy9h" role="1bW5cS">
                  <node concept="1QHqEK" id="51adnVzXy9i" role="3cqZAp">
                    <node concept="1QHqEC" id="51adnVzXy9j" role="1QHqEI">
                      <node concept="3clFbS" id="51adnVzXy9k" role="1bW5cS">
                        <node concept="3clFbF" id="51adnVzXy9l" role="3cqZAp">
                          <node concept="2YIFZM" id="51adnVzXy9m" role="3clFbG">
                            <ref role="1Pybhc" to="t99v:5$zfhXzsScF" resolve="MigrationOutputUtil" />
                            <ref role="37wK5l" to="t99v:5$zfhXztqeB" resolve="showNodes" />
                            <node concept="2OqwBi" id="51adnVzXy9n" role="37wK5m">
                              <node concept="2WthIp" id="51adnVzXy9o" role="2Oq$k0" />
                              <node concept="1DTwFV" id="51adnVzXy9p" role="2OqNvi">
                                <ref role="2WH_rO" node="51adnVzXyUS" resolve="project" />
                              </node>
                            </node>
                            <node concept="1Ls8ON" id="51adnVzXy9q" role="37wK5m">
                              <node concept="Xl_RD" id="51adnVzXy9r" role="1Lso8e">
                                <property role="Xl_RC" value="Deprecated stuff in project" />
                              </node>
                              <node concept="37vLTw" id="51adnVzXy9s" role="1Lso8e">
                                <ref role="3cqZAo" node="51adnVzXy8K" resolve="dep" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="10M0yZ" id="51adnVzXy9w" role="37wK5m">
                <ref role="1PxDUh" to="bd8o:~ModalityState" resolve="ModalityState" />
                <ref role="3cqZAo" to="bd8o:~ModalityState.NON_MODAL" resolve="NON_MODAL" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="tC5Ba" id="7R6nBIbwjvd">
    <property role="3GE5qa" value="migrationList" />
    <property role="TrG5h" value="ProjectMigrations" />
    <property role="1XlLyE" value="true" />
    <property role="2f7twF" value="Project Migrations" />
    <node concept="tT9cl" id="7R6nBIbwq8V" role="2f5YQi">
      <ref role="tU$_T" node="5$zfhXzqWk4" resolve="Migrations" />
      <ref role="2f8Tey" node="7R6nBIbwq8B" resolve="projectMigrations" />
    </node>
    <node concept="2OiAzN" id="7R6nBIbwjvh" role="ftER_">
      <node concept="2OiTZ2" id="7R6nBIbwjvj" role="2Oj6PV">
        <node concept="3clFbS" id="7R6nBIbwjvl" role="2VODD2">
          <node concept="2Gpval" id="5$zfhXzr2Xd" role="3cqZAp">
            <node concept="2GrKxI" id="5$zfhXzr2Xf" role="2Gsz3X">
              <property role="TrG5h" value="pm" />
            </node>
            <node concept="3clFbS" id="5$zfhXzr2Xh" role="2LFqv$">
              <node concept="2JFkCU" id="5$zfhXzrxKJ" role="3cqZAp">
                <node concept="tCFHf" id="5$zfhXzr_pQ" role="2JFLmv">
                  <ref role="tCJdB" node="5$zfhXzrxUf" resolve="RunProjectMigration" />
                  <node concept="2GrUjf" id="5$zfhXzr_q0" role="2J__8u">
                    <ref role="2Gs0qQ" node="5$zfhXzr2Xf" resolve="pm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="25gV4LsuSK$" role="2GsD0m">
              <node concept="2YIFZM" id="25gV4LsxqOk" role="2Oq$k0">
                <ref role="37wK5l" to="bdll:~ProjectMigrationsRegistry.getInstance():jetbrains.mps.migration.global.ProjectMigrationsRegistry" resolve="getInstance" />
                <ref role="1Pybhc" to="bdll:~ProjectMigrationsRegistry" resolve="ProjectMigrationsRegistry" />
              </node>
              <node concept="liA8E" id="25gV4LsuSKA" role="2OqNvi">
                <ref role="37wK5l" to="bdll:~ProjectMigrationsRegistry.getMigrations():java.util.List" resolve="getMigrations" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="tC5Ba" id="7R6nBIbwknP">
    <property role="3GE5qa" value="migrationList" />
    <property role="TrG5h" value="LanguageMigrations" />
    <property role="1XlLyE" value="true" />
    <property role="2f7twF" value="Language Migrations" />
    <node concept="tT9cl" id="7R6nBIbwqbg" role="2f5YQi">
      <ref role="tU$_T" node="5$zfhXzqWk4" resolve="Migrations" />
      <ref role="2f8Tey" node="7R6nBIbwq8J" resolve="languageMigrations" />
    </node>
    <node concept="2OiAzN" id="7R6nBIbwknQ" role="ftER_">
      <node concept="2OiTZ2" id="7R6nBIbwknR" role="2Oj6PV">
        <node concept="3clFbS" id="7R6nBIbwknS" role="2VODD2">
          <node concept="3cpWs8" id="5$zfhXzrXR3" role="3cqZAp">
            <node concept="3cpWsn" id="5$zfhXzrXR4" role="3cpWs9">
              <property role="TrG5h" value="project" />
              <node concept="3uibUv" id="4zPpOEulQfy" role="1tU5fm">
                <ref role="3uigEE" to="4nm9:~Project" resolve="Project" />
              </node>
              <node concept="2OqwBi" id="5$zfhXzrXR5" role="33vP2m">
                <node concept="tl45R" id="5$zfhXzrXR6" role="2Oq$k0" />
                <node concept="liA8E" id="5$zfhXzrXR7" role="2OqNvi">
                  <ref role="37wK5l" to="qkt:~AnActionEvent.getData(com.intellij.openapi.actionSystem.DataKey):java.lang.Object" resolve="getData" />
                  <node concept="10M0yZ" id="5$zfhXzrXR8" role="37wK5m">
                    <ref role="3cqZAo" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
                    <ref role="1PxDUh" to="qq03:~MPSCommonDataKeys" resolve="MPSCommonDataKeys" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="4voYo4rvD1i" role="3cqZAp">
            <node concept="3clFbS" id="4voYo4rvD1k" role="3clFbx">
              <node concept="3cpWs6" id="4voYo4rvE0F" role="3cqZAp" />
            </node>
            <node concept="3clFbC" id="4voYo4rvDC9" role="3clFbw">
              <node concept="10Nm6u" id="4voYo4rvDU8" role="3uHU7w" />
              <node concept="37vLTw" id="4voYo4rvDkS" role="3uHU7B">
                <ref role="3cqZAo" node="5$zfhXzrXR4" resolve="project" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4zPpOEulQHT" role="3cqZAp">
            <node concept="3cpWsn" id="4zPpOEulQHU" role="3cpWs9">
              <property role="TrG5h" value="mpsProject" />
              <node concept="3uibUv" id="4yRsQKnxjPR" role="1tU5fm">
                <ref role="3uigEE" to="z1c3:~Project" resolve="Project" />
              </node>
              <node concept="2OqwBi" id="4zPpOEulQHW" role="33vP2m">
                <node concept="tl45R" id="4zPpOEulQHX" role="2Oq$k0" />
                <node concept="liA8E" id="4zPpOEulQHY" role="2OqNvi">
                  <ref role="37wK5l" to="qkt:~AnActionEvent.getData(com.intellij.openapi.actionSystem.DataKey):java.lang.Object" resolve="getData" />
                  <node concept="10M0yZ" id="4zPpOEulQHZ" role="37wK5m">
                    <ref role="3cqZAo" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
                    <ref role="1PxDUh" to="qq03:~MPSCommonDataKeys" resolve="MPSCommonDataKeys" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="4voYo4rvEns" role="3cqZAp">
            <node concept="3clFbS" id="4voYo4rvEnu" role="3clFbx">
              <node concept="3cpWs6" id="4voYo4rvFjE" role="3cqZAp" />
            </node>
            <node concept="3clFbC" id="4voYo4rvF70" role="3clFbw">
              <node concept="10Nm6u" id="4voYo4rvFd7" role="3uHU7w" />
              <node concept="37vLTw" id="4voYo4rvEO3" role="3uHU7B">
                <ref role="3cqZAo" node="4zPpOEulQHU" resolve="mpsProject" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="5$zfhXzrTuL" role="3cqZAp">
            <node concept="3cpWsn" id="5$zfhXzrTuM" role="3cpWs9">
              <property role="TrG5h" value="migrationComponent" />
              <node concept="3uibUv" id="68FOACLsjSW" role="1tU5fm">
                <ref role="3uigEE" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
              </node>
              <node concept="2OqwBi" id="5$zfhXzrTuQ" role="33vP2m">
                <node concept="37vLTw" id="4zPpOEulQsu" role="2Oq$k0">
                  <ref role="3cqZAo" node="5$zfhXzrXR4" resolve="project" />
                </node>
                <node concept="liA8E" id="5$zfhXzrTuU" role="2OqNvi">
                  <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                  <node concept="3VsKOn" id="5$zfhXzrTuV" role="37wK5m">
                    <ref role="3VsUkX" to="bim2:tdUHv2l0Sg" resolve="MigrationComponent" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="4voYo4rvFRH" role="3cqZAp">
            <node concept="3clFbS" id="4voYo4rvFRJ" role="3clFbx">
              <node concept="3cpWs6" id="4voYo4rvGKC" role="3cqZAp" />
            </node>
            <node concept="3clFbC" id="4voYo4rvGu2" role="3clFbw">
              <node concept="10Nm6u" id="4voYo4rvGE5" role="3uHU7w" />
              <node concept="37vLTw" id="4voYo4rvGkB" role="3uHU7B">
                <ref role="3cqZAo" node="5$zfhXzrTuM" resolve="migrationComponent" />
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="4voYo4rvGLH" role="3cqZAp" />
          <node concept="3cpWs8" id="5$zfhXzrXqb" role="3cqZAp">
            <node concept="3cpWsn" id="5$zfhXzrXqc" role="3cpWs9">
              <property role="TrG5h" value="languages" />
              <node concept="_YKpA" id="68FOACLsfOA" role="1tU5fm">
                <node concept="3uibUv" id="68FOACLsfOC" role="_ZDj9">
                  <ref role="3uigEE" to="c17a:~SLanguage" resolve="SLanguage" />
                </node>
              </node>
              <node concept="2OqwBi" id="68FOACLsd0$" role="33vP2m">
                <node concept="2OqwBi" id="13xmEyWv3d1" role="2Oq$k0">
                  <node concept="2OqwBi" id="68FOACLscrg" role="2Oq$k0">
                    <node concept="2OqwBi" id="68FOACLsa2V" role="2Oq$k0">
                      <node concept="2YIFZM" id="68FOACLs4D5" role="2Oq$k0">
                        <ref role="37wK5l" to="auc7:3UfGsecu96H" resolve="getMigrateableModulesFromProject" />
                        <ref role="1Pybhc" to="auc7:3UfGsecu96G" resolve="MigrationsUtil" />
                        <node concept="37vLTw" id="68FOACLs4D_" role="37wK5m">
                          <ref role="3cqZAo" node="4zPpOEulQHU" resolve="mpsProject" />
                        </node>
                      </node>
                      <node concept="3goQfb" id="68FOACLsarr" role="2OqNvi">
                        <node concept="1bVj0M" id="68FOACLsart" role="23t8la">
                          <node concept="3clFbS" id="68FOACLsaru" role="1bW5cS">
                            <node concept="3clFbF" id="68FOACLsb2t" role="3cqZAp">
                              <node concept="2OqwBi" id="5$zfhXzrXqf" role="3clFbG">
                                <node concept="2ShNRf" id="5$zfhXzrXqg" role="2Oq$k0">
                                  <node concept="1pGfFk" id="5$zfhXzrXqh" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.&lt;init&gt;(jetbrains.mps.smodel.language.LanguageRegistry,java.util.Collection)" resolve="SLanguageHierarchy" />
                                    <node concept="2YIFZM" id="oze2ctd6Vo" role="37wK5m">
                                      <ref role="1Pybhc" to="vndm:~LanguageRegistry" resolve="LanguageRegistry" />
                                      <ref role="37wK5l" to="vndm:~LanguageRegistry.getInstance(org.jetbrains.mps.openapi.module.SRepository):jetbrains.mps.smodel.language.LanguageRegistry" resolve="getInstance" />
                                      <node concept="2OqwBi" id="oze2ctd8rK" role="37wK5m">
                                        <node concept="37vLTw" id="oze2ctd8cz" role="2Oq$k0">
                                          <ref role="3cqZAo" node="4zPpOEulQHU" resolve="mpsProject" />
                                        </node>
                                        <node concept="liA8E" id="oze2ctd8HA" role="2OqNvi">
                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="2OqwBi" id="5$zfhXzrXqi" role="37wK5m">
                                      <node concept="37vLTw" id="68FOACLsc2M" role="2Oq$k0">
                                        <ref role="3cqZAo" node="68FOACLsarv" resolve="module" />
                                      </node>
                                      <node concept="liA8E" id="5$zfhXzrXqk" role="2OqNvi">
                                        <ref role="37wK5l" to="lui2:~SModule.getUsedLanguages():java.util.Set" resolve="getUsedLanguages" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="liA8E" id="5$zfhXzrXql" role="2OqNvi">
                                  <ref role="37wK5l" to="w1kc:~SLanguageHierarchy.getExtended():java.util.Set" resolve="getExtended" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="68FOACLsarv" role="1bW2Oz">
                            <property role="TrG5h" value="module" />
                            <node concept="2jxLKc" id="68FOACLsarw" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1VAtEI" id="68FOACLscOx" role="2OqNvi" />
                  </node>
                  <node concept="2S7cBI" id="13xmEyWv3yS" role="2OqNvi">
                    <node concept="1bVj0M" id="13xmEyWv3yU" role="23t8la">
                      <node concept="3clFbS" id="13xmEyWv3yV" role="1bW5cS">
                        <node concept="3clFbF" id="13xmEyWv3S6" role="3cqZAp">
                          <node concept="2OqwBi" id="13xmEyWv49n" role="3clFbG">
                            <node concept="37vLTw" id="13xmEyWv3S5" role="2Oq$k0">
                              <ref role="3cqZAo" node="13xmEyWv3yW" resolve="it" />
                            </node>
                            <node concept="liA8E" id="13xmEyWv4st" role="2OqNvi">
                              <ref role="37wK5l" to="c17a:~SLanguage.getQualifiedName():java.lang.String" resolve="getQualifiedName" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="13xmEyWv3yW" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="13xmEyWv3yX" role="1tU5fm" />
                      </node>
                    </node>
                    <node concept="1nlBCl" id="13xmEyWv3yY" role="2S7zOq">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="68FOACLsdwu" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="oze2ctc4GQ" role="3cqZAp" />
          <node concept="1DcWWT" id="oze2ctc56k" role="3cqZAp">
            <node concept="37vLTw" id="oze2ctc5Hn" role="1DdaDG">
              <ref role="3cqZAo" node="5$zfhXzrXqc" resolve="languages" />
            </node>
            <node concept="3cpWsn" id="oze2ctc56U" role="1Duv9x">
              <property role="3TUv4t" value="false" />
              <property role="TrG5h" value="language" />
              <node concept="3uibUv" id="oze2ctc56W" role="1tU5fm">
                <ref role="3uigEE" to="c17a:~SLanguage" resolve="SLanguage" />
              </node>
            </node>
            <node concept="3clFbS" id="oze2ctc56m" role="2LFqv$">
              <node concept="3cpWs8" id="oze2ctc56o" role="3cqZAp">
                <node concept="3cpWsn" id="oze2ctc56n" role="3cpWs9">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="name" />
                  <node concept="3uibUv" id="oze2ctc56p" role="1tU5fm">
                    <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                  </node>
                  <node concept="2OqwBi" id="oze2ctc56Z" role="33vP2m">
                    <node concept="37vLTw" id="oze2ctc56Y" role="2Oq$k0">
                      <ref role="3cqZAo" node="oze2ctc56U" resolve="language" />
                    </node>
                    <node concept="liA8E" id="oze2ctc570" role="2OqNvi">
                      <ref role="37wK5l" to="c17a:~SLanguage.getQualifiedName():java.lang.String" resolve="getQualifiedName" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="oze2ctc56x" role="3cqZAp">
                <node concept="3cpWsn" id="oze2ctc56w" role="3cpWs9">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="langRootsGroup" />
                  <node concept="3uibUv" id="oze2ctc56y" role="1tU5fm">
                    <ref role="3uigEE" to="qkt:~DefaultActionGroup" resolve="DefaultActionGroup" />
                  </node>
                  <node concept="2ShNRf" id="oze2ctc572" role="33vP2m">
                    <node concept="1pGfFk" id="oze2ctc57n" role="2ShVmc">
                      <ref role="37wK5l" to="qkt:~DefaultActionGroup.&lt;init&gt;(java.lang.String,boolean)" resolve="DefaultActionGroup" />
                      <node concept="2YIFZM" id="oze2ctc57o" role="37wK5m">
                        <ref role="1Pybhc" to="18ew:~NameUtil" resolve="NameUtil" />
                        <ref role="37wK5l" to="18ew:~NameUtil.compactNamespace(java.lang.String):java.lang.String" resolve="compactNamespace" />
                        <node concept="37vLTw" id="oze2ctc56_" role="37wK5m">
                          <ref role="3cqZAo" node="oze2ctc56n" resolve="name" />
                        </node>
                      </node>
                      <node concept="3clFbT" id="oze2ctc56A" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="oze2ctc9$f" role="3cqZAp" />
              <node concept="1Dw8fO" id="oze2ctc9sm" role="3cqZAp">
                <node concept="3cpWsn" id="oze2ctc9sn" role="1Duv9x">
                  <property role="TrG5h" value="ver" />
                  <node concept="10Oyi0" id="oze2ctc9so" role="1tU5fm" />
                  <node concept="3cmrfG" id="oze2ctc9sp" role="33vP2m">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
                <node concept="3clFbS" id="oze2ctc9sq" role="2LFqv$">
                  <node concept="3cpWs8" id="oze2ctc9sr" role="3cqZAp">
                    <node concept="3cpWsn" id="oze2ctc9ss" role="3cpWs9">
                      <property role="TrG5h" value="script" />
                      <node concept="3uibUv" id="oze2ctc9st" role="1tU5fm">
                        <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
                      </node>
                      <node concept="2OqwBi" id="oze2ctc9su" role="33vP2m">
                        <node concept="37vLTw" id="oze2ctc9sw" role="2Oq$k0">
                          <ref role="3cqZAo" node="5$zfhXzrTuM" resolve="migrationComponent" />
                        </node>
                        <node concept="liA8E" id="oze2ctc9sy" role="2OqNvi">
                          <ref role="37wK5l" to="bim2:5TtkZMYUq8y" resolve="fetchMigrationScript" />
                          <node concept="2ShNRf" id="oze2ctc9sz" role="37wK5m">
                            <node concept="1pGfFk" id="oze2ctc9s$" role="2ShVmc">
                              <ref role="37wK5l" to="6f4m:1HyHl70Zxpa" resolve="MigrationScriptReference" />
                              <node concept="37vLTw" id="oze2ctc9KQ" role="37wK5m">
                                <ref role="3cqZAo" node="oze2ctc56U" resolve="language" />
                              </node>
                              <node concept="37vLTw" id="oze2ctc9sA" role="37wK5m">
                                <ref role="3cqZAo" node="oze2ctc9sn" resolve="ver" />
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbT" id="oze2ctc9sB" role="37wK5m">
                            <property role="3clFbU" value="true" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbJ" id="oze2ctc9sC" role="3cqZAp">
                    <node concept="3clFbS" id="oze2ctc9sD" role="3clFbx">
                      <node concept="3N13vt" id="oze2ctc9sE" role="3cqZAp" />
                    </node>
                    <node concept="3clFbC" id="oze2ctc9sF" role="3clFbw">
                      <node concept="37vLTw" id="oze2ctc9sG" role="3uHU7B">
                        <ref role="3cqZAo" node="oze2ctc9ss" resolve="script" />
                      </node>
                      <node concept="10Nm6u" id="oze2ctc9sH" role="3uHU7w" />
                    </node>
                  </node>
                  <node concept="3clFbH" id="oze2ctcacl" role="3cqZAp" />
                  <node concept="3clFbF" id="oze2ctc9RL" role="3cqZAp">
                    <node concept="2OqwBi" id="oze2ctc9V8" role="3clFbG">
                      <node concept="37vLTw" id="oze2ctc9RJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="oze2ctc56w" resolve="langRootsGroup" />
                      </node>
                      <node concept="liA8E" id="oze2ctca0N" role="2OqNvi">
                        <ref role="37wK5l" to="qkt:~DefaultActionGroup.add(com.intellij.openapi.actionSystem.AnAction):void" resolve="add" />
                        <node concept="2ShNRf" id="oze2ctd521" role="37wK5m">
                          <node concept="1pGfFk" id="oze2ctd6aE" role="2ShVmc">
                            <ref role="37wK5l" node="oze2ctcRgd" resolve="RunMigration" />
                            <node concept="37vLTw" id="oze2ctd6bf" role="37wK5m">
                              <ref role="3cqZAo" node="oze2ctc9ss" resolve="script" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3eOVzh" id="oze2ctc9sL" role="1Dwp0S">
                  <node concept="2OqwBi" id="oze2ctc9sM" role="3uHU7w">
                    <node concept="liA8E" id="oze2ctc9sO" role="2OqNvi">
                      <ref role="37wK5l" to="c17a:~SLanguage.getLanguageVersion():int" resolve="getLanguageVersion" />
                    </node>
                    <node concept="37vLTw" id="oze2ctc9F9" role="2Oq$k0">
                      <ref role="3cqZAo" node="oze2ctc56U" resolve="language" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="oze2ctc9sP" role="3uHU7B">
                    <ref role="3cqZAo" node="oze2ctc9sn" resolve="ver" />
                  </node>
                </node>
                <node concept="3uNrnE" id="oze2ctc9sQ" role="1Dwrff">
                  <node concept="37vLTw" id="oze2ctc9sR" role="2$L3a6">
                    <ref role="3cqZAo" node="oze2ctc9sn" resolve="ver" />
                  </node>
                </node>
              </node>
              <node concept="3clFbJ" id="oze2ctc56L" role="3cqZAp">
                <node concept="3eOSWO" id="oze2ctc56M" role="3clFbw">
                  <node concept="2OqwBi" id="oze2ctc57t" role="3uHU7B">
                    <node concept="37vLTw" id="oze2ctc57s" role="2Oq$k0">
                      <ref role="3cqZAo" node="oze2ctc56w" resolve="langRootsGroup" />
                    </node>
                    <node concept="liA8E" id="oze2ctc57u" role="2OqNvi">
                      <ref role="37wK5l" to="qkt:~DefaultActionGroup.getChildrenCount():int" resolve="getChildrenCount" />
                    </node>
                  </node>
                  <node concept="3cmrfG" id="oze2ctc56O" role="3uHU7w">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
                <node concept="3clFbS" id="oze2ctc56Q" role="3clFbx">
                  <node concept="3clFbF" id="oze2ctc8Pa" role="3cqZAp">
                    <node concept="2OqwBi" id="oze2ctc8R9" role="3clFbG">
                      <node concept="2WthIp" id="oze2ctc8P8" role="2Oq$k0" />
                      <node concept="liA8E" id="oze2ctc8XD" role="2OqNvi">
                        <ref role="37wK5l" to="qkt:~DefaultActionGroup.add(com.intellij.openapi.actionSystem.AnAction):void" resolve="add" />
                        <node concept="37vLTw" id="oze2ctc8Y9" role="37wK5m">
                          <ref role="3cqZAo" node="oze2ctc56w" resolve="langRootsGroup" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2JRCWQ" id="4voYo4rvlmK">
    <property role="TrG5h" value="NonDumbAware" />
    <node concept="2JRCWP" id="4voYo4rvlGM" role="2JRCWR">
      <ref role="2JRCWa" node="51adnVzU34y" resolve="ShowDeprecatedStuff" />
    </node>
    <node concept="2JRCWP" id="4voYo4rvlGR" role="2JRCWR">
      <ref role="2JRCWa" node="5$zfhXzhGml" resolve="ShowDeprecatedUsages" />
    </node>
    <node concept="2JRCWP" id="4voYo4rvlGZ" role="2JRCWR">
      <ref role="2JRCWa" node="5$zfhXzhIiu" resolve="RunPreUpdateCheck" />
    </node>
    <node concept="2JRCWP" id="4voYo4rvlH9" role="2JRCWR">
      <ref role="2JRCWa" node="5$zfhXzhLy1" resolve="ExecuteRerunnableMigrations" />
    </node>
    <node concept="2JRCWP" id="4voYo4rvlHz" role="2JRCWR">
      <ref role="2JRCWa" node="5$zfhXzrxUf" resolve="RunProjectMigration" />
    </node>
  </node>
  <node concept="312cEu" id="oze2ctcP$4">
    <property role="3GE5qa" value="migrationList" />
    <property role="TrG5h" value="RunMigration" />
    <node concept="3Tm1VV" id="oze2ctcP$5" role="1B3o_S" />
    <node concept="3uibUv" id="oze2ctcQG9" role="1zkMxy">
      <ref role="3uigEE" to="7bx7:~BaseAction" resolve="BaseAction" />
    </node>
    <node concept="312cEg" id="oze2ctcRg9" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myScript" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="oze2ctcRgb" role="1tU5fm">
        <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
      </node>
      <node concept="3Tm6S6" id="oze2ctcRgc" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="oze2ctcZod" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myProject" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="7osCUTviT8O" role="1tU5fm">
        <ref role="3uigEE" to="z1c4:~MPSProject" resolve="MPSProject" />
      </node>
      <node concept="3Tm6S6" id="oze2ctcZof" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="oze2ctcRgd" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="oze2ctcRge" role="3clF45" />
      <node concept="37vLTG" id="oze2ctcRgf" role="3clF46">
        <property role="TrG5h" value="script" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="oze2ctcRgg" role="1tU5fm">
          <ref role="3uigEE" to="6f4m:2RG318eVG1Z" resolve="MigrationScript" />
        </node>
      </node>
      <node concept="3clFbS" id="oze2ctcRgh" role="3clF47">
        <node concept="3clFbF" id="oze2ctcRgi" role="3cqZAp">
          <node concept="37vLTI" id="oze2ctcRgj" role="3clFbG">
            <node concept="2OqwBi" id="oze2ctcRgk" role="37vLTJ">
              <node concept="Xjq3P" id="oze2ctcRgl" role="2Oq$k0" />
              <node concept="2OwXpG" id="oze2ctcRgm" role="2OqNvi">
                <ref role="2Oxat5" node="oze2ctcRg9" resolve="myScript" />
              </node>
            </node>
            <node concept="37vLTw" id="oze2ctcRgn" role="37vLTx">
              <ref role="3cqZAo" node="oze2ctcRgf" resolve="script" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7osCUTvjkRy" role="3cqZAp">
          <node concept="2OqwBi" id="7osCUTvjkTw" role="3clFbG">
            <node concept="Xjq3P" id="7osCUTvjkRw" role="2Oq$k0" />
            <node concept="liA8E" id="7osCUTvjkXQ" role="2OqNvi">
              <ref role="37wK5l" to="7bx7:~BaseAction.setExecuteOutsideCommand(boolean):void" resolve="setExecuteOutsideCommand" />
              <node concept="3clFbT" id="7osCUTvjl2j" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oze2ctd3QU" role="3cqZAp">
          <node concept="2OqwBi" id="oze2ctd3S9" role="3clFbG">
            <node concept="1rXfSq" id="oze2ctd3QS" role="2Oq$k0">
              <ref role="37wK5l" to="qkt:~AnAction.getTemplatePresentation():com.intellij.openapi.actionSystem.Presentation" resolve="getTemplatePresentation" />
            </node>
            <node concept="liA8E" id="oze2ctd3Z2" role="2OqNvi">
              <ref role="37wK5l" to="qkt:~Presentation.setText(java.lang.String):void" resolve="setText" />
              <node concept="2OqwBi" id="oze2ctd49l" role="37wK5m">
                <node concept="37vLTw" id="oze2ctd45s" role="2Oq$k0">
                  <ref role="3cqZAo" node="oze2ctcRg9" resolve="myScript" />
                </node>
                <node concept="liA8E" id="oze2ctd4cR" role="2OqNvi">
                  <ref role="37wK5l" to="6f4m:2RG318eVG20" resolve="getCaption" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="oze2ctcRgA" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="oze2ctcRh4" role="jymVt">
      <property role="TrG5h" value="collectActionData" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="2AHcQZ" id="oze2ctcRh5" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="37vLTG" id="oze2ctcRh6" role="3clF46">
        <property role="TrG5h" value="event" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="oze2ctcRh7" role="1tU5fm">
          <ref role="3uigEE" to="qkt:~AnActionEvent" resolve="AnActionEvent" />
        </node>
      </node>
      <node concept="37vLTG" id="oze2ctcRh8" role="3clF46">
        <property role="TrG5h" value="_params" />
        <property role="3TUv4t" value="true" />
        <node concept="3uibUv" id="oze2ctcRh9" role="1tU5fm">
          <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
          <node concept="3uibUv" id="oze2ctcRha" role="11_B2D">
            <ref role="3uigEE" to="wyt6:~String" resolve="String" />
          </node>
          <node concept="3uibUv" id="oze2ctcRhb" role="11_B2D">
            <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="oze2ctcRhc" role="3clF47">
        <node concept="3clFbJ" id="oze2ctcRhd" role="3cqZAp">
          <node concept="3fqX7Q" id="oze2ctcRhe" role="3clFbw">
            <node concept="1eOMI4" id="oze2ctcRhi" role="3fr31v">
              <node concept="3nyPlj" id="oze2ctcRhf" role="1eOMHV">
                <ref role="37wK5l" to="7bx7:~BaseAction.collectActionData(com.intellij.openapi.actionSystem.AnActionEvent,java.util.Map):boolean" resolve="collectActionData" />
                <node concept="37vLTw" id="oze2ctcRhg" role="37wK5m">
                  <ref role="3cqZAo" node="oze2ctcRh6" resolve="event" />
                </node>
                <node concept="37vLTw" id="oze2ctcRhh" role="37wK5m">
                  <ref role="3cqZAo" node="oze2ctcRh8" resolve="_params" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="oze2ctcRhk" role="3clFbx">
            <node concept="3cpWs6" id="oze2ctcRhl" role="3cqZAp">
              <node concept="3clFbT" id="oze2ctcRhm" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oze2ctd1dm" role="3cqZAp">
          <node concept="37vLTI" id="oze2ctd1Uj" role="3clFbG">
            <node concept="37vLTw" id="oze2ctd1dk" role="37vLTJ">
              <ref role="3cqZAo" node="oze2ctcZod" resolve="myProject" />
            </node>
            <node concept="2OqwBi" id="oze2ctcRjv" role="37vLTx">
              <node concept="37vLTw" id="oze2ctcRju" role="2Oq$k0">
                <ref role="3cqZAo" node="oze2ctcRh6" resolve="event" />
              </node>
              <node concept="liA8E" id="oze2ctcRjw" role="2OqNvi">
                <ref role="37wK5l" to="qkt:~AnActionEvent.getData(com.intellij.openapi.actionSystem.DataKey):java.lang.Object" resolve="getData" />
                <node concept="10M0yZ" id="oze2ctcRvV" role="37wK5m">
                  <ref role="1PxDUh" to="qq03:~MPSCommonDataKeys" resolve="MPSCommonDataKeys" />
                  <ref role="3cqZAo" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="oze2ctcRhA" role="3cqZAp">
          <node concept="3y3z36" id="oze2ctd4A_" role="3cqZAk">
            <node concept="10Nm6u" id="oze2ctd4Ly" role="3uHU7w" />
            <node concept="37vLTw" id="oze2ctd4qG" role="3uHU7B">
              <ref role="3cqZAo" node="oze2ctcZod" resolve="myProject" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tmbuc" id="oze2ctcRhC" role="1B3o_S" />
      <node concept="10P_77" id="oze2ctcRhD" role="3clF45" />
    </node>
    <node concept="3clFb_" id="oze2ctcRhE" role="jymVt">
      <property role="TrG5h" value="doExecute" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="2AHcQZ" id="oze2ctcRhF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="37vLTG" id="oze2ctcRhG" role="3clF46">
        <property role="TrG5h" value="event" />
        <property role="3TUv4t" value="true" />
        <node concept="2AHcQZ" id="oze2ctcRhH" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
        <node concept="3uibUv" id="oze2ctcRhI" role="1tU5fm">
          <ref role="3uigEE" to="qkt:~AnActionEvent" resolve="AnActionEvent" />
        </node>
      </node>
      <node concept="37vLTG" id="oze2ctcRhJ" role="3clF46">
        <property role="TrG5h" value="_params" />
        <property role="3TUv4t" value="true" />
        <node concept="3uibUv" id="oze2ctcRhK" role="1tU5fm">
          <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
          <node concept="3uibUv" id="oze2ctcRhL" role="11_B2D">
            <ref role="3uigEE" to="wyt6:~String" resolve="String" />
          </node>
          <node concept="3uibUv" id="oze2ctcRhM" role="11_B2D">
            <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="oze2ctcRhN" role="3clF47">
        <node concept="3cpWs8" id="7osCUTvj4d8" role="3cqZAp">
          <node concept="3cpWsn" id="7osCUTvj4d9" role="3cpWs9">
            <property role="TrG5h" value="modules" />
            <property role="3TUv4t" value="true" />
            <node concept="10Q1$e" id="7osCUTvjZqL" role="1tU5fm">
              <node concept="3uibUv" id="7osCUTvk2bd" role="10Q1$1">
                <ref role="3uigEE" to="33ny:~List" resolve="List" />
                <node concept="3uibUv" id="7osCUTvk2qF" role="11_B2D">
                  <ref role="3uigEE" to="lui2:~SModule" resolve="SModule" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="7osCUTvjZR0" role="33vP2m">
              <node concept="3$_iS1" id="7osCUTvjZQX" role="2ShVmc">
                <node concept="3$GHV9" id="7osCUTvk03E" role="3$GQph">
                  <node concept="3cmrfG" id="7osCUTvk0jz" role="3$I4v7">
                    <property role="3cmrfH" value="1" />
                  </node>
                </node>
                <node concept="3uibUv" id="7osCUTvk1Mt" role="3$_nBY">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1QHqEK" id="7osCUTvjm3i" role="3cqZAp">
          <node concept="1QHqEC" id="7osCUTvjm3k" role="1QHqEI">
            <node concept="3clFbS" id="7osCUTvjm3m" role="1bW5cS">
              <node concept="3clFbF" id="7osCUTvjmAu" role="3cqZAp">
                <node concept="37vLTI" id="7osCUTvjmAw" role="3clFbG">
                  <node concept="2OqwBi" id="1wrYbVZLaw" role="37vLTx">
                    <node concept="2YIFZM" id="1wrYbVZK7E" role="2Oq$k0">
                      <ref role="37wK5l" to="auc7:3UfGsecu96H" resolve="getMigrateableModulesFromProject" />
                      <ref role="1Pybhc" to="auc7:3UfGsecu96G" resolve="MigrationsUtil" />
                      <node concept="37vLTw" id="1wrYbVZL4E" role="37wK5m">
                        <ref role="3cqZAo" node="oze2ctcZod" resolve="myProject" />
                      </node>
                    </node>
                    <node concept="ANE8D" id="1wrYbVZLiY" role="2OqNvi" />
                  </node>
                  <node concept="AH0OO" id="7osCUTvk2I1" role="37vLTJ">
                    <node concept="3cmrfG" id="7osCUTvk2O7" role="AHEQo">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="37vLTw" id="7osCUTvjmA$" role="AHHXb">
                      <ref role="3cqZAo" node="7osCUTvj4d9" resolve="modules" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7osCUTvjmme" role="ukAjM">
            <node concept="37vLTw" id="7osCUTvjmiA" role="2Oq$k0">
              <ref role="3cqZAo" node="oze2ctcZod" resolve="myProject" />
            </node>
            <node concept="liA8E" id="7osCUTvjm$y" role="2OqNvi">
              <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1OUHNoJWXME" role="3cqZAp">
          <node concept="2OqwBi" id="1OUHNoJWXMF" role="3clFbG">
            <node concept="2YIFZM" id="1OUHNoJWXMG" role="2Oq$k0">
              <ref role="37wK5l" to="xygl:~ProgressManager.getInstance():com.intellij.openapi.progress.ProgressManager" resolve="getInstance" />
              <ref role="1Pybhc" to="xygl:~ProgressManager" resolve="ProgressManager" />
            </node>
            <node concept="liA8E" id="1OUHNoJWXMH" role="2OqNvi">
              <ref role="37wK5l" to="xygl:~ProgressManager.run(com.intellij.openapi.progress.Task):void" resolve="run" />
              <node concept="2ShNRf" id="1OUHNoJWXMI" role="37wK5m">
                <node concept="YeOm9" id="1OUHNoJWXMJ" role="2ShVmc">
                  <node concept="1Y3b0j" id="1OUHNoJWXMK" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="xygl:~Task$Modal" resolve="Task.Modal" />
                    <ref role="37wK5l" to="xygl:~Task$Modal.&lt;init&gt;(com.intellij.openapi.project.Project,java.lang.String,boolean)" resolve="Task.Modal" />
                    <node concept="3Tm1VV" id="1OUHNoJWXML" role="1B3o_S" />
                    <node concept="3clFb_" id="1OUHNoJWXMM" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="run" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="1OUHNoJWXMN" role="1B3o_S" />
                      <node concept="3cqZAl" id="1OUHNoJWXMO" role="3clF45" />
                      <node concept="37vLTG" id="1OUHNoJWXMP" role="3clF46">
                        <property role="TrG5h" value="progressIndicator" />
                        <node concept="3uibUv" id="1OUHNoJWXMQ" role="1tU5fm">
                          <ref role="3uigEE" to="xygl:~ProgressIndicator" resolve="ProgressIndicator" />
                        </node>
                        <node concept="2AHcQZ" id="1OUHNoJWXMR" role="2AJF6D">
                          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="1OUHNoJWXMS" role="3clF47">
                        <node concept="3cpWs8" id="1OUHNoJWXMT" role="3cqZAp">
                          <node concept="3cpWsn" id="1OUHNoJWXMU" role="3cpWs9">
                            <property role="TrG5h" value="progressMonitor" />
                            <node concept="3uibUv" id="1OUHNoJWXMV" role="1tU5fm">
                              <ref role="3uigEE" to="mk8z:~ProgressMonitorAdapter" resolve="ProgressMonitorAdapter" />
                            </node>
                            <node concept="2ShNRf" id="1OUHNoJWXMW" role="33vP2m">
                              <node concept="1pGfFk" id="1OUHNoJWXMX" role="2ShVmc">
                                <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.&lt;init&gt;(com.intellij.openapi.progress.ProgressIndicator)" resolve="ProgressMonitorAdapter" />
                                <node concept="37vLTw" id="1OUHNoJWXMY" role="37wK5m">
                                  <ref role="3cqZAo" node="1OUHNoJWXMP" resolve="progressIndicator" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cpWs8" id="1OUHNoJWXN2" role="3cqZAp">
                          <node concept="3cpWsn" id="1OUHNoJWXN3" role="3cpWs9">
                            <property role="TrG5h" value="steps" />
                            <node concept="2OqwBi" id="7osCUTvj5tF" role="33vP2m">
                              <node concept="AH0OO" id="7osCUTvk3f1" role="2Oq$k0">
                                <node concept="3cmrfG" id="7osCUTvk3vr" role="AHEQo">
                                  <property role="3cmrfH" value="0" />
                                </node>
                                <node concept="37vLTw" id="7osCUTvj4X8" role="AHHXb">
                                  <ref role="3cqZAo" node="7osCUTvj4d9" resolve="modules" />
                                </node>
                              </node>
                              <node concept="liA8E" id="7osCUTvk4_P" role="2OqNvi">
                                <ref role="37wK5l" to="33ny:~List.size():int" resolve="size" />
                              </node>
                            </node>
                            <node concept="10Oyi0" id="1OUHNoJWXN4" role="1tU5fm" />
                          </node>
                        </node>
                        <node concept="3clFbF" id="1OUHNoJWXNv" role="3cqZAp">
                          <node concept="2OqwBi" id="1OUHNoJWXNw" role="3clFbG">
                            <node concept="37vLTw" id="2vOGnLEns91" role="2Oq$k0">
                              <ref role="3cqZAo" node="1OUHNoJWXMU" resolve="progressMonitor" />
                            </node>
                            <node concept="liA8E" id="1OUHNoJWXNy" role="2OqNvi">
                              <ref role="37wK5l" to="mk90:~ProgressMonitorBase.start(java.lang.String,int):void" resolve="start" />
                              <node concept="2OqwBi" id="2QLRRyfDkaG" role="37wK5m">
                                <node concept="37vLTw" id="2QLRRyfDjWf" role="2Oq$k0">
                                  <ref role="3cqZAo" node="oze2ctcRg9" resolve="myScript" />
                                </node>
                                <node concept="liA8E" id="2QLRRyfDkeP" role="2OqNvi">
                                  <ref role="37wK5l" to="6f4m:2RG318eVG20" resolve="getCaption" />
                                </node>
                              </node>
                              <node concept="37vLTw" id="1OUHNoJWXN$" role="37wK5m">
                                <ref role="3cqZAo" node="1OUHNoJWXN3" resolve="steps" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="2Gpval" id="7osCUTvjjv0" role="3cqZAp">
                          <node concept="2GrKxI" id="7osCUTvjjv2" role="2Gsz3X">
                            <property role="TrG5h" value="module" />
                          </node>
                          <node concept="3clFbS" id="7osCUTvjjv4" role="2LFqv$">
                            <node concept="3clFbF" id="2QLRRyfDnF8" role="3cqZAp">
                              <node concept="2OqwBi" id="2QLRRyfDnZg" role="3clFbG">
                                <node concept="37vLTw" id="2QLRRyfDnF6" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1OUHNoJWXMU" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="2QLRRyfDo4h" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.step(java.lang.String):void" resolve="step" />
                                  <node concept="2OqwBi" id="2QLRRyfDoOM" role="37wK5m">
                                    <node concept="2GrUjf" id="2QLRRyfDoAB" role="2Oq$k0">
                                      <ref role="2Gs0qQ" node="7osCUTvjjv2" resolve="module" />
                                    </node>
                                    <node concept="liA8E" id="2QLRRyfDoVZ" role="2OqNvi">
                                      <ref role="37wK5l" to="lui2:~SModule.getModuleName():java.lang.String" resolve="getModuleName" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7osCUTvjcTN" role="3cqZAp">
                              <node concept="2YIFZM" id="7osCUTvjeFS" role="3clFbG">
                                <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
                                <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
                                <node concept="1bVj0M" id="7osCUTvjfhK" role="37wK5m">
                                  <node concept="3clFbS" id="7osCUTvjfhL" role="1bW5cS">
                                    <node concept="1QHqEO" id="7osCUTvjfIS" role="3cqZAp">
                                      <node concept="1QHqEC" id="7osCUTvjfIT" role="1QHqEI">
                                        <node concept="3clFbS" id="7osCUTvjfIU" role="1bW5cS">
                                          <node concept="3clFbF" id="7osCUTvjymj" role="3cqZAp">
                                            <node concept="2OqwBi" id="7osCUTvj$aG" role="3clFbG">
                                              <node concept="37vLTw" id="7osCUTvjymi" role="2Oq$k0">
                                                <ref role="3cqZAo" node="oze2ctcRg9" resolve="myScript" />
                                              </node>
                                              <node concept="liA8E" id="7osCUTvj$f6" role="2OqNvi">
                                                <ref role="37wK5l" to="6f4m:5UnT0284V4S" resolve="execute" />
                                                <node concept="2GrUjf" id="7osCUTvj$FT" role="37wK5m">
                                                  <ref role="2Gs0qQ" node="7osCUTvjjv2" resolve="module" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="7osCUTvjhMT" role="ukAjM">
                                        <node concept="2OqwBi" id="7osCUTvjgbw" role="2Oq$k0">
                                          <node concept="Xjq3P" id="7osCUTvjhA9" role="2Oq$k0">
                                            <ref role="1HBi2w" node="oze2ctcP$4" resolve="RunMigration" />
                                          </node>
                                          <node concept="2OwXpG" id="7osCUTvjhHz" role="2OqNvi">
                                            <ref role="2Oxat5" node="oze2ctcZod" resolve="myProject" />
                                          </node>
                                        </node>
                                        <node concept="liA8E" id="7osCUTvjhXV" role="2OqNvi">
                                          <ref role="37wK5l" to="z1c3:~Project.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7osCUTvjBRJ" role="3cqZAp">
                              <node concept="2OqwBi" id="7osCUTvjCjn" role="3clFbG">
                                <node concept="37vLTw" id="7osCUTvjBRH" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1OUHNoJWXMU" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="7osCUTvjCoo" role="2OqNvi">
                                  <ref role="37wK5l" to="mk90:~ProgressMonitorBase.advance(int):void" resolve="advance" />
                                  <node concept="3cmrfG" id="7osCUTvjCO6" role="37wK5m">
                                    <property role="3cmrfH" value="1" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbJ" id="1OUHNoJWXNF" role="3cqZAp">
                              <node concept="3clFbS" id="1OUHNoJWXNG" role="3clFbx">
                                <node concept="3zACq4" id="1OUHNoJWXNL" role="3cqZAp" />
                              </node>
                              <node concept="2OqwBi" id="1OUHNoJWXNN" role="3clFbw">
                                <node concept="37vLTw" id="1OUHNoJWXNO" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1OUHNoJWXMU" resolve="progressMonitor" />
                                </node>
                                <node concept="liA8E" id="1OUHNoJWXNP" role="2OqNvi">
                                  <ref role="37wK5l" to="mk8z:~ProgressMonitorAdapter.isCanceled():boolean" resolve="isCanceled" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="AH0OO" id="7osCUTvk3Ms" role="2GsD0m">
                            <node concept="3cmrfG" id="7osCUTvk451" role="AHEQo">
                              <property role="3cmrfH" value="0" />
                            </node>
                            <node concept="37vLTw" id="7osCUTvjkwh" role="AHHXb">
                              <ref role="3cqZAo" node="7osCUTvj4d9" resolve="modules" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="7osCUTviUGa" role="37wK5m">
                      <node concept="37vLTw" id="7osCUTviPws" role="2Oq$k0">
                        <ref role="3cqZAo" node="oze2ctcZod" resolve="myProject" />
                      </node>
                      <node concept="liA8E" id="7osCUTviVbp" role="2OqNvi">
                        <ref role="37wK5l" to="z1c4:~MPSProject.getProject():com.intellij.openapi.project.Project" resolve="getProject" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="1OUHNoJWXOn" role="37wK5m">
                      <property role="Xl_RC" value="Run Migration" />
                    </node>
                    <node concept="3clFbT" id="1OUHNoJWXOo" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="oze2ctcRiC" role="1B3o_S" />
      <node concept="3cqZAl" id="oze2ctcRiD" role="3clF45" />
    </node>
  </node>
  <node concept="tC5Ba" id="4H96pIbL85m">
    <property role="TrG5h" value="MigrationUtils" />
    <property role="1XlLyE" value="true" />
    <property role="2f7twF" value="Utils" />
    <node concept="tT9cl" id="4H96pIbL85o" role="2f5YQi">
      <ref role="tU$_T" node="5$zfhXzhGmb" resolve="MigrationsMenu" />
      <ref role="2f8Tey" node="4H96pIbL85e" resolve="utils" />
    </node>
    <node concept="ftmFs" id="4H96pIbL85J" role="ftER_">
      <node concept="tCFHf" id="4H96pIbL85M" role="ftvYc">
        <ref role="tCJdB" node="4H96pIbL85q" resolve="FindMetaCreationCode" />
      </node>
    </node>
  </node>
  <node concept="sE7Ow" id="4H96pIbL85q">
    <property role="TrG5h" value="FindMetaCreationCode" />
    <property role="2uzpH1" value="Find Code that Creates Language Structures" />
    <node concept="2XrIbr" id="4H96pIbM6iD" role="32lrUH">
      <property role="TrG5h" value="convertNodes" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="5$zfhXztqeE" role="3clF47">
        <node concept="3cpWs8" id="1udyd_Tt_Gc" role="3cqZAp">
          <node concept="3cpWsn" id="1udyd_Tt_Gd" role="3cpWs9">
            <property role="TrG5h" value="sr" />
            <node concept="3uibUv" id="1udyd_Tt_Ge" role="1tU5fm">
              <ref role="3uigEE" to="g4jo:J2bOg02HbG" resolve="SearchResults" />
            </node>
            <node concept="2ShNRf" id="1udyd_TtAk8" role="33vP2m">
              <node concept="1pGfFk" id="1udyd_TtAk6" role="2ShVmc">
                <ref role="37wK5l" to="g4jo:J2bOg02HbW" resolve="SearchResults" />
                <node concept="3Tqbb2" id="1udyd_TtAk7" role="1pMfVU" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1udyd_TtEm6" role="3cqZAp">
          <node concept="2OqwBi" id="1udyd_TtFDe" role="3clFbG">
            <node concept="2OqwBi" id="51adnVzXbDz" role="2Oq$k0">
              <node concept="37vLTw" id="4H96pIbMmIw" role="2Oq$k0">
                <ref role="3cqZAo" node="4H96pIbM7tz" resolve="nodes" />
              </node>
              <node concept="3$u5V9" id="51adnVzXcCf" role="2OqNvi">
                <node concept="1bVj0M" id="51adnVzXcCh" role="23t8la">
                  <node concept="3clFbS" id="51adnVzXcCi" role="1bW5cS">
                    <node concept="3clFbF" id="51adnVzXcL$" role="3cqZAp">
                      <node concept="2ShNRf" id="5$zfhXztBbU" role="3clFbG">
                        <node concept="1pGfFk" id="5$zfhXztBbV" role="2ShVmc">
                          <ref role="37wK5l" to="g4jo:J2bOg02GIO" resolve="SearchResult" />
                          <node concept="37vLTw" id="1udyd_TtFbl" role="37wK5m">
                            <ref role="3cqZAo" node="51adnVzXcCj" resolve="node" />
                          </node>
                          <node concept="37vLTw" id="51adnVzXdSb" role="37wK5m">
                            <ref role="3cqZAo" node="51adnVzXcCj" resolve="node" />
                          </node>
                          <node concept="Xl_RD" id="4H96pIbMnq1" role="37wK5m">
                            <property role="Xl_RC" value="Usages" />
                          </node>
                          <node concept="3uibUv" id="1udyd_TtEI0" role="1pMfVU">
                            <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="51adnVzXcCj" role="1bW2Oz">
                    <property role="TrG5h" value="node" />
                    <node concept="2jxLKc" id="51adnVzXcCk" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2es0OD" id="1udyd_TtGhE" role="2OqNvi">
              <node concept="1bVj0M" id="1udyd_TtGhG" role="23t8la">
                <node concept="3clFbS" id="1udyd_TtGhH" role="1bW5cS">
                  <node concept="3clFbF" id="1udyd_TtGnI" role="3cqZAp">
                    <node concept="2OqwBi" id="1udyd_TtGE0" role="3clFbG">
                      <node concept="37vLTw" id="1udyd_TtGnH" role="2Oq$k0">
                        <ref role="3cqZAo" node="1udyd_Tt_Gd" resolve="sr" />
                      </node>
                      <node concept="liA8E" id="1udyd_TtGUJ" role="2OqNvi">
                        <ref role="37wK5l" to="g4jo:73Fgxppsozx" resolve="add" />
                        <node concept="37vLTw" id="1udyd_TtHcS" role="37wK5m">
                          <ref role="3cqZAo" node="1udyd_TtGhI" resolve="it" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="1udyd_TtGhI" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="1udyd_TtGhJ" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4H96pIbMolB" role="3cqZAp">
          <node concept="37vLTw" id="4H96pIbMomR" role="3cqZAk">
            <ref role="3cqZAo" node="1udyd_Tt_Gd" resolve="sr" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="4H96pIbM7It" role="3clF45">
        <ref role="3uigEE" to="g4jo:J2bOg02HbG" resolve="SearchResults" />
      </node>
      <node concept="3Tm1VV" id="5$zfhXztqeD" role="1B3o_S" />
      <node concept="37vLTG" id="4H96pIbM7tz" role="3clF46">
        <property role="TrG5h" value="nodes" />
        <node concept="2hMVRd" id="4H96pIbM7tx" role="1tU5fm">
          <node concept="3Tqbb2" id="4H96pIbM7y0" role="2hN53Y" />
        </node>
      </node>
    </node>
    <node concept="1DS2jV" id="4H96pIbLiFv" role="1NuT2Z">
      <property role="TrG5h" value="project" />
      <ref role="1DUlNI" to="qq03:~MPSCommonDataKeys.MPS_PROJECT" resolve="MPS_PROJECT" />
      <node concept="1oajcY" id="4H96pIbLiFw" role="1oa70y" />
    </node>
    <node concept="1DS2jV" id="4H96pIbM4ca" role="1NuT2Z">
      <property role="TrG5h" value="ideaProject" />
      <ref role="1DUlNI" to="qkt:~CommonDataKeys.PROJECT" resolve="PROJECT" />
      <node concept="1oajcY" id="4H96pIbM4cb" role="1oa70y" />
    </node>
    <node concept="tnohg" id="4H96pIbL85r" role="tncku">
      <node concept="3clFbS" id="4H96pIbL85s" role="2VODD2">
        <node concept="3cpWs8" id="4H96pIbM8fm" role="3cqZAp">
          <node concept="3cpWsn" id="4H96pIbM8fp" role="3cpWs9">
            <property role="TrG5h" value="nodes" />
            <node concept="2hMVRd" id="4H96pIbM8fi" role="1tU5fm">
              <node concept="3Tqbb2" id="4H96pIbM8qR" role="2hN53Y" />
            </node>
            <node concept="2ShNRf" id="4H96pIbM8XY" role="33vP2m">
              <node concept="2i4dXS" id="4H96pIbM8vs" role="2ShVmc">
                <node concept="3Tqbb2" id="4H96pIbM8vt" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="L3pyB" id="4H96pIbLiJ5" role="3cqZAp">
          <node concept="3clFbS" id="4H96pIbLiJ6" role="L3pyw">
            <node concept="3clFbF" id="4H96pIbMdZb" role="3cqZAp">
              <node concept="2OqwBi" id="4H96pIbMeu2" role="3clFbG">
                <node concept="37vLTw" id="4H96pIbMdZ9" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="4H96pIbMfPf" role="2OqNvi">
                  <node concept="2OqwBi" id="4H96pIbLj6Q" role="25WWJ7">
                    <node concept="2OqwBi" id="4H96pIbLm$X" role="2Oq$k0">
                      <node concept="2OqwBi" id="4H96pIbLjLL" role="2Oq$k0">
                        <node concept="qVDSY" id="4H96pIbLj6R" role="2Oq$k0">
                          <node concept="chp4Y" id="4H96pIbLj6S" role="qVDSX">
                            <ref role="cht4Q" to="tpee:gEShNN5" resolve="GenericNewExpression" />
                          </node>
                          <node concept="1dO9Bo" id="4H96pIbLj6T" role="1dOa5D" />
                        </node>
                        <node concept="3$u5V9" id="4H96pIbLkW5" role="2OqNvi">
                          <node concept="1bVj0M" id="4H96pIbLkW7" role="23t8la">
                            <node concept="3clFbS" id="4H96pIbLkW8" role="1bW5cS">
                              <node concept="3clFbF" id="4H96pIbLlhH" role="3cqZAp">
                                <node concept="2OqwBi" id="4H96pIbLlyg" role="3clFbG">
                                  <node concept="37vLTw" id="4H96pIbLlhG" role="2Oq$k0">
                                    <ref role="3cqZAo" node="4H96pIbLkW9" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="4H96pIbLm7$" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tpee:gEShVi6" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Rh6nW" id="4H96pIbLkW9" role="1bW2Oz">
                              <property role="TrG5h" value="it" />
                              <node concept="2jxLKc" id="4H96pIbLkWa" role="1tU5fm" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="v3k3i" id="4H96pIbLni3" role="2OqNvi">
                        <node concept="chp4Y" id="4H96pIbLn$S" role="v3oSu">
                          <ref role="cht4Q" to="tp25:hbzrR4P" resolve="SNodeCreator" />
                        </node>
                      </node>
                    </node>
                    <node concept="3zZkjj" id="4H96pIbLj6U" role="2OqNvi">
                      <node concept="1bVj0M" id="4H96pIbLj6V" role="23t8la">
                        <node concept="3clFbS" id="4H96pIbLj6W" role="1bW5cS">
                          <node concept="3clFbF" id="4H96pIbLj6X" role="3cqZAp">
                            <node concept="2OqwBi" id="4H96pIbLp_Q" role="3clFbG">
                              <node concept="2OqwBi" id="4H96pIbLj71" role="2Oq$k0">
                                <node concept="2OqwBi" id="4H96pIbLj72" role="2Oq$k0">
                                  <node concept="37vLTw" id="4H96pIbLj75" role="2Oq$k0">
                                    <ref role="3cqZAo" node="4H96pIbLj7f" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="4H96pIbLj77" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tp25:hbzrR4S" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4H96pIbLj78" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp25:g$ehGDh" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="4H96pIbLskI" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="4H96pIbLs_W" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:h0PkWnZ" resolve="AbstractConceptDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4H96pIbLj7f" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4H96pIbLj7g" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUPWf9" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUPWfa" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUPWfb" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUPWfc" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUPWfd" role="25WWJ7">
                    <node concept="2OqwBi" id="1qtZSOUPWfe" role="2Oq$k0">
                      <node concept="2OqwBi" id="1qtZSOUPWff" role="2Oq$k0">
                        <node concept="qVDSY" id="1qtZSOUPWfg" role="2Oq$k0">
                          <node concept="chp4Y" id="1qtZSOUPWfh" role="qVDSX">
                            <ref role="cht4Q" to="tpee:gEShNN5" resolve="GenericNewExpression" />
                          </node>
                          <node concept="1dO9Bo" id="1qtZSOUPWfi" role="1dOa5D" />
                        </node>
                        <node concept="3$u5V9" id="1qtZSOUPWfj" role="2OqNvi">
                          <node concept="1bVj0M" id="1qtZSOUPWfk" role="23t8la">
                            <node concept="3clFbS" id="1qtZSOUPWfl" role="1bW5cS">
                              <node concept="3clFbF" id="1qtZSOUPWfm" role="3cqZAp">
                                <node concept="2OqwBi" id="1qtZSOUPWfn" role="3clFbG">
                                  <node concept="37vLTw" id="1qtZSOUPWfo" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1qtZSOUPWfq" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="1qtZSOUPWfp" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tpee:gEShVi6" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Rh6nW" id="1qtZSOUPWfq" role="1bW2Oz">
                              <property role="TrG5h" value="it" />
                              <node concept="2jxLKc" id="1qtZSOUPWfr" role="1tU5fm" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="v3k3i" id="1qtZSOUPWfs" role="2OqNvi">
                        <node concept="chp4Y" id="1qtZSOUPWft" role="v3oSu">
                          <ref role="cht4Q" to="tp25:hbzrR4P" resolve="SNodeCreator" />
                        </node>
                      </node>
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUPWfu" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUPWfv" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUPWfw" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUPWfx" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUPWfy" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUPWfz" role="2Oq$k0">
                                <node concept="2OqwBi" id="1qtZSOUPWf$" role="2Oq$k0">
                                  <node concept="37vLTw" id="1qtZSOUPWf_" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1qtZSOUPWfE" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="1qtZSOUPWfA" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tp25:hbzrR4S" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="1qtZSOUPWfB" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp25:g$ehGDh" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1qtZSOUPWfC" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="1qtZSOUPWfD" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:f_TJgxF" resolve="PropertyDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUPWfE" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUPWfF" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUPY46" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUPY47" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUPY48" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUPY49" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUPY4a" role="25WWJ7">
                    <node concept="2OqwBi" id="1qtZSOUPY4b" role="2Oq$k0">
                      <node concept="2OqwBi" id="1qtZSOUPY4c" role="2Oq$k0">
                        <node concept="qVDSY" id="1qtZSOUPY4d" role="2Oq$k0">
                          <node concept="chp4Y" id="1qtZSOUPY4e" role="qVDSX">
                            <ref role="cht4Q" to="tpee:gEShNN5" resolve="GenericNewExpression" />
                          </node>
                          <node concept="1dO9Bo" id="1qtZSOUPY4f" role="1dOa5D" />
                        </node>
                        <node concept="3$u5V9" id="1qtZSOUPY4g" role="2OqNvi">
                          <node concept="1bVj0M" id="1qtZSOUPY4h" role="23t8la">
                            <node concept="3clFbS" id="1qtZSOUPY4i" role="1bW5cS">
                              <node concept="3clFbF" id="1qtZSOUPY4j" role="3cqZAp">
                                <node concept="2OqwBi" id="1qtZSOUPY4k" role="3clFbG">
                                  <node concept="37vLTw" id="1qtZSOUPY4l" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1qtZSOUPY4n" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="1qtZSOUPY4m" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tpee:gEShVi6" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Rh6nW" id="1qtZSOUPY4n" role="1bW2Oz">
                              <property role="TrG5h" value="it" />
                              <node concept="2jxLKc" id="1qtZSOUPY4o" role="1tU5fm" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="v3k3i" id="1qtZSOUPY4p" role="2OqNvi">
                        <node concept="chp4Y" id="1qtZSOUPY4q" role="v3oSu">
                          <ref role="cht4Q" to="tp25:hbzrR4P" resolve="SNodeCreator" />
                        </node>
                      </node>
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUPY4r" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUPY4s" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUPY4t" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUPY4u" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUPY4v" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUPY4w" role="2Oq$k0">
                                <node concept="2OqwBi" id="1qtZSOUPY4x" role="2Oq$k0">
                                  <node concept="37vLTw" id="1qtZSOUPY4y" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1qtZSOUPY4B" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="1qtZSOUPY4z" role="2OqNvi">
                                    <ref role="3Tt5mk" to="tp25:hbzrR4S" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="1qtZSOUPY4$" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp25:g$ehGDh" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1qtZSOUPY4_" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="1qtZSOUPY4A" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:f_TJgxE" resolve="LinkDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUPY4B" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUPY4C" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4H96pIbMgbN" role="3cqZAp">
              <node concept="2OqwBi" id="4H96pIbMgbO" role="3clFbG">
                <node concept="37vLTw" id="4H96pIbMgbP" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="4H96pIbMgbQ" role="2OqNvi">
                  <node concept="2OqwBi" id="4H96pIbLthB" role="25WWJ7">
                    <node concept="qVDSY" id="4H96pIbLthC" role="2Oq$k0">
                      <node concept="chp4Y" id="1qtZSOUQ1Mw" role="qVDSX">
                        <ref role="cht4Q" to="tpce:h0PkWnZ" resolve="AbstractConceptDeclaration" />
                      </node>
                      <node concept="1dO9Bo" id="4H96pIbLthE" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="4H96pIbLthF" role="2OqNvi">
                      <node concept="1bVj0M" id="4H96pIbLthG" role="23t8la">
                        <node concept="3clFbS" id="4H96pIbLthH" role="1bW5cS">
                          <node concept="3clFbF" id="4H96pIbLthI" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUQ6MF" role="3clFbG">
                              <node concept="2OqwBi" id="4H96pIbLthK" role="2Oq$k0">
                                <node concept="37vLTw" id="4H96pIbLthL" role="2Oq$k0">
                                  <ref role="3cqZAo" node="4H96pIbLthP" resolve="it" />
                                </node>
                                <node concept="2Xjw5R" id="1qtZSOUQ5tH" role="2OqNvi">
                                  <node concept="1xMEDy" id="1qtZSOUQ5tJ" role="1xVPHs">
                                    <node concept="chp4Y" id="1qtZSOUQ6qX" role="ri$Ld">
                                      <ref role="cht4Q" to="tp3r:hqc44pp" resolve="Quotation" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3x8VRR" id="1qtZSOUQ7D6" role="2OqNvi" />
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4H96pIbLthP" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4H96pIbLthQ" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUQ86$" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUQ86_" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUQ86A" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUQ86B" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUQ86C" role="25WWJ7">
                    <node concept="qVDSY" id="1qtZSOUQ86D" role="2Oq$k0">
                      <node concept="chp4Y" id="1qtZSOUQ8Et" role="qVDSX">
                        <ref role="cht4Q" to="tpce:f_TJgxF" resolve="PropertyDeclaration" />
                      </node>
                      <node concept="1dO9Bo" id="1qtZSOUQ86F" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUQ86G" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUQ86H" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUQ86I" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUQ86J" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUQ86K" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUQ86L" role="2Oq$k0">
                                <node concept="37vLTw" id="1qtZSOUQ86M" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1qtZSOUQ86R" resolve="it" />
                                </node>
                                <node concept="2Xjw5R" id="1qtZSOUQ86N" role="2OqNvi">
                                  <node concept="1xMEDy" id="1qtZSOUQ86O" role="1xVPHs">
                                    <node concept="chp4Y" id="1qtZSOUQ86P" role="ri$Ld">
                                      <ref role="cht4Q" to="tp3r:hqc44pp" resolve="Quotation" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3x8VRR" id="1qtZSOUQ86Q" role="2OqNvi" />
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUQ86R" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUQ86S" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUQ8ab" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUQ8ac" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUQ8ad" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUQ8ae" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUQ8af" role="25WWJ7">
                    <node concept="qVDSY" id="1qtZSOUQ8ag" role="2Oq$k0">
                      <node concept="chp4Y" id="1qtZSOUQ9na" role="qVDSX">
                        <ref role="cht4Q" to="tpce:f_TJgxE" resolve="LinkDeclaration" />
                      </node>
                      <node concept="1dO9Bo" id="1qtZSOUQ8ai" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUQ8aj" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUQ8ak" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUQ8al" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUQ8am" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUQ8an" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUQ8ao" role="2Oq$k0">
                                <node concept="37vLTw" id="1qtZSOUQ8ap" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1qtZSOUQ8au" resolve="it" />
                                </node>
                                <node concept="2Xjw5R" id="1qtZSOUQ8aq" role="2OqNvi">
                                  <node concept="1xMEDy" id="1qtZSOUQ8ar" role="1xVPHs">
                                    <node concept="chp4Y" id="1qtZSOUQ8as" role="ri$Ld">
                                      <ref role="cht4Q" to="tp3r:hqc44pp" resolve="Quotation" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3x8VRR" id="1qtZSOUQ8at" role="2OqNvi" />
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUQ8au" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUQ8av" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4H96pIbMgdr" role="3cqZAp">
              <node concept="2OqwBi" id="4H96pIbMgds" role="3clFbG">
                <node concept="37vLTw" id="4H96pIbMgdt" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="4H96pIbMgdu" role="2OqNvi">
                  <node concept="2OqwBi" id="4H96pIbLtwx" role="25WWJ7">
                    <node concept="qVDSY" id="4H96pIbLtwy" role="2Oq$k0">
                      <node concept="chp4Y" id="4H96pIbLtGG" role="qVDSX">
                        <ref role="cht4Q" to="tp3r:4IP40Bi2$Ew" resolve="NodeBuilderNode" />
                      </node>
                      <node concept="1dO9Bo" id="4H96pIbLtw$" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="4H96pIbLtw_" role="2OqNvi">
                      <node concept="1bVj0M" id="4H96pIbLtwA" role="23t8la">
                        <node concept="3clFbS" id="4H96pIbLtwB" role="1bW5cS">
                          <node concept="3clFbF" id="4H96pIbLtwC" role="3cqZAp">
                            <node concept="2OqwBi" id="4H96pIbLu63" role="3clFbG">
                              <node concept="2OqwBi" id="4H96pIbLu64" role="2Oq$k0">
                                <node concept="37vLTw" id="4H96pIbLu66" role="2Oq$k0">
                                  <ref role="3cqZAo" node="4H96pIbLtwJ" resolve="it" />
                                </node>
                                <node concept="3TrEf2" id="4H96pIbLvHr" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp3r:4IP40Bi2KaL" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="4H96pIbLu69" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="4H96pIbLu6a" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:h0PkWnZ" resolve="AbstractConceptDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4H96pIbLtwJ" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4H96pIbLtwK" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUQa$K" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUQa$L" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUQa$M" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUQa$N" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUQa$O" role="25WWJ7">
                    <node concept="qVDSY" id="1qtZSOUQa$P" role="2Oq$k0">
                      <node concept="chp4Y" id="1qtZSOUQa$Q" role="qVDSX">
                        <ref role="cht4Q" to="tp3r:4IP40Bi2$Ew" resolve="NodeBuilderNode" />
                      </node>
                      <node concept="1dO9Bo" id="1qtZSOUQa$R" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUQa$S" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUQa$T" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUQa$U" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUQa$V" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUQa$W" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUQa$X" role="2Oq$k0">
                                <node concept="37vLTw" id="1qtZSOUQa$Y" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1qtZSOUQa_2" resolve="it" />
                                </node>
                                <node concept="3TrEf2" id="1qtZSOUQa$Z" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp3r:4IP40Bi2KaL" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1qtZSOUQa_0" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="1qtZSOUQa_1" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:f_TJgxF" resolve="PropertyDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUQa_2" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUQa_3" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1qtZSOUQbUa" role="3cqZAp">
              <node concept="2OqwBi" id="1qtZSOUQbUb" role="3clFbG">
                <node concept="37vLTw" id="1qtZSOUQbUc" role="2Oq$k0">
                  <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                </node>
                <node concept="X8dFx" id="1qtZSOUQbUd" role="2OqNvi">
                  <node concept="2OqwBi" id="1qtZSOUQbUe" role="25WWJ7">
                    <node concept="qVDSY" id="1qtZSOUQbUf" role="2Oq$k0">
                      <node concept="chp4Y" id="1qtZSOUQbUg" role="qVDSX">
                        <ref role="cht4Q" to="tp3r:4IP40Bi2$Ew" resolve="NodeBuilderNode" />
                      </node>
                      <node concept="1dO9Bo" id="1qtZSOUQbUh" role="1dOa5D" />
                    </node>
                    <node concept="3zZkjj" id="1qtZSOUQbUi" role="2OqNvi">
                      <node concept="1bVj0M" id="1qtZSOUQbUj" role="23t8la">
                        <node concept="3clFbS" id="1qtZSOUQbUk" role="1bW5cS">
                          <node concept="3clFbF" id="1qtZSOUQbUl" role="3cqZAp">
                            <node concept="2OqwBi" id="1qtZSOUQbUm" role="3clFbG">
                              <node concept="2OqwBi" id="1qtZSOUQbUn" role="2Oq$k0">
                                <node concept="37vLTw" id="1qtZSOUQbUo" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1qtZSOUQbUs" resolve="it" />
                                </node>
                                <node concept="3TrEf2" id="1qtZSOUQbUp" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tp3r:4IP40Bi2KaL" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1qtZSOUQbUq" role="2OqNvi">
                                <ref role="37wK5l" to="tpcn:73yVtVlWOga" resolve="isSubconceptOf" />
                                <node concept="3B5_sB" id="1qtZSOUQbUr" role="37wK5m">
                                  <ref role="3B5MYn" to="tpce:f_TJgxE" resolve="LinkDeclaration" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="1qtZSOUQbUs" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="1qtZSOUQbUt" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1eOMI4" id="4H96pIbLiYf" role="L3pyr">
            <node concept="10QFUN" id="4H96pIbLiYg" role="1eOMHV">
              <node concept="2OqwBi" id="4H96pIbLiYc" role="10QFUP">
                <node concept="2WthIp" id="4H96pIbLiYd" role="2Oq$k0" />
                <node concept="1DTwFV" id="4H96pIbLiYe" role="2OqNvi">
                  <ref role="2WH_rO" node="4H96pIbLiFv" resolve="project" />
                </node>
              </node>
              <node concept="3uibUv" id="4H96pIbLj5n" role="10QFUM">
                <ref role="3uigEE" to="z1c3:~Project" resolve="Project" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4H96pIbM3zz" role="3cqZAp">
          <node concept="2OqwBi" id="6M9lfhDGIIu" role="3clFbG">
            <node concept="2OqwBi" id="4H96pIbM4$U" role="2Oq$k0">
              <node concept="2OqwBi" id="4H96pIbM4jV" role="2Oq$k0">
                <node concept="2WthIp" id="4H96pIbM4jY" role="2Oq$k0" />
                <node concept="1DTwFV" id="4H96pIbM4k0" role="2OqNvi">
                  <ref role="2WH_rO" node="4H96pIbM4ca" resolve="ideaProject" />
                </node>
              </node>
              <node concept="liA8E" id="YwfKjlWKKQ" role="2OqNvi">
                <ref role="37wK5l" to="1m72:~ComponentManager.getComponent(java.lang.Class):java.lang.Object" resolve="getComponent" />
                <node concept="3VsKOn" id="YwfKjlWLmT" role="37wK5m">
                  <ref role="3VsUkX" to="ngmm:~UsagesViewTool" resolve="UsagesViewTool" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="YwfKjlX7TS" role="2OqNvi">
              <ref role="37wK5l" to="ngmm:~UsagesViewTool.show(jetbrains.mps.ide.findusages.model.SearchResults,java.lang.String):void" resolve="show" />
              <node concept="2OqwBi" id="4H96pIbMmgi" role="37wK5m">
                <node concept="2WthIp" id="4H96pIbMmgl" role="2Oq$k0" />
                <node concept="2XshWL" id="4H96pIbMmgn" role="2OqNvi">
                  <ref role="2WH_rO" node="4H96pIbM6iD" resolve="convertNodes" />
                  <node concept="37vLTw" id="4H96pIbMmmI" role="2XxRq1">
                    <ref role="3cqZAo" node="4H96pIbM8fp" resolve="nodes" />
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="6M9lfhDGpMO" role="37wK5m">
                <property role="Xl_RC" value="No results to show" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>


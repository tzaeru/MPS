package jetbrains.mps.samples.Expressions.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.ConceptPresentationAspectBase;
import jetbrains.mps.smodel.runtime.ConceptPresentation;
import jetbrains.mps.smodel.runtime.ConceptPresentationBuilder;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Map;
import java.util.HashMap;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public class ConceptPresentationAspectImpl extends ConceptPresentationAspectBase {
  private final ConceptPresentation props_AndSimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ArithmeticSimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_BinarySimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_LogicalSimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_NotSimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_OrSimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathAssignment = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathBooleanConstant = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathBooleanType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathElementType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathExpression = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathFloatConstant = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathFloatType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathIntegerConstant = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathIntegerType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathLongConstant = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathLongType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathNumberType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathType = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathTypedVarDeclaration = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathVarDeclaration = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathVarReference = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_SimpleMathWrapper = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_UnarySimpleMathExpression = new ConceptPresentationBuilder().create();

  @Override
  @Nullable
  public ConceptPresentation getDescriptor(SAbstractConcept c) {
    {
      SAbstractConcept cncpt = c;
      Integer preIndex = indices_lpa09p_a0z.get(cncpt);
      int switchIndex = (preIndex == null ? -1 : preIndex);
      switch (switchIndex) {
        case 0:
          if (true) {
            return props_AndSimpleMathExpression;
          }
          break;
        case 1:
          if (true) {
            return props_ArithmeticSimpleMathExpression;
          }
          break;
        case 2:
          if (true) {
            return props_BinarySimpleMathExpression;
          }
          break;
        case 3:
          if (true) {
            return props_LogicalSimpleMathExpression;
          }
          break;
        case 4:
          if (true) {
            return props_NotSimpleMathExpression;
          }
          break;
        case 5:
          if (true) {
            return props_OrSimpleMathExpression;
          }
          break;
        case 6:
          if (true) {
            return props_SimpleMathAssignment;
          }
          break;
        case 7:
          if (true) {
            return props_SimpleMathBooleanConstant;
          }
          break;
        case 8:
          if (true) {
            return props_SimpleMathBooleanType;
          }
          break;
        case 9:
          if (true) {
            return props_SimpleMathElementType;
          }
          break;
        case 10:
          if (true) {
            return props_SimpleMathExpression;
          }
          break;
        case 11:
          if (true) {
            return props_SimpleMathFloatConstant;
          }
          break;
        case 12:
          if (true) {
            return props_SimpleMathFloatType;
          }
          break;
        case 13:
          if (true) {
            return props_SimpleMathIntegerConstant;
          }
          break;
        case 14:
          if (true) {
            return props_SimpleMathIntegerType;
          }
          break;
        case 15:
          if (true) {
            return props_SimpleMathLongConstant;
          }
          break;
        case 16:
          if (true) {
            return props_SimpleMathLongType;
          }
          break;
        case 17:
          if (true) {
            return props_SimpleMathNumberType;
          }
          break;
        case 18:
          if (true) {
            return props_SimpleMathType;
          }
          break;
        case 19:
          if (true) {
            return props_SimpleMathTypedVarDeclaration;
          }
          break;
        case 20:
          if (true) {
            return props_SimpleMathVarDeclaration;
          }
          break;
        case 21:
          if (true) {
            return props_SimpleMathVarReference;
          }
          break;
        case 22:
          if (true) {
            return props_SimpleMathWrapper;
          }
          break;
        case 23:
          if (true) {
            return props_UnarySimpleMathExpression;
          }
          break;
        default:
      }
    }
    throw new IllegalStateException();
  }
  private static Map<SAbstractConcept, Integer> buildConceptIndices(SAbstractConcept... concepts) {
    HashMap<SAbstractConcept, Integer> res = new HashMap<SAbstractConcept, Integer>();
    int counter = 0;
    for (SAbstractConcept c : concepts) {
      res.put(c, counter++);
    }
    return res;
  }
  private static final Map<SAbstractConcept, Integer> indices_lpa09p_a0z = buildConceptIndices(MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b825cc49L, "jetbrains.mps.samples.Expressions.structure.AndSimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b8289497L, "jetbrains.mps.samples.Expressions.structure.ArithmeticSimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81c52ccL, "jetbrains.mps.samples.Expressions.structure.BinarySimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81dcba5L, "jetbrains.mps.samples.Expressions.structure.LogicalSimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81d5484L, "jetbrains.mps.samples.Expressions.structure.NotSimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b825cc4aL, "jetbrains.mps.samples.Expressions.structure.OrSimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0xce8a4f56651064cL, "jetbrains.mps.samples.Expressions.structure.SimpleMathAssignment"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b826823eL, "jetbrains.mps.samples.Expressions.structure.SimpleMathBooleanConstant"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81f9f68L, "jetbrains.mps.samples.Expressions.structure.SimpleMathBooleanType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b835454eL, "jetbrains.mps.samples.Expressions.structure.SimpleMathElementType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81c4c0bL, "jetbrains.mps.samples.Expressions.structure.SimpleMathExpression"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b84b40a4L, "jetbrains.mps.samples.Expressions.structure.SimpleMathFloatConstant"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b84b3e3eL, "jetbrains.mps.samples.Expressions.structure.SimpleMathFloatType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b826940aL, "jetbrains.mps.samples.Expressions.structure.SimpleMathIntegerConstant"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b82698e0L, "jetbrains.mps.samples.Expressions.structure.SimpleMathIntegerType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b83bf7eaL, "jetbrains.mps.samples.Expressions.structure.SimpleMathLongConstant"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b837a88aL, "jetbrains.mps.samples.Expressions.structure.SimpleMathLongType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b8354763L, "jetbrains.mps.samples.Expressions.structure.SimpleMathNumberType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81f9f67L, "jetbrains.mps.samples.Expressions.structure.SimpleMathType"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x77a1220187231476L, "jetbrains.mps.samples.Expressions.structure.SimpleMathTypedVarDeclaration"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b832ccbfL, "jetbrains.mps.samples.Expressions.structure.SimpleMathVarDeclaration"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b832e4e1L, "jetbrains.mps.samples.Expressions.structure.SimpleMathVarReference"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b8237002L, "jetbrains.mps.samples.Expressions.structure.SimpleMathWrapper"), MetaAdapterFactory.getConcept(0x7e282943fc6b4900L, 0xada534c0024cc4f4L, 0x1cc69153b81d547bL, "jetbrains.mps.samples.Expressions.structure.UnarySimpleMathExpression"));
}

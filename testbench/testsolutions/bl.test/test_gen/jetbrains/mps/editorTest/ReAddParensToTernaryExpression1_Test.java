package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class ReAddParensToTernaryExpression1_Test extends BaseTransformationTest {
  @Test
  public void test_ReAddParensToTernaryExpression1() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.ReAddParensToTernaryExpression1_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("7247988578514919061", "7247988578514919077");
      invokeAction("jetbrains.mps.ide.editor.actions.Backspace_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.End_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
      typeString("(");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
    }
  }
}

package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class MoveRangeOutOfBlock_Test extends BaseTransformationTest {
  @Test
  public void test_MoveRangeOutOfBlock() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.MoveRangeOutOfBlock_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("7386845112408811681", "7386845112408811701");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectPrevious_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectPrevious_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveElementsUp_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
    }
  }
}

package jetbrains.mps.lang.test.intentions;

/*Generated by MPS */

import jetbrains.mps.intentions.IntentionAspectBase;
import jetbrains.mps.intentions.IntentionFactory;
import java.util.Map;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.HashMap;
import org.jetbrains.annotations.Nullable;
import java.util.Collection;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public final class IntentionsDescriptor extends IntentionAspectBase {
  private static final IntentionFactory[] EMPTY_ARRAY = new IntentionFactory[0];
  private Map<SAbstractConcept, IntentionFactory[]> myCached = new HashMap<SAbstractConcept, IntentionFactory[]>();

  public IntentionsDescriptor() {
  }

  @Nullable
  public Collection<IntentionFactory> getIntentions(@NotNull SAbstractConcept concept) {
    if (myCached.containsKey(concept)) {
      return Arrays.asList(myCached.get(concept));
    }

    IntentionFactory[] intentions = EMPTY_ARRAY;
    {
      SAbstractConcept cncpt = concept;
      Integer preIndex = indices_hphjzv_d0f.get(cncpt);
      int switchIndex = (preIndex == null ? -1 : preIndex);
      switch (switchIndex) {
        case 0:
          if (true) {
            // Concept: BaseConcept 
            intentions = new IntentionFactory[8];
            intentions[0] = new AddTestAnnotation_Intention();
            intentions[1] = new AddOperationsAnnotation_Intention();
            intentions[2] = new AddCellAnnotation_Intention();
            intentions[3] = new AddMockAnnotation_Intention();
            intentions[4] = new AddNodeHasErrorAnnotation_Intention();
            intentions[5] = new AddScopeTestAnnotation_Intention();
            intentions[6] = new AddNodeHasWarningAnnotation_Intention();
            intentions[7] = new SpecifyRuleReferences_Intention();
          }
          break;
        case 1:
          if (true) {
            // Concept: MessageStatement 
            intentions = new IntentionFactory[1];
            intentions[0] = new AddMessageAnnotation_Intention();
          }
          break;
        case 2:
          if (true) {
            // Concept: ScopesTest 
            intentions = new IntentionFactory[1];
            intentions[0] = new AddScopeExpectedNodes_Intention();
          }
          break;
        default:
          // default 
      }
    }
    myCached.put(concept, intentions);
    return Arrays.asList(intentions);
  }

  @NotNull
  @Override
  public Collection<IntentionFactory> getAllIntentions() {
    IntentionFactory[] rv = new IntentionFactory[10];
    rv[0] = new AddTestAnnotation_Intention();
    rv[1] = new AddOperationsAnnotation_Intention();
    rv[2] = new AddCellAnnotation_Intention();
    rv[3] = new AddMockAnnotation_Intention();
    rv[4] = new AddNodeHasErrorAnnotation_Intention();
    rv[5] = new AddScopeTestAnnotation_Intention();
    rv[6] = new AddScopeExpectedNodes_Intention();
    rv[7] = new AddNodeHasWarningAnnotation_Intention();
    rv[8] = new AddMessageAnnotation_Intention();
    rv[9] = new SpecifyRuleReferences_Intention();
    return Arrays.asList(rv);
  }
  private static Map<SAbstractConcept, Integer> buildConceptIndices(SAbstractConcept... concepts) {
    HashMap<SAbstractConcept, Integer> res = new HashMap<SAbstractConcept, Integer>();
    int counter = 0;
    for (SAbstractConcept c : concepts) {
      res.put(c, counter++);
    }
    return res;
  }
  private static final Map<SAbstractConcept, Integer> indices_hphjzv_d0f = buildConceptIndices(MetaAdapterFactory.getConcept(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL, "jetbrains.mps.lang.core.structure.BaseConcept"), MetaAdapterFactory.getInterfaceConcept(0x7a5dda6291404668L, 0xab76d5ed1746f2b2L, 0x11db4aad802L, "jetbrains.mps.lang.typesystem.structure.MessageStatement"), MetaAdapterFactory.getConcept(0x8585453e6bfb4d80L, 0x98deb16074f1d86cL, 0x7181d929c720809L, "jetbrains.mps.lang.test.structure.ScopesTest"));
}

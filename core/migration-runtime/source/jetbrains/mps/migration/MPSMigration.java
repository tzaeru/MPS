/*
 * Copyright 2003-2015 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jetbrains.mps.migration;

import jetbrains.mps.components.ComponentPluginBase;
import jetbrains.mps.migration.global.MigrationPropertiesManager;

/**
 * FIXME Is it worth a component plugin? MigrationPropertiesManager is in fact per-project component.
 * @author Artem Tikhomirov
 * @since 3.3
 */
public class MPSMigration extends ComponentPluginBase {
  @Override
  public void init() {
    super.init();
    init(new MigrationPropertiesManager());
  }
}

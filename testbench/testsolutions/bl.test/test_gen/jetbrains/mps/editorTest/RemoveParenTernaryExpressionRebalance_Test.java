package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class RemoveParenTernaryExpressionRebalance_Test extends BaseTransformationTest {
  @Test
  public void test_RemoveParenTernaryExpressionRebalance() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.RemoveParenTernaryExpressionRebalance_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("7247988578522302532", "7247988578533497719");
      invokeAction("jetbrains.mps.ide.editor.actions.Backspace_Action");
    }
  }
}

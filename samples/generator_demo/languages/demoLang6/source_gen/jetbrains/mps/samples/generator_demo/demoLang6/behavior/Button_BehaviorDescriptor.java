package jetbrains.mps.samples.generator_demo.demoLang6.behavior;

/*Generated by MPS */


/**
 * Will be removed after 3.4
 * Need to support compilation of the legacy behavior descriptors before the language is rebuilt
 * This class is not involved in the actual method invocation
 */
@Deprecated
public class Button_BehaviorDescriptor {
  public String getConceptFqName() {
    return null;
  }
}

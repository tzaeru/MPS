package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class MoveInManyStatementsListCommentedUp_Test extends BaseTransformationTest {
  @Test
  public void test_MoveInManyStatementsListCommentedUp() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.MoveInManyStatementsListCommentedUp_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("5845623209572069239", "5845623209572069254");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveElementsUp_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveElementsUp_Action");
    }
  }
}

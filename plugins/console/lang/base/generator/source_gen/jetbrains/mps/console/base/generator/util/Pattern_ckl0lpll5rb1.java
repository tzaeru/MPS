package jetbrains.mps.console.base.generator.util;

/*Generated by MPS */

import jetbrains.mps.lang.pattern.GeneratedMatcher;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

/*package*/ class Pattern_ckl0lpll5rb1 extends GeneratedMatcher {
  public Pattern_ckl0lpll5rb1(SNode patternNode) {
    super(patternNode, false);


    myTopMatcher.child(MetaAdapterFactory.getContainmentLink(0x8388864671ce4f1cL, 0x9c53c54016f6ad4fL, 0x10c260e9444L, 0x10c260ee40eL, "elementType")).at(0).capture("elementType");
  }

}

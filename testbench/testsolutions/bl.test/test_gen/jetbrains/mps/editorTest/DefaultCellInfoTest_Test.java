package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;
import jetbrains.mps.nodeEditor.EditorComponent;
import jetbrains.mps.nodeEditor.InspectorTool;
import java.util.Set;
import jetbrains.mps.nodeEditor.cells.EditorCell;
import junit.framework.Assert;

@MPSLaunch
public class DefaultCellInfoTest_Test extends BaseTransformationTest {
  @Test
  public void test_DefaultCellInfoTest() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.DefaultCellInfoTest_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("5560058483159205760", "5560058483159208304");
      EditorComponent inspector = myProject.getComponent(InspectorTool.class).getInspector();
      Set<EditorCell> errorCells = inspector.getCellTracker().getErrorCells();
      Assert.assertTrue(!(errorCells.isEmpty()));

      EditorCell editorCell = errorCells.iterator().next();
      inspector.getSelectionManager().setSelection(editorCell);
      invokeAction("jetbrains.mps.ide.editor.actions.MoveLeft_Action");
    }
  }
}

package jetbrains.mps.lang.actions.testLanguage.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.ConceptPresentationAspectBase;
import jetbrains.mps.smodel.runtime.ConceptPresentation;
import jetbrains.mps.smodel.runtime.ConceptPresentationBuilder;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Map;
import java.util.HashMap;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public class ConceptPresentationAspectImpl extends ConceptPresentationAspectBase {
  private final ConceptPresentation props_ActionTestAbstractChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestAbstractChildWrapper = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestChild1 = new ConceptPresentationBuilder().icon(IconContainer.RESOURCE_a0a0c).create();
  private final ConceptPresentation props_ActionTestChild1Child = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestChild1SubConcept = new ConceptPresentationBuilder().icon(IconContainer.RESOURCE_a0a0e).create();
  private final ConceptPresentation props_ActionTestChild2 = new ConceptPresentationBuilder().icon(IconContainer.RESOURCE_a0a0f).create();
  private final ConceptPresentation props_ActionTestChildToWrap1 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestChildToWrap2 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestChildWrapper = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestContainer = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestDefaultAbstractChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestDefaultChild1 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestDefaultChild2 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAbstractChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAddConceptAbstractChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAddConceptChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAnotherAbstractChild = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAnotherChild1 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAnotherChild2 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformAnotherChildCommonSuperConcept = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformChild1 = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformTestAncestor = new ConceptPresentationBuilder().create();
  private final ConceptPresentation props_ActionTestSidetransformTestContainer = new ConceptPresentationBuilder().create();

  @Override
  @Nullable
  public ConceptPresentation getDescriptor(SAbstractConcept c) {
    {
      SAbstractConcept cncpt = c;
      Integer preIndex = indices_lpa09p_a0y.get(cncpt);
      int switchIndex = (preIndex == null ? -1 : preIndex);
      switch (switchIndex) {
        case 0:
          if (true) {
            return props_ActionTestAbstractChild;
          }
          break;
        case 1:
          if (true) {
            return props_ActionTestAbstractChildWrapper;
          }
          break;
        case 2:
          if (true) {
            return props_ActionTestChild1;
          }
          break;
        case 3:
          if (true) {
            return props_ActionTestChild1Child;
          }
          break;
        case 4:
          if (true) {
            return props_ActionTestChild1SubConcept;
          }
          break;
        case 5:
          if (true) {
            return props_ActionTestChild2;
          }
          break;
        case 6:
          if (true) {
            return props_ActionTestChildToWrap1;
          }
          break;
        case 7:
          if (true) {
            return props_ActionTestChildToWrap2;
          }
          break;
        case 8:
          if (true) {
            return props_ActionTestChildWrapper;
          }
          break;
        case 9:
          if (true) {
            return props_ActionTestContainer;
          }
          break;
        case 10:
          if (true) {
            return props_ActionTestDefaultAbstractChild;
          }
          break;
        case 11:
          if (true) {
            return props_ActionTestDefaultChild1;
          }
          break;
        case 12:
          if (true) {
            return props_ActionTestDefaultChild2;
          }
          break;
        case 13:
          if (true) {
            return props_ActionTestSidetransformAbstractChild;
          }
          break;
        case 14:
          if (true) {
            return props_ActionTestSidetransformAddConceptAbstractChild;
          }
          break;
        case 15:
          if (true) {
            return props_ActionTestSidetransformAddConceptChild;
          }
          break;
        case 16:
          if (true) {
            return props_ActionTestSidetransformAnotherAbstractChild;
          }
          break;
        case 17:
          if (true) {
            return props_ActionTestSidetransformAnotherChild1;
          }
          break;
        case 18:
          if (true) {
            return props_ActionTestSidetransformAnotherChild2;
          }
          break;
        case 19:
          if (true) {
            return props_ActionTestSidetransformAnotherChildCommonSuperConcept;
          }
          break;
        case 20:
          if (true) {
            return props_ActionTestSidetransformChild1;
          }
          break;
        case 21:
          if (true) {
            return props_ActionTestSidetransformTestAncestor;
          }
          break;
        case 22:
          if (true) {
            return props_ActionTestSidetransformTestContainer;
          }
          break;
        default:
      }
    }
    throw new IllegalStateException();
  }
  private static Map<SAbstractConcept, Integer> buildConceptIndices(SAbstractConcept... concepts) {
    HashMap<SAbstractConcept, Integer> res = new HashMap<SAbstractConcept, Integer>();
    int counter = 0;
    for (SAbstractConcept c : concepts) {
      res.put(c, counter++);
    }
    return res;
  }
  private static final Map<SAbstractConcept, Integer> indices_lpa09p_a0y = buildConceptIndices(MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2365c14e1671a9caL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestAbstractChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x47ad1fca2ec53f71L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestAbstractChildWrapper"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2365c14e167220f9L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChild1"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x7f805f0c2dd45b4eL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChild1Child"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c99ba4dd5ff2533L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChild1SubConcept"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2365c14e1672224dL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChild2"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x47ad1fca2ee71badL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChildToWrap1"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2622c47afd434fd2L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChildToWrap2"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x47ad1fca2ec9ad4dL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestChildWrapper"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2365c14e1671696fL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestContainer"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x73dfe0fc25514b8dL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestDefaultAbstractChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x73dfe0fc25516733L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestDefaultChild1"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x73dfe0fc25516dd5L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestDefaultChild2"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x179f28a7ade471f3L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAbstractChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x43d1b15d415e8d80L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAddConceptAbstractChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x43d1b15d4168a426L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAddConceptChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c35cefefca54d96L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAnotherAbstractChild"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c35cefefca55189L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAnotherChild1"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c35cefefca551bdL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAnotherChild2"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c35cefefcaa0dd0L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformAnotherChildCommonSuperConcept"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x44969b12b8c94c1bL, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformChild1"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x2c35cefefc9cfa78L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformTestAncestor"), MetaAdapterFactory.getConcept(0x737ed1fffa634ebcL, 0xa834435499b23c64L, 0x179f28a7ade381e5L, "jetbrains.mps.lang.actions.testLanguage.structure.ActionTestSidetransformTestContainer"));
}

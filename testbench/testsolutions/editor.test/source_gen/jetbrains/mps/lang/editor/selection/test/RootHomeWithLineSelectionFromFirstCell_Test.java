package jetbrains.mps.lang.editor.selection.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class RootHomeWithLineSelectionFromFirstCell_Test extends BaseTransformationTest {
  @Test
  public void test_RootHomeWithLineSelectionFromFirstCell() throws Throwable {
    initTest("${mps_home}", "r:f429894b-858b-4e34-87ae-2cfe2a061928(jetbrains.mps.lang.editor.selection.test)");
    runTest("jetbrains.mps.lang.editor.selection.test.RootHomeWithLineSelectionFromFirstCell_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("2025581204008535356", "2025581204008535365");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectNext_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.RootHome_Action");
    }
  }
}

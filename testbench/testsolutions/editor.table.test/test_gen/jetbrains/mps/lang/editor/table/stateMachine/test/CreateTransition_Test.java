package jetbrains.mps.lang.editor.table.stateMachine.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class CreateTransition_Test extends BaseTransformationTest {
  @Test
  public void test_CreateTransition() throws Throwable {
    initTest("${mps_home}", "r:dc1400b5-0aa4-448e-8f15-11fb0ccb5c23(jetbrains.mps.lang.editor.table.stateMachine.test@tests)");
    runTest("jetbrains.mps.lang.editor.table.stateMachine.test.CreateTransition_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("8017670888383389087", "8207994269122295113");
      invokeAction("jetbrains.mps.ide.editor.actions.Insert_Action");
    }
  }
}

<?xml version="1.0" encoding="UTF-8"?>
<solution name="migratingSolution" uuid="90cf0f4e-629b-49b1-a23e-d4e05c2a8224" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:a59395ba-5d94-4758-a87c-b11e086d5491:updatedLanguage" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="90cf0f4e-629b-49b1-a23e-d4e05c2a8224(migratingSolution)" version="0" />
  </dependencyVersions>
</solution>


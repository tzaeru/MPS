package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import java.util.ArrayList;

@MPSLaunch
public class DeleteCompletion_Test extends BaseTransformationTest {
  @Test
  public void test_DeleteCompletion() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.DeleteCompletion_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("8933061889659412224", "8933061889659412234");
      invokeAction("jetbrains.mps.ide.editor.actions.Complete_Action");
      typeString("aDe");
      pressKeys(ListSequence.fromListAndArray(new ArrayList<String>(), " BACK_SPACE"));
      pressKeys(ListSequence.fromListAndArray(new ArrayList<String>(), " BACK_SPACE"));
      pressKeys(ListSequence.fromListAndArray(new ArrayList<String>(), " BACK_SPACE"));
      typeString("abc");
      pressKeys(ListSequence.fromListAndArray(new ArrayList<String>(), " ENTER"));
    }
  }
}

package jetbrains.mps.baseLanguage.unitTest.execution.client;

/*Generated by MPS */

import org.jetbrains.mps.openapi.model.SNode;
import org.jetbrains.mps.openapi.model.SNodeReference;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.NonNls;
import jetbrains.mps.baseLanguage.tuples.runtime.Tuples;
import java.util.List;

public interface ITestNodeWrapper<N extends SNode> {
  N getNode();
  SNodeReference getNodePointer();
  boolean isTestCase();
  @Nullable
  ITestNodeWrapper getTestCase();
  @NotNull
  Iterable<ITestNodeWrapper> getTestMethods();
  @NonNls
  String getName();
  @NonNls
  String getFqName();
  String getCachedFqName();
  Tuples._3<String, List<String>, List<String>> getTestRunParameters();
}

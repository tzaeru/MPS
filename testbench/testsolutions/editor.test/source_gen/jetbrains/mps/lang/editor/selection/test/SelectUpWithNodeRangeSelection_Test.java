package jetbrains.mps.lang.editor.selection.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class SelectUpWithNodeRangeSelection_Test extends BaseTransformationTest {
  @Test
  public void test_SelectUpWithNodeRangeSelection() throws Throwable {
    initTest("${mps_home}", "r:f429894b-858b-4e34-87ae-2cfe2a061928(jetbrains.mps.lang.editor.selection.test)");
    runTest("jetbrains.mps.lang.editor.selection.test.SelectUpWithNodeRangeSelection_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("5476958923832863115", "5476958923832861258");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectNext_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectNext_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectUp_Action");
    }
  }
}

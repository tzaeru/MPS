/*
 * Copyright 2003-2016 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jetbrains.mps.lang.editor.menus.substitute;

import jetbrains.mps.lang.editor.menus.transformation.DefaultEmptyCellMenu;
import jetbrains.mps.lang.editor.menus.transformation.EmptyTransformationMenu;
import jetbrains.mps.lang.editor.menus.transformation.ImplicitTransformationMenu;
import jetbrains.mps.openapi.editor.descriptor.TransformationMenu;
import jetbrains.mps.openapi.editor.menus.transformation.MenuLookup;
import jetbrains.mps.openapi.editor.menus.transformation.TransformationMenuContext;
import jetbrains.mps.openapi.editor.menus.transformation.TransformationMenuItem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import org.jetbrains.mps.openapi.language.SLanguage;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author simon
 */
public class DefaultEmptyCellSubstituteMenuLookup implements MenuLookup<TransformationMenu> {
  private final SContainmentLink myLink;

  public DefaultEmptyCellSubstituteMenuLookup(SContainmentLink link) {
    myLink = link;
  }

  @NotNull
  @Override
  public Collection<TransformationMenu> lookup(@NotNull Collection<SLanguage> usedLanguages) {
    return Collections.singleton(new DefaultEmptyCellMenu(myLink));
  }

  @Override
  public TransformationMenu createImplicitMenu() {
    return EmptyTransformationMenu.INSTANCE;
  }
}

<?xml version="1.0" encoding="UTF-8"?>
<solution name="myReaderConfiguration" uuid="2403a764-6efe-4d9b-a8ae-0fe408281aaa" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="jetbrains" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="4" />
    <language slang="l:ed6d7656-532c-4bc2-81d1-af945aeb8280:jetbrains.mps.baseLanguage.blTypes" version="0" />
    <language slang="l:479c7a8c-02f9-43b5-9139-d910cb22f298:jetbrains.mps.core.xml" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
    <language slang="l:089e26c5-bfc3-4a60-9953-f68169a4608a:jetbrains.mps.samples.readerConfigLanguage" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="2403a764-6efe-4d9b-a8ae-0fe408281aaa(myReaderConfiguration)" version="0" />
  </dependencyVersions>
</solution>


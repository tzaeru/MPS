package jetbrains.mps.samples.Kaja.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import java.util.Map;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import jetbrains.mps.smodel.SNodePointer;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  private final Map<SConceptId, Integer> myIndexMap = new HashMap<SConceptId, Integer>(29);
  /*package*/ final ConceptDescriptor myConceptAbstractCommand = createDescriptorForAbstractCommand();
  /*package*/ final ConceptDescriptor myConceptCommandList = createDescriptorForCommandList();
  /*package*/ final ConceptDescriptor myConceptCommentLine = createDescriptorForCommentLine();
  /*package*/ final ConceptDescriptor myConceptDirection = createDescriptorForDirection();
  /*package*/ final ConceptDescriptor myConceptDrop = createDescriptorForDrop();
  /*package*/ final ConceptDescriptor myConceptEast = createDescriptorForEast();
  /*package*/ final ConceptDescriptor myConceptEmptyLine = createDescriptorForEmptyLine();
  /*package*/ final ConceptDescriptor myConceptHeading = createDescriptorForHeading();
  /*package*/ final ConceptDescriptor myConceptIfStatement = createDescriptorForIfStatement();
  /*package*/ final ConceptDescriptor myConceptIsFull = createDescriptorForIsFull();
  /*package*/ final ConceptDescriptor myConceptIsMark = createDescriptorForIsMark();
  /*package*/ final ConceptDescriptor myConceptIsWall = createDescriptorForIsWall();
  /*package*/ final ConceptDescriptor myConceptLeftTurn = createDescriptorForLeftTurn();
  /*package*/ final ConceptDescriptor myConceptLibrary = createDescriptorForLibrary();
  /*package*/ final ConceptDescriptor myConceptLogicalExpression = createDescriptorForLogicalExpression();
  /*package*/ final ConceptDescriptor myConceptLooking = createDescriptorForLooking();
  /*package*/ final ConceptDescriptor myConceptNorth = createDescriptorForNorth();
  /*package*/ final ConceptDescriptor myConceptNot = createDescriptorForNot();
  /*package*/ final ConceptDescriptor myConceptPick = createDescriptorForPick();
  /*package*/ final ConceptDescriptor myConceptRepeat = createDescriptorForRepeat();
  /*package*/ final ConceptDescriptor myConceptRequire = createDescriptorForRequire();
  /*package*/ final ConceptDescriptor myConceptRoutineCall = createDescriptorForRoutineCall();
  /*package*/ final ConceptDescriptor myConceptRoutineDefinition = createDescriptorForRoutineDefinition();
  /*package*/ final ConceptDescriptor myConceptScript = createDescriptorForScript();
  /*package*/ final ConceptDescriptor myConceptSouth = createDescriptorForSouth();
  /*package*/ final ConceptDescriptor myConceptStep = createDescriptorForStep();
  /*package*/ final ConceptDescriptor myConceptTraceMessage = createDescriptorForTraceMessage();
  /*package*/ final ConceptDescriptor myConceptWest = createDescriptorForWest();
  /*package*/ final ConceptDescriptor myConceptWhile = createDescriptorForWhile();

  public StructureAspectDescriptor() {
    myIndexMap.put(myConceptAbstractCommand.getId(), 0);
    myIndexMap.put(myConceptCommandList.getId(), 1);
    myIndexMap.put(myConceptCommentLine.getId(), 2);
    myIndexMap.put(myConceptDirection.getId(), 3);
    myIndexMap.put(myConceptDrop.getId(), 4);
    myIndexMap.put(myConceptEast.getId(), 5);
    myIndexMap.put(myConceptEmptyLine.getId(), 6);
    myIndexMap.put(myConceptHeading.getId(), 7);
    myIndexMap.put(myConceptIfStatement.getId(), 8);
    myIndexMap.put(myConceptIsFull.getId(), 9);
    myIndexMap.put(myConceptIsMark.getId(), 10);
    myIndexMap.put(myConceptIsWall.getId(), 11);
    myIndexMap.put(myConceptLeftTurn.getId(), 12);
    myIndexMap.put(myConceptLibrary.getId(), 13);
    myIndexMap.put(myConceptLogicalExpression.getId(), 14);
    myIndexMap.put(myConceptLooking.getId(), 15);
    myIndexMap.put(myConceptNorth.getId(), 16);
    myIndexMap.put(myConceptNot.getId(), 17);
    myIndexMap.put(myConceptPick.getId(), 18);
    myIndexMap.put(myConceptRepeat.getId(), 19);
    myIndexMap.put(myConceptRequire.getId(), 20);
    myIndexMap.put(myConceptRoutineCall.getId(), 21);
    myIndexMap.put(myConceptRoutineDefinition.getId(), 22);
    myIndexMap.put(myConceptScript.getId(), 23);
    myIndexMap.put(myConceptSouth.getId(), 24);
    myIndexMap.put(myConceptStep.getId(), 25);
    myIndexMap.put(myConceptTraceMessage.getId(), 26);
    myIndexMap.put(myConceptWest.getId(), 27);
    myIndexMap.put(myConceptWhile.getId(), 28);
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptAbstractCommand, myConceptCommandList, myConceptCommentLine, myConceptDirection, myConceptDrop, myConceptEast, myConceptEmptyLine, myConceptHeading, myConceptIfStatement, myConceptIsFull, myConceptIsMark, myConceptIsWall, myConceptLeftTurn, myConceptLibrary, myConceptLogicalExpression, myConceptLooking, myConceptNorth, myConceptNot, myConceptPick, myConceptRepeat, myConceptRequire, myConceptRoutineCall, myConceptRoutineDefinition, myConceptScript, myConceptSouth, myConceptStep, myConceptTraceMessage, myConceptWest, myConceptWhile);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    Integer index = myIndexMap.get(id);
    if (index == null) {
      return null;
    }
    switch (((int) index)) {
      case 0:
        return myConceptAbstractCommand;
      case 1:
        return myConceptCommandList;
      case 2:
        return myConceptCommentLine;
      case 3:
        return myConceptDirection;
      case 4:
        return myConceptDrop;
      case 5:
        return myConceptEast;
      case 6:
        return myConceptEmptyLine;
      case 7:
        return myConceptHeading;
      case 8:
        return myConceptIfStatement;
      case 9:
        return myConceptIsFull;
      case 10:
        return myConceptIsMark;
      case 11:
        return myConceptIsWall;
      case 12:
        return myConceptLeftTurn;
      case 13:
        return myConceptLibrary;
      case 14:
        return myConceptLogicalExpression;
      case 15:
        return myConceptLooking;
      case 16:
        return myConceptNorth;
      case 17:
        return myConceptNot;
      case 18:
        return myConceptPick;
      case 19:
        return myConceptRepeat;
      case 20:
        return myConceptRequire;
      case 21:
        return myConceptRoutineCall;
      case 22:
        return myConceptRoutineDefinition;
      case 23:
        return myConceptScript;
      case 24:
        return myConceptSouth;
      case 25:
        return myConceptStep;
      case 26:
        return myConceptTraceMessage;
      case 27:
        return myConceptWest;
      case 28:
        return myConceptWhile;
      default:
        throw new IllegalStateException();
    }
  }

  private static ConceptDescriptor createDescriptorForAbstractCommand() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.AbstractCommand", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).abstract_().sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3265739055509559114")).create();
  }
  private static ConceptDescriptor createDescriptorForCommandList() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.CommandList", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand", "jetbrains.mps.lang.core.structure.IDontSubstituteByDefault").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x19796fa16a19888bL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785f06a40L, "commands", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL), true, true, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039896128"))).children(new String[]{"commands"}, new boolean[]{true}).sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039896127")).create();
  }
  private static ConceptDescriptor createDescriptorForCommentLine() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.CommentLine", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x58e59ea713fa2b43L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x58e59ea713fa2b45L, "text", new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "6405700485436287813"))).properties("text").alias("#", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "6405700485436287811")).create();
  }
  private static ConceptDescriptor createDescriptorForDirection() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Direction", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).abstract_().sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039675705")).create();
  }
  private static ConceptDescriptor createDescriptorForDrop() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Drop", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x58e59ea713f79f27L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).alias("drop", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "6405700485436120871")).create();
  }
  private static ConceptDescriptor createDescriptorForEast() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.East", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d41L)).super_("jetbrains.mps.samples.Kaja.structure.Direction").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).parents("jetbrains.mps.samples.Kaja.structure.Direction").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).alias("east", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039675713")).create();
  }
  private static ConceptDescriptor createDescriptorForEmptyLine() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.EmptyLine", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ee0a16L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand", "jetbrains.mps.lang.core.structure.IDontSubstituteByDefault").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x19796fa16a19888bL)).final_().alias("<empty>", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039740438")).create();
  }
  private static ConceptDescriptor createDescriptorForHeading() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Heading", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed2c41L)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ed2c42L, "direction", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039683650"))).children(new String[]{"direction"}, new boolean[]{false}).alias("heading", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039683649")).create();
  }
  private static ConceptDescriptor createDescriptorForIfStatement() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.IfStatement", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fbeL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ec9fc0L, "condition", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647680")), new ConceptDescriptorBuilder.Link(0x2de971c785ec9fc4L, "trueBranch", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647684")), new ConceptDescriptorBuilder.Link(0x2de971c785ec9fc5L, "falseBranch", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647685"))).children(new String[]{"condition", "trueBranch", "falseBranch"}, new boolean[]{false, false, false}).alias("if", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647678")).create();
  }
  private static ConceptDescriptor createDescriptorForIsFull() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.IsFull", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0xbebd01a737bec18L)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).alias("full", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "859008965969439768")).create();
  }
  private static ConceptDescriptor createDescriptorForIsMark() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.IsMark", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x58e59ea713f89fe6L)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).alias("mark", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "6405700485436186598")).create();
  }
  private static ConceptDescriptor createDescriptorForIsWall() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.IsWall", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785eca02cL)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).alias("wall ahead", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647788")).create();
  }
  private static ConceptDescriptor createDescriptorForLeftTurn() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.LeftTurn", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9f8bL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).alias("turnLeft", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647627")).create();
  }
  private static ConceptDescriptor createDescriptorForLibrary() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Library", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x3cfcda239f19d316L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x3cfcda239f19d317L, "definitions", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed6f79L), true, true, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "4394627182934741783"))).children(new String[]{"definitions"}, new boolean[]{true}).rootable().sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "4394627182934741782")).create();
  }
  private static ConceptDescriptor createDescriptorForLogicalExpression() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.LogicalExpression", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).abstract_().sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039647681")).create();
  }
  private static ConceptDescriptor createDescriptorForLooking() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Looking", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x61fd16e423a38042L)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x61fd16e423a38043L, "direction", new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "7060824959893078083"))).properties("direction").alias("looking", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "7060824959893078082")).create();
  }
  private static ConceptDescriptor createDescriptorForNorth() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.North", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d3bL)).super_("jetbrains.mps.samples.Kaja.structure.Direction").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).parents("jetbrains.mps.samples.Kaja.structure.Direction").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).alias("north", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039675707")).create();
  }
  private static ConceptDescriptor createDescriptorForNot() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Not", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ecb8b0L)).super_("jetbrains.mps.samples.Kaja.structure.LogicalExpression").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).parents("jetbrains.mps.samples.Kaja.structure.LogicalExpression").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ecb8b3L, "original", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039654067"))).children(new String[]{"original"}, new boolean[]{false}).alias("not", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039654064")).create();
  }
  private static ConceptDescriptor createDescriptorForPick() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Pick", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x58e59ea713f85f1dL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).alias("pick", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "6405700485436170013")).create();
  }
  private static ConceptDescriptor createDescriptorForRepeat() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Repeat", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ecd14cL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x2de971c785ecd14eL, "count", new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039660366"))).properties("count").childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ecd14fL, "body", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039660367"))).children(new String[]{"body"}, new boolean[]{false}).alias("repeat", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039660364")).create();
  }
  private static ConceptDescriptor createDescriptorForRequire() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Require", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x3cfcda239f1a1049L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x3cfcda239f1a104aL, "library", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x3cfcda239f19d316L), false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "4394627182934757450"))).references("library").alias("require", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "4394627182934757449")).create();
  }
  private static ConceptDescriptor createDescriptorForRoutineCall() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.RoutineCall", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed6f92L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(0x2de971c785ede3ccL, "definition", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed6f79L), false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039730636"))).references("definition").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039700882")).create();
  }
  private static ConceptDescriptor createDescriptorForRoutineDefinition() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.RoutineDefinition", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed6f79L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand", "jetbrains.mps.lang.core.structure.INamedConcept").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ed6f7cL, "body", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039700860"))).children(new String[]{"body"}, new boolean[]{false}).alias("routine", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039700857")).create();
  }
  private static ConceptDescriptor createDescriptorForScript() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Script", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc45746L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").version(1).super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept", "jetbrains.mps.execution.util.structure.IMainClass").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L), MetaIdFactory.conceptId(0x4caf0310491e41f5L, 0x8a9b2006b3a94898L, 0x40c1a7cb987d20d5L)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2d523c5e4cc4574cL, "body", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3265739055509559116")), new ConceptDescriptorBuilder.Link(0x2de971c785ed6f78L, "definitions", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed6f79L), true, true, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039700856"))).children(new String[]{"body", "definitions"}, new boolean[]{false, true}).rootable().sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3265739055509559110")).create();
  }
  private static ConceptDescriptor createDescriptorForSouth() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.South", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d4bL)).super_("jetbrains.mps.samples.Kaja.structure.Direction").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).parents("jetbrains.mps.samples.Kaja.structure.Direction").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).alias("south", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039675723")).create();
  }
  private static ConceptDescriptor createDescriptorForStep() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.Step", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc45762L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).alias("step", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3265739055509559138")).create();
  }
  private static ConceptDescriptor createDescriptorForTraceMessage() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.TraceMessage", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2c8eb033a8375aeeL)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(0x2c8eb033a8375aefL, "message", new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3210697320273763055"))).properties("message").alias("trace", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3210697320273763054")).create();
  }
  private static ConceptDescriptor createDescriptorForWest() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.West", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed2c3cL)).super_("jetbrains.mps.samples.Kaja.structure.Direction").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).parents("jetbrains.mps.samples.Kaja.structure.Direction").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ed0d39L)).alias("west", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039683644")).create();
  }
  private static ConceptDescriptor createDescriptorForWhile() {
    return new ConceptDescriptorBuilder("jetbrains.mps.samples.Kaja.structure.While", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ecece0L)).super_("jetbrains.mps.samples.Kaja.structure.AbstractCommand").version(1).super_(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).parents("jetbrains.mps.samples.Kaja.structure.AbstractCommand").parentIds(MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2d523c5e4cc4574aL)).childDescriptors(new ConceptDescriptorBuilder.Link(0x2de971c785ecece2L, "condition", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785ec9fc1L), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039667426")), new ConceptDescriptorBuilder.Link(0x2de971c785ecece3L, "body", MetaIdFactory.conceptId(0x49a08c51fe543ccL, 0xbd998b46d641d7f5L, 0x2de971c785f06a3fL), false, false, false, new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039667427"))).children(new String[]{"condition", "body"}, new boolean[]{false, false}).alias("while", "").sourceNode(new SNodePointer("r:b567205c-7e17-4168-b413-945a6e17f37d(jetbrains.mps.samples.Kaja.structure)", "3308300503039667424")).create();
  }
}

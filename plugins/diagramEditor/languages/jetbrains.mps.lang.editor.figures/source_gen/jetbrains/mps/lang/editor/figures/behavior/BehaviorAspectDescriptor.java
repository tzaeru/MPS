package jetbrains.mps.lang.editor.figures.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBehaviorAspectDescriptor;
import jetbrains.mps.core.aspects.behaviour.api.BHDescriptor;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Map;
import java.util.HashMap;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public final class BehaviorAspectDescriptor extends BaseBehaviorAspectDescriptor {
  private final BHDescriptor myFigureParameter__BehaviorDescriptor = new FigureParameter__BehaviorDescriptor();
  private final BHDescriptor myFigureParameterAttributeMethod__BehaviorDescriptor = new FigureParameterAttributeMethod__BehaviorDescriptor();
  private final BHDescriptor myFigureParameterAttributeField__BehaviorDescriptor = new FigureParameterAttributeField__BehaviorDescriptor();
  private final BHDescriptor myExternalViewFigure__BehaviorDescriptor = new ExternalViewFigure__BehaviorDescriptor();
  private final BHDescriptor myExternalViewFigureParameter__BehaviorDescriptor = new ExternalViewFigureParameter__BehaviorDescriptor();
  private final BHDescriptor myFigureAttribute__BehaviorDescriptor = new FigureAttribute__BehaviorDescriptor();
  private final BHDescriptor myFigureParameterAttribute__BehaviorDescriptor = new FigureParameterAttribute__BehaviorDescriptor();
  private final BHDescriptor myFigureParameterAttributeViewProperty__BehaviorDescriptor = new FigureParameterAttributeViewProperty__BehaviorDescriptor();

  public BehaviorAspectDescriptor() {
  }

  @Nullable
  public BHDescriptor getDescriptor(@NotNull SAbstractConcept concept) {
    {
      SAbstractConcept cncpt = concept;
      Integer preIndex = indices_846f5o_a0l.get(cncpt);
      int switchIndex = (preIndex == null ? -1 : preIndex);
      switch (switchIndex) {
        case 0:
          if (true) {
            return myExternalViewFigure__BehaviorDescriptor;
          }
          break;
        case 1:
          if (true) {
            return myExternalViewFigureParameter__BehaviorDescriptor;
          }
          break;
        case 2:
          if (true) {
            return myFigureAttribute__BehaviorDescriptor;
          }
          break;
        case 3:
          if (true) {
            return myFigureParameter__BehaviorDescriptor;
          }
          break;
        case 4:
          if (true) {
            return myFigureParameterAttribute__BehaviorDescriptor;
          }
          break;
        case 5:
          if (true) {
            return myFigureParameterAttributeField__BehaviorDescriptor;
          }
          break;
        case 6:
          if (true) {
            return myFigureParameterAttributeMethod__BehaviorDescriptor;
          }
          break;
        case 7:
          if (true) {
            return myFigureParameterAttributeViewProperty__BehaviorDescriptor;
          }
          break;
        default:
          // default 
      }
    }
    return null;
  }
  private static Map<SAbstractConcept, Integer> buildConceptIndices(SAbstractConcept... concepts) {
    HashMap<SAbstractConcept, Integer> res = new HashMap<SAbstractConcept, Integer>();
    int counter = 0;
    for (SAbstractConcept c : concepts) {
      res.put(c, counter++);
    }
    return res;
  }
  private static final Map<SAbstractConcept, Integer> indices_846f5o_a0l = buildConceptIndices(MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1e3b9cbb9f7493c2L, "jetbrains.mps.lang.editor.figures.structure.ExternalViewFigure"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1e3b9cbb9f749406L, "jetbrains.mps.lang.editor.figures.structure.ExternalViewFigureParameter"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a095b5a4L, "jetbrains.mps.lang.editor.figures.structure.FigureAttribute"), MetaAdapterFactory.getInterfaceConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4bf6bbafe7e7155L, "jetbrains.mps.lang.editor.figures.structure.FigureParameter"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x4b412569a0c593e1L, "jetbrains.mps.lang.editor.figures.structure.FigureParameterAttribute"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1ceea85e3fd59976L, "jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeField"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x1ceea85e3fd59954L, "jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeMethod"), MetaAdapterFactory.getConcept(0xd7722d504b934c3aL, 0xae061903d05f95a7L, 0x6595651980a1f8ecL, "jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeViewProperty"));
}

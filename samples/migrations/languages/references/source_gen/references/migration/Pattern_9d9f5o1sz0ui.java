package references.migration;

/*Generated by MPS */

import jetbrains.mps.lang.pattern.GeneratedMatcher;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

/*package*/ class Pattern_9d9f5o1sz0ui extends GeneratedMatcher {
  public Pattern_9d9f5o1sz0ui(SNode patternNode) {
    super(patternNode, false);


    myTopMatcher.association(MetaAdapterFactory.getReferenceLink(0x1610048531ac4899L, 0x91122289e22843ddL, 0x6aff2c104931574dL, 0x6aff2c104932a69aL, "target"), "comp");
  }

}

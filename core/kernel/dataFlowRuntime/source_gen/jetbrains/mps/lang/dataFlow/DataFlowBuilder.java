package jetbrains.mps.lang.dataFlow;

/*Generated by MPS */

import jetbrains.mps.lang.dataFlow.framework.IDataFlowBuilder;
import jetbrains.mps.smodel.IOperationContext;
import java.util.Collection;
import jetbrains.mps.lang.dataFlow.framework.IDataFlowModeId;
import java.util.Collections;

public abstract class DataFlowBuilder implements IDataFlowBuilder {

  /**
   * Since MPS 3.4 use {@link jetbrains.mps.lang.dataFlow.DataFlowBuilder#build(DataFlowBuilderContext) }}
   * 
   * @deprecated 
   */
  @Deprecated
  public void build(IOperationContext operationContext, DataFlowBuilderContext context) {
  }

  public void build(DataFlowBuilderContext context) {
    build(null, context);
  }

  @Override
  public Collection<IDataFlowModeId> getModes() {
    return Collections.<IDataFlowModeId>emptyList();
  }
}

package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class PasteSingleStatement_Test extends BaseTransformationTest {
  @Test
  public void test_PasteSingleStatement() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.PasteSingleStatement_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("3498805908282577298", "3498805908282580479");
      invokeAction("$Copy");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveUp_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.End_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectUp_Action");
      invokeAction("jetbrains.mps.ide.editor.actions.SelectUp_Action");
      invokeAction("$Paste");

    }
  }
}

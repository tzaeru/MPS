package jetbrains.mps.console.base.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.openapi.editor.style.Style;
import jetbrains.mps.editor.runtime.style.StyleImpl;
import jetbrains.mps.editor.runtime.style.StyleAttributes;
import jetbrains.mps.nodeEditor.cells.EditorCell_Constant;
import jetbrains.mps.lang.editor.cellProviders.SingleRoleCellProvider;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import jetbrains.mps.nodeEditor.cellMenu.OldNewCompositeSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.SChildSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.DefaultChildSubstituteInfo;
import jetbrains.mps.nodeEditor.MPSFonts;
import jetbrains.mps.openapi.editor.style.StyleRegistry;
import jetbrains.mps.nodeEditor.MPSColors;
import jetbrains.mps.lang.editor.menus.transformation.NamedTransformationMenuLookup;
import jetbrains.mps.smodel.language.LanguageRegistry;
import jetbrains.mps.nodeEditor.cells.EditorCell_ContextAssistantComponent;

public class CommandHolder_Editor extends DefaultNodeEditor {
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createCollection_nvbf9m_a(editorContext, node);
  }
  public EditorCell createInspectedCell(EditorContext editorContext, SNode node) {
    return this.createCollection_nvbf9m_a_0(editorContext, node);
  }
  private EditorCell createCollection_nvbf9m_a(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createHorizontal(editorContext, node);
    editorCell.setCellId("Collection_nvbf9m_a");
    editorCell.setBig(true);
    Style style = new StyleImpl();
    style.set(StyleAttributes.READ_ONLY, 0, true);
    editorCell.getStyle().putAll(style);
    CommandHolder_Actions.setCellActions(editorCell, node, editorContext);
    editorCell.addEditorCell(this.createConstant_nvbf9m_a0(editorContext, node));
    editorCell.addEditorCell(this.createRefNode_nvbf9m_b0(editorContext, node));
    editorCell.addEditorCell(this.createContextAssistant_nvbf9m_c0(editorContext, node));
    return editorCell;
  }
  private EditorCell createConstant_nvbf9m_a0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "> ");
    editorCell.setCellId("Constant_nvbf9m_a0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.SELECTABLE, 0, false);
    style.set(StyleAttributes.PUNCTUATION_RIGHT, 0, true);
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createRefNode_nvbf9m_b0(EditorContext editorContext, SNode node) {
    SingleRoleCellProvider provider = new CommandHolder_Editor.commandSingleRoleHandler_nvbf9m_b0(node, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x4e27160acb4484bL, 0x4e27160acb44924L, "command"), editorContext);
    return provider.createCell();
  }
  private class commandSingleRoleHandler_nvbf9m_b0 extends SingleRoleCellProvider {
    public commandSingleRoleHandler_nvbf9m_b0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(ownerNode, containmentLink, context);
    }
    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = super.createChildCell(child);
      installCellInfo(child, editorCell);
      return editorCell;
    }
    private void installCellInfo(SNode child, EditorCell editorCell) {
      editorCell.setSubstituteInfo(new OldNewCompositeSubstituteInfo(myEditorContext, new SChildSubstituteInfo(editorCell, myOwnerNode, MetaAdapterFactory.getContainmentLink(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x4e27160acb4484bL, 0x4e27160acb44924L, "command"), child), new DefaultChildSubstituteInfo(myOwnerNode, myContainmentLink.getDeclarationNode(), myEditorContext)));
      if (editorCell.getRole() == null) {
        editorCell.setRole("command");
      }
      Style style = new StyleImpl();
      SNode node = myOwnerNode;
      EditorContext editorContext = myEditorContext;
      style.set(StyleAttributes.READ_ONLY, 0, false);
      editorCell.getStyle().putAll(style);
    }
    @Override
    protected EditorCell createEmptyCell() {
      EditorCell editorCell = createEmptyCell_internal(myEditorContext, myOwnerNode);
      installCellInfo(null, editorCell);

      return editorCell;
    }
    private EditorCell createEmptyCell_internal(EditorContext editorContext, SNode node) {
      return this.createConstant_nvbf9m_a1a(editorContext, node);
    }
    private EditorCell createConstant_nvbf9m_a1a(EditorContext editorContext, SNode node) {
      EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "");
      editorCell.setCellId("Constant_nvbf9m_a1a");
      Style style = new StyleImpl();
      style.set(StyleAttributes.FONT_STYLE, 0, MPSFonts.PLAIN);
      style.set(StyleAttributes.TEXT_COLOR, 0, StyleRegistry.getInstance().getSimpleColor(MPSColors.gray));
      style.set(StyleAttributes.EDITABLE, 0, true);
      editorCell.getStyle().putAll(style);
      editorCell.setTransformationMenuLookup(new NamedTransformationMenuLookup(LanguageRegistry.getInstance(editorContext.getRepository()), MetaAdapterFactory.getConcept(0xde1ad86d6e504a02L, 0xb306d4d17f64c375L, 0x4e27160acb4484bL, "jetbrains.mps.console.base.structure.CommandHolder"), "jetbrains.mps.console.base.editor.CommandHolder_Empty_ContextAssistantMenu"));
      editorCell.setDefaultText("<no command>");
      editorCell.setSubstituteInfo(new SChildSubstituteInfo(editorCell));
      return editorCell;
    }
  }
  private EditorCell createContextAssistant_nvbf9m_c0(final EditorContext editorContext, final SNode node) {
    EditorCell editorCell = new EditorCell_ContextAssistantComponent(editorContext, node);
    editorCell.setCellId("ContextAssistant_nvbf9m_c0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.SELECTABLE, 0, false);
    editorCell.getStyle().putAll(style);
    return editorCell;
  }
  private EditorCell createCollection_nvbf9m_a_0(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createIndent2(editorContext, node);
    editorCell.setCellId("Collection_nvbf9m_a_0");
    editorCell.setBig(true);
    editorCell.addEditorCell(this.createConstant_nvbf9m_a0_0(editorContext, node));
    editorCell.addEditorCell(this.createConstant_nvbf9m_b0(editorContext, node));
    return editorCell;
  }
  private EditorCell createConstant_nvbf9m_a0_0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Congratulations!!!");
    editorCell.setCellId("Constant_nvbf9m_a0_0");
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_nvbf9m_b0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "Now you have seen all MPS internals.");
    editorCell.setCellId("Constant_nvbf9m_b0");
    editorCell.setDefaultText("");
    return editorCell;
  }
}

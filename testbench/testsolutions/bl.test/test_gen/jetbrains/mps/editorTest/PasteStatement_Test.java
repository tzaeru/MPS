package jetbrains.mps.editorTest;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class PasteStatement_Test extends BaseTransformationTest {
  @Test
  public void test_PasteStatement() throws Throwable {
    initTest("${mps_home}", "r:914ee49a-537d-44b2-a5fb-bac87a54743d(jetbrains.mps.editorTest@tests)");
    runTest("jetbrains.mps.editorTest.PasteStatement_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("1236160248682577254", "1236160248682577265");
      invokeAction("$Copy");
      invokeAction("jetbrains.mps.ide.editor.actions.MoveDown_Action");
      invokeAction("$Paste");
    }
  }
}
